﻿Partial Public Class Form1
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblTime = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.TimeEdit2 = New DevExpress.XtraEditors.TimeEdit()
        Me.TimeEdit1 = New DevExpress.XtraEditors.TimeEdit()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.SCHMEDBTXT = New DevExpress.XtraEditors.TextEdit()
        Me.PASSDBTXT = New DevExpress.XtraEditors.TextEdit()
        Me.USERDBTXT = New DevExpress.XtraEditors.TextEdit()
        Me.PORTDBTXT = New DevExpress.XtraEditors.TextEdit()
        Me.IPDBTXT = New DevExpress.XtraEditors.TextEdit()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.btnStockCut = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.txtWriteFile = New DevExpress.XtraEditors.TextEdit()
        Me.txtReadFile = New DevExpress.XtraEditors.TextEdit()
        Me.Night = New DevExpress.XtraEditors.TimeEdit()
        Me.morning = New DevExpress.XtraEditors.TimeEdit()
        Me.daytime = New DevExpress.XtraEditors.TimeEdit()
        Me.TimeEdit3 = New DevExpress.XtraEditors.TimeEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.TimeEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SCHMEDBTXT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PASSDBTXT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.USERDBTXT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PORTDBTXT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IPDBTXT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWriteFile.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtReadFile.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Night.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.morning.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.daytime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Location = New System.Drawing.Point(658, 79)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(74, 59)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "อัพเดจ ราคา"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblTime
        '
        Me.lblTime.Appearance.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTime.Location = New System.Drawing.Point(24, 42)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(52, 33)
        Me.lblTime.StyleController = Me.LayoutControl1
        Me.lblTime.TabIndex = 1
        Me.lblTime.Text = "เวลา"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.TimeEdit2)
        Me.LayoutControl1.Controls.Add(Me.TimeEdit1)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton5)
        Me.LayoutControl1.Controls.Add(Me.SCHMEDBTXT)
        Me.LayoutControl1.Controls.Add(Me.PASSDBTXT)
        Me.LayoutControl1.Controls.Add(Me.USERDBTXT)
        Me.LayoutControl1.Controls.Add(Me.PORTDBTXT)
        Me.LayoutControl1.Controls.Add(Me.IPDBTXT)
        Me.LayoutControl1.Controls.Add(Me.Button5)
        Me.LayoutControl1.Controls.Add(Me.Button3)
        Me.LayoutControl1.Controls.Add(Me.Button4)
        Me.LayoutControl1.Controls.Add(Me.btnStockCut)
        Me.LayoutControl1.Controls.Add(Me.Button2)
        Me.LayoutControl1.Controls.Add(Me.lblTime)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton4)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton3)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.Button1)
        Me.LayoutControl1.Controls.Add(Me.txtWriteFile)
        Me.LayoutControl1.Controls.Add(Me.txtReadFile)
        Me.LayoutControl1.Controls.Add(Me.Night)
        Me.LayoutControl1.Controls.Add(Me.morning)
        Me.LayoutControl1.Controls.Add(Me.daytime)
        Me.LayoutControl1.Controls.Add(Me.TimeEdit3)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(989, 222, 250, 350)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(912, 420)
        Me.LayoutControl1.TabIndex = 7
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'TimeEdit2
        '
        Me.TimeEdit2.EditValue = New Date(2015, 10, 3, 14, 0, 0, 0)
        Me.TimeEdit2.Location = New System.Drawing.Point(470, 229)
        Me.TimeEdit2.Name = "TimeEdit2"
        Me.TimeEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TimeEdit2.Properties.Mask.EditMask = "t"
        Me.TimeEdit2.Size = New System.Drawing.Size(418, 20)
        Me.TimeEdit2.StyleController = Me.LayoutControl1
        Me.TimeEdit2.TabIndex = 20
        '
        'TimeEdit1
        '
        Me.TimeEdit1.EditValue = New Date(2015, 10, 3, 14, 0, 0, 0)
        Me.TimeEdit1.Location = New System.Drawing.Point(470, 205)
        Me.TimeEdit1.Name = "TimeEdit1"
        Me.TimeEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TimeEdit1.Properties.Mask.EditMask = "t"
        Me.TimeEdit1.Size = New System.Drawing.Size(418, 20)
        Me.TimeEdit1.StyleController = Me.LayoutControl1
        Me.TimeEdit1.TabIndex = 19
        '
        'SimpleButton5
        '
        Me.SimpleButton5.Location = New System.Drawing.Point(209, 325)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(181, 22)
        Me.SimpleButton5.StyleController = Me.LayoutControl1
        Me.SimpleButton5.TabIndex = 18
        Me.SimpleButton5.Text = "Update Config"
        '
        'SCHMEDBTXT
        '
        Me.SCHMEDBTXT.Location = New System.Drawing.Point(100, 301)
        Me.SCHMEDBTXT.Name = "SCHMEDBTXT"
        Me.SCHMEDBTXT.Size = New System.Drawing.Size(290, 20)
        Me.SCHMEDBTXT.StyleController = Me.LayoutControl1
        Me.SCHMEDBTXT.TabIndex = 17
        '
        'PASSDBTXT
        '
        Me.PASSDBTXT.Location = New System.Drawing.Point(100, 277)
        Me.PASSDBTXT.Name = "PASSDBTXT"
        Me.PASSDBTXT.Size = New System.Drawing.Size(290, 20)
        Me.PASSDBTXT.StyleController = Me.LayoutControl1
        Me.PASSDBTXT.TabIndex = 16
        '
        'USERDBTXT
        '
        Me.USERDBTXT.Location = New System.Drawing.Point(100, 253)
        Me.USERDBTXT.Name = "USERDBTXT"
        Me.USERDBTXT.Size = New System.Drawing.Size(290, 20)
        Me.USERDBTXT.StyleController = Me.LayoutControl1
        Me.USERDBTXT.TabIndex = 15
        '
        'PORTDBTXT
        '
        Me.PORTDBTXT.Location = New System.Drawing.Point(100, 229)
        Me.PORTDBTXT.Name = "PORTDBTXT"
        Me.PORTDBTXT.Size = New System.Drawing.Size(290, 20)
        Me.PORTDBTXT.StyleController = Me.LayoutControl1
        Me.PORTDBTXT.TabIndex = 14
        '
        'IPDBTXT
        '
        Me.IPDBTXT.Location = New System.Drawing.Point(100, 205)
        Me.IPDBTXT.Name = "IPDBTXT"
        Me.IPDBTXT.Size = New System.Drawing.Size(290, 20)
        Me.IPDBTXT.StyleController = Me.LayoutControl1
        Me.IPDBTXT.TabIndex = 13
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(814, 142)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(74, 59)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "อ่าน Order"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(658, 142)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(74, 59)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "ค่าอาหาร"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(736, 142)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(74, 59)
        Me.Button4.TabIndex = 5
        Me.Button4.Text = "สิ้นวัน"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'btnStockCut
        '
        Me.btnStockCut.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStockCut.Location = New System.Drawing.Point(814, 79)
        Me.btnStockCut.Name = "btnStockCut"
        Me.btnStockCut.Size = New System.Drawing.Size(74, 59)
        Me.btnStockCut.TabIndex = 2
        Me.btnStockCut.Text = "ตัดสต๊อก"
        Me.btnStockCut.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(736, 79)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(74, 59)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "ค่าห้อง"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Location = New System.Drawing.Point(491, 131)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(163, 22)
        Me.SimpleButton4.StyleController = Me.LayoutControl1
        Me.SimpleButton4.TabIndex = 12
        Me.SimpleButton4.Text = "เย็น"
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Location = New System.Drawing.Point(491, 105)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(163, 22)
        Me.SimpleButton3.StyleController = Me.LayoutControl1
        Me.SimpleButton3.TabIndex = 11
        Me.SimpleButton3.Text = "กลางวัน"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(491, 79)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(163, 22)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 10
        Me.SimpleButton2.Text = "เช้า"
        '
        'txtWriteFile
        '
        Me.txtWriteFile.Location = New System.Drawing.Point(100, 181)
        Me.txtWriteFile.Name = "txtWriteFile"
        Me.txtWriteFile.Size = New System.Drawing.Size(554, 20)
        Me.txtWriteFile.StyleController = Me.LayoutControl1
        Me.txtWriteFile.TabIndex = 8
        '
        'txtReadFile
        '
        Me.txtReadFile.Location = New System.Drawing.Point(100, 157)
        Me.txtReadFile.Name = "txtReadFile"
        Me.txtReadFile.Size = New System.Drawing.Size(554, 20)
        Me.txtReadFile.StyleController = Me.LayoutControl1
        Me.txtReadFile.TabIndex = 7
        '
        'Night
        '
        Me.Night.EditValue = New Date(2015, 10, 3, 14, 0, 0, 0)
        Me.Night.Location = New System.Drawing.Point(100, 131)
        Me.Night.Name = "Night"
        Me.Night.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.Night.Properties.Mask.EditMask = "t"
        Me.Night.Size = New System.Drawing.Size(387, 20)
        Me.Night.StyleController = Me.LayoutControl1
        Me.Night.TabIndex = 6
        '
        'morning
        '
        Me.morning.EditValue = New Date(2015, 10, 3, 1, 0, 0, 0)
        Me.morning.Location = New System.Drawing.Point(100, 79)
        Me.morning.Name = "morning"
        Me.morning.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.morning.Properties.Mask.EditMask = "t"
        Me.morning.Size = New System.Drawing.Size(387, 20)
        Me.morning.StyleController = Me.LayoutControl1
        Me.morning.TabIndex = 5
        '
        'daytime
        '
        Me.daytime.EditValue = New Date(2015, 10, 3, 8, 30, 0, 0)
        Me.daytime.Location = New System.Drawing.Point(100, 105)
        Me.daytime.Name = "daytime"
        Me.daytime.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.daytime.Properties.Mask.EditMask = "t"
        Me.daytime.Size = New System.Drawing.Size(387, 20)
        Me.daytime.StyleController = Me.LayoutControl1
        Me.daytime.TabIndex = 4
        '
        'TimeEdit3
        '
        Me.TimeEdit3.EditValue = New Date(2015, 10, 3, 14, 0, 0, 0)
        Me.TimeEdit3.Location = New System.Drawing.Point(470, 253)
        Me.TimeEdit3.Name = "TimeEdit3"
        Me.TimeEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TimeEdit3.Properties.Mask.EditMask = "t"
        Me.TimeEdit3.Size = New System.Drawing.Size(418, 20)
        Me.TimeEdit3.StyleController = Me.LayoutControl1
        Me.TimeEdit3.TabIndex = 20
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(912, 420)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.CustomizationFormText = "อาหาร"
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem1, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.EmptySpaceItem1, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.LayoutControlItem21, Me.LayoutControlItem22, Me.EmptySpaceItem2, Me.LayoutControlItem23, Me.LayoutControlItem24, Me.LayoutControlItem25})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(892, 400)
        Me.LayoutControlGroup2.Text = "อาหาร"
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.morning
        Me.LayoutControlItem2.CustomizationFormText = "เวลาอาหารเช้า"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 37)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(467, 26)
        Me.LayoutControlItem2.Text = "เวลาอาหารเช้า"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.daytime
        Me.LayoutControlItem1.CustomizationFormText = "เวลาอาหารเที่ยง"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 63)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(467, 26)
        Me.LayoutControlItem1.Text = "เวลาอาหารเที่ยง"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.Night
        Me.LayoutControlItem3.CustomizationFormText = "เวลาอาหารเย็น"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 89)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(467, 26)
        Me.LayoutControlItem3.Text = "เวลาอาหารเย็น"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.txtReadFile
        Me.LayoutControlItem4.CustomizationFormText = "Path Read"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 115)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(634, 24)
        Me.LayoutControlItem4.Text = "Path Read"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.txtWriteFile
        Me.LayoutControlItem5.CustomizationFormText = "Path Write"
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 139)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(634, 24)
        Me.LayoutControlItem5.Text = "Path Write"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(73, 13)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(370, 235)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(498, 123)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.SimpleButton2
        Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
        Me.LayoutControlItem7.Location = New System.Drawing.Point(467, 37)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(167, 26)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.SimpleButton3
        Me.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8"
        Me.LayoutControlItem8.Location = New System.Drawing.Point(467, 63)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(167, 26)
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextVisible = False
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.SimpleButton4
        Me.LayoutControlItem9.CustomizationFormText = "LayoutControlItem9"
        Me.LayoutControlItem9.Location = New System.Drawing.Point(467, 89)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(167, 26)
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = False
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.Button1
        Me.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10"
        Me.LayoutControlItem10.Location = New System.Drawing.Point(634, 37)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(78, 63)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.Button2
        Me.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11"
        Me.LayoutControlItem11.Location = New System.Drawing.Point(712, 37)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(78, 63)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.lblTime
        Me.LayoutControlItem12.CustomizationFormText = "LayoutControlItem12"
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(868, 37)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = False
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.btnStockCut
        Me.LayoutControlItem13.CustomizationFormText = "LayoutControlItem13"
        Me.LayoutControlItem13.Location = New System.Drawing.Point(790, 37)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(78, 63)
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = False
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.Button3
        Me.LayoutControlItem14.CustomizationFormText = "LayoutControlItem14"
        Me.LayoutControlItem14.Location = New System.Drawing.Point(634, 100)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(78, 63)
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem14.TextVisible = False
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.Button4
        Me.LayoutControlItem15.CustomizationFormText = "LayoutControlItem15"
        Me.LayoutControlItem15.Location = New System.Drawing.Point(712, 100)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(78, 63)
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem15.TextVisible = False
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.Button5
        Me.LayoutControlItem16.CustomizationFormText = "LayoutControlItem16"
        Me.LayoutControlItem16.Location = New System.Drawing.Point(790, 100)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(78, 63)
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem16.TextVisible = False
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.IPDBTXT
        Me.LayoutControlItem17.CustomizationFormText = "IP"
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 163)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(370, 24)
        Me.LayoutControlItem17.Text = "IP"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.PORTDBTXT
        Me.LayoutControlItem18.CustomizationFormText = "PORT"
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 187)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(370, 24)
        Me.LayoutControlItem18.Text = "PORT"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.USERDBTXT
        Me.LayoutControlItem19.CustomizationFormText = "USER"
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 211)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(370, 24)
        Me.LayoutControlItem19.Text = "USER"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.PASSDBTXT
        Me.LayoutControlItem20.CustomizationFormText = "PASSWORD"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 235)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(370, 24)
        Me.LayoutControlItem20.Text = "PASSWORD"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.SCHMEDBTXT
        Me.LayoutControlItem21.CustomizationFormText = "SCHEMA"
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 259)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(370, 24)
        Me.LayoutControlItem21.Text = "SCHEMA"
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.SimpleButton5
        Me.LayoutControlItem22.CustomizationFormText = "LayoutControlItem22"
        Me.LayoutControlItem22.Location = New System.Drawing.Point(185, 283)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(185, 75)
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem22.TextVisible = False
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 283)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(185, 75)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.TimeEdit1
        Me.LayoutControlItem23.Location = New System.Drawing.Point(370, 163)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(498, 24)
        Me.LayoutControlItem23.Text = "AutoFood1 "
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.TimeEdit2
        Me.LayoutControlItem24.Location = New System.Drawing.Point(370, 187)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(498, 24)
        Me.LayoutControlItem24.Text = "AutoFood2"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.TimeEdit3
        Me.LayoutControlItem25.CustomizationFormText = "LayoutControlItem24"
        Me.LayoutControlItem25.Location = New System.Drawing.Point(370, 211)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(498, 24)
        Me.LayoutControlItem25.Text = "AutoFood3"
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(73, 13)
        '
        'Form1
        '
        Me.ClientSize = New System.Drawing.Size(912, 420)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.TimeEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SCHMEDBTXT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PASSDBTXT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.USERDBTXT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PORTDBTXT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IPDBTXT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWriteFile.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtReadFile.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Night.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.morning.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.daytime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblTime As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnStockCut As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Night As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents morning As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents daytime As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtWriteFile As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtReadFile As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents USERDBTXT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PORTDBTXT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents IPDBTXT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SCHMEDBTXT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PASSDBTXT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents TimeEdit1 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TimeEdit2 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TimeEdit3 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem

#End Region

End Class
