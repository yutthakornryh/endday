﻿Imports System.ComponentModel
Imports System.Text


Partial Public Class Form1
    Inherits DevExpress.XtraEditors.XtraForm
    'Dim day As String = Today.ToString("yyyy/MM/dd")
    'Dim time As String = "09:20"

    Private endDay As New ENDDAYCLASS()
    Private curdate, lastdate As DateTime
    Private endDayTime As String = "00:00:01"
    Private cutStockTime As String = "23:59:00"
    Private Second As Integer = 0
    Shared Sub New()
        ' DevExpress.UserSkins.BonusSkins.Register()
        DevExpress.Skins.SkinManager.EnableFormSkins()
    End Sub
    Public Sub New()
        InitializeComponent()
        'set date
        Dim defaultDate = getCurrDate()
        curdate = defaultDate
        lastdate = defaultDate
    End Sub

    Private Function getCurrDate() As DateTime
        Dim dt As New DataTable
        Dim db = ConnecDBRYH2.NewConnection
        dt = db.GetTable("SELECT current_timestamp()")
        db.Dispose()
        If dt.Rows.Count > 0 Then
            Return Convert.ToDateTime(dt.Rows(0)(0))
        Else
            Return Today
        End If
    End Function

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        'MsgBox(DateTime.Now.ToString("hh:mm").Trim & " = " & time.Trim)
        'If DateTime.Now.ToString("hh:mm").Trim = time.Trim Then
        'MsgBox(DateTime.Now.ToString("hh:mm:ss").Trim & " = " & cutStockTime.Trim)
        Second += 1
        If Second = 20 Then
            Second = 0
            endDay.ReadOrder()

            ' MsgBox("")
        End If


        lastdate = curdate
        curdate = getCurrDate()
        '   MsgBox(morning.Time.ToString("HH:mm:ss").Trim)
        If curdate.ToString("HH:mm:ss").Trim = TimeEdit1.Time.ToString("HH:mm:ss").Trim Then
            endDay.getOrderInsertFoodAuto1()

        End If
        If curdate.ToString("HH:mm:ss").Trim = TimeEdit2.Time.ToString("HH:mm:ss").Trim Then
            endDay.getOrderInsertFoodAuto1()

        End If
        If curdate.ToString("HH:mm:ss").Trim = TimeEdit3.Time.ToString("HH:mm:ss").Trim Then
            endDay.getOrderInsertFoodAuto1()

        End If

        If curdate.ToString("HH:mm:ss").Trim = endDayTime.Trim Then
            If curdate.Day = 1 Then
                endDay.getOrderSeq()
                endDay.getStockOnhandaily()
                '     MsgBox("")
            End If
            endDay.getOrderMasVn()
            endDay.RunEndDay()

            endDay.getOrderRoom()
            endDay.getOrderFoodMorning()
            endDay.getOrderFoodDayTime()
            endDay.getOrderFoodDayEvening()


            '   MsgBox("OK")
        End If
        lblTime.Text = curdate.ToString("HH:mm:ss")
        If curdate.ToString("HH:mm:ss").Trim = cutStockTime.Trim Then
            'endDay.CutStock()
            '   MsgBox("Cut Stock")
        End If

        'If curdate.ToString("yyyy/MM/dd") <> lastdate.ToString("yyyy/MM/dd") Then

        'End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        IPDBTXT.Text = My.Settings.IPDB
        PASSDBTXT.Text = My.Settings.DBPASSWORD
        PORTDBTXT.Text = My.Settings.DBPORT
        USERDBTXT.Text = My.Settings.DBUSER
        SCHMEDBTXT.Text = My.Settings.DBSCHEMA
        Timer1.Enabled = True
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Timer1.Enabled = False
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        endDay.RunEndDay()
    End Sub

    Private Sub btnStockCut_Click(sender As Object, e As EventArgs) Handles btnStockCut.Click
        endDay.getStockOnhandaily()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        endDay.getOrderRoom()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        endDay.getOrderFoodMorning()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        'MsgBox(Date.Now.ToString("dd"))
        endDay.getOrderMasVn()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        endDay.ReadOrder()
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        endDay.getOrderFoodMorning()
    End Sub

    Private Sub morning_EditValueChanged(sender As Object, e As EventArgs) Handles morning.EditValueChanged

    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        endDay.getOrderFoodDayTime()

    End Sub

    Private Sub SimpleButton4_Click(sender As Object, e As EventArgs) Handles SimpleButton4.Click
        endDay.getOrderFoodDayEvening()
    End Sub

    Private Sub SimpleButton5_Click(sender As Object, e As EventArgs) Handles SimpleButton5.Click
        MsgBox("Update เสร็จสิ้น")
        My.Settings.IPDB = IPDBTXT.Text
        My.Settings.DBPASSWORD = PASSDBTXT.Text
        My.Settings.DBPORT = PORTDBTXT.Text
        My.Settings.DBUSER = USERDBTXT.Text
        My.Settings.DBSCHEMA = SCHMEDBTXT.Text
    End Sub

    Private Sub SimpleButton6_Click(sender As Object, e As EventArgs)
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs)

    End Sub
End Class
