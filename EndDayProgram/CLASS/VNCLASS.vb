﻿Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Drawing
Imports System.Windows.Forms
Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Globalization
Imports System.Threading
Imports DevComponents.DotNetBar.SuperGrid
Imports System.Text

Public Class VNCLASS
    Private VN As VNStruct
    ' Private iDate As New FriendsDate() 'object จัดการวันที่
    Public ConnectionDB As ConnecDBRYH
    Private sqlBuilder As New StringBuilder
    Public Sub New()
        Me.DataClear()
    End Sub

    Structure VNStruct
        Private HN As String
        Public Property _HN As String

        Public Property _SEX As String
        Public Property _ORDERID As String

        Private VN As String
        Public Property _VN As String

        Public Property _AN As String

        Private MSTAT As String
        Public Property _MSTAT As String

        Private TELEPHONE As String

        Public Property _TELEPHONE As String

        Private APDATE As String
        Public Property _APPDATE As String

        Private APPOINT As Integer
        Public Property _APPOINT As Integer

        Private ASTTIME As String
        Public Property _ASTTIME As String

        Private APPREM As String
        Public Property _APPREM As String

        Private DATE_SERV As DateTime
        Public Property _DATE_SERV As DateTime

        Private TYPEIN As String
        Public Property _TYPEIN As String

        Private TYPEINDESC As String
        Public Property _TYPEINDESC As String

        Private INTIME As String
        Public Property _INTIME As String

        Private INTIMEDESC As String
        Public Property _INTIMEDESC As String

        Private REFERINHOSP As String
        Public Property _REFERINHOSP As String

        Private URGID As Integer
        Public Property _URGID As Integer

        Private CHIEFCOMP As String
        Public Property _CHIEFCOMP As String

        Private REFERINHOSPDESC As String
        Public Property _REFERINHOSPDESC As String

        Private CLINIC As String
        Public Property _CLINIC As String

        Private CLINICNAME As String
        Public Property _CLINICNAME As String

        Private CSTATUS As Integer
        Public Property _CSTATUS As Integer

        Private CLINICSTATUS As String
        Public Property _CLINICSTATUS As String

        Private CLINICSTATUSDESC As String
        Public Property _CINICFINISH As Boolean

        Private CINICFINISH As Boolean
        Public Property _CLINICSTATUSDESC As String

        Private SENDBYCLINIC As String
        Public Property _SENDBYCLINIC As String

        Private DR As String
        Public Property _DR As String
        Private DRNAME As String
        Public Property _DRNAME As String

        Private PROVIDERKEY As Integer
        Public Property _PROVIDERKEY As Integer

        Private FTPRENAME As String
        Public Property _FTPRENAME As String

        Private PRENAME As String
        Public Property _PRENAME As String

        Private NAME As String
        Public Property _NAME As String

        Private LNAME As String
        Public Property _LNAME As String

        Private SEPRENAME As String
        Public Property _SEPRENAME As String

        Private ENAME As String
        Public Property _ENAME As String

        Private ELNAME As String
        Public Property _ELNAME As String

        Private BIRTH As String
        Public Property _BIRTH As String

        Private ABOGROUP As String
        Public Property _ABOGROUP As String

        Private OCCUPATION As String
        Public Property _OCCUPATION As String

        Private RELATION As String
        Public Property _RELATION As String

        Private RELIGION As String
        Public Property _RELIGION As String

        Private CID As String
        Public Property _CID As String

        Private ABOGROUPDESC As String
        Public Property _ABOGROUPDESC As String

        Private MOBILE As String
        Public Property _MOBILE As String

        Private NAMEPC As String
        Public Property _NAMEPC As String

        Private TELEPHONEPC As String
        Public Property _TELEPHONEPC As String

        Private โรคประจำตัว As String
        Public Property _โรคประจำตัว As String

        Private DRUGALERT As String
        Public Property _DRUGALERT As String

        Private DRUGALERTDESC As String
        Public Property _DRUGALERTDESC As String

        Private DOCCER As String
        Public Property _DOCCER As String

        Private CLAIM As String
        Public Property _CLAIM As String

        Private AGEYEAR As String
        Public Property _AGEYEAR As String

        Private AGEMONTH As Integer
        Public Property _AGEMONTH As Integer

        Private AGEDAY As Integer
        Public Property _AGEDAY As Integer

        Private ISREQUESTDOCUMENT As Boolean
        Public Property _ISREQUESTDOCUMENT As Boolean

        Private RECTIME As String
        Public Property _RECTIME As String

        Private SENTIME As String
        Public Property _SENTIME As String

        Private FINTIME As String
        Public Property _FINTIME As String

        Private KEYBY As String
        Public Property _KEYBY As String

        Private KEYBYNAME As String
        Public Property _KEYBYNAME As String

        Private ISNEWPATIENT As Integer
        Public Property _ISNEWPATIENT As Integer

        Private BP1 As String
        Public Property _BP1 As String

        Private BP2 As String
        Public Property _BP2 As String

        Private P As String
        Public Property _P As String

        Private BT As String
        Public Property _BT As String

        Private R As String
        Public Property _R As String

        Private BW As String
        Public Property _BW As String

        Private HT As String
        Public Property _HT As String

        Private BMI As String
        Public Property _BMI As String

        Private O2 As String
        Public Property _O2 As String

        Private LEGAL As String
        Public Property _LEGAL As String

        Private Systolic As String
        Public Property _Systolic As String

        Private Diastolic As String
        Public Property _Diastolic As String

        Private DIAGDOSE As String
        Public Property _DIAGDOSE As String

        Private DOCUMENT As String
        Public Property _DOCUMENT As String

        Private NATION As String
        Public Property _NATION As String

        Private EMOTION As String
        Public Property _EMOTION As String

        Private DISEASES As String 'โรคประจำตัว
        Public Property _DISEASES As String

        Private EmergencyTel As String 'เบอร์โทรฉุกเฉิน
        Public Property _EmergencyTel As String

        Private PENNAME As Boolean 'สถานะปิดบัง
        Public Property _PENNAME As Boolean

        Private PEN_NAME As String
        Public Property _PEN_NAME As String

        Private PEN_LNAME As String
        Public Property _PEN_LNAME As String

        Private DOCTORN As String
        Public Property _DOCTORN As String

        Private USERREQ As String
        Public Property _USERREQ As String

        Private F_DOC As String
        Public Property _F_DOC As String

        Private PLIGHT As String
        Public Property _PLIGHT As String

        Private F_VS As String
        Public Property _F_VS As String

        Private TTDRUG As String
        Public Property _TTDRUG As String

        Private TDRUG As String
        Public Property _TDRUG As String

        Private MIND As String
        Public Property _MIND As String

        Private RFEELID As String
        Public Property _RFEELID As String

        Private ICTYPE As String
        Public Property _ICTYPE As String

        Private F_ISOLATE As String
        Public Property _F_ISOLATE As String

        Private F_MASK As String
        Public Property _F_MASK As String

        Private SICK_BB As String
        Public Property _SICK_BB As String

        Private HIS_OR As String
        Public Property _HIS_OR As String

        Private CONSULT As String
        Public Property _CONSULT As String

        Private CATE As String
        Public Property _CATE As String

        Private STAKE As String
        Public Property _STAKE As String

        Private DATEMEET As Date
        Public Property _DATEMEET As Date

        Private TIMEMEET As String
        Public Property _TIMEMEET As String

        Private BCLINIC As String
        Public Property _BCLINIC As String

        Private BCLINIC_NAME As String
        Public Property _BCLINIC_NAME As String

        Private PENID As Integer
        Public Property _PENID As Integer

        '-------รายละเอียด vital sign--------
        Private VSTIME As Date
        Public Property _VSTIME As Date

        Private BPP As Date
        Public Property _BPP As Date

        Private RT As Date
        Public Property _RT As Date

        Private O2SATPAIN As Date
        Public Property _O2SATPAIN As Date

        Private COMANEWS As Date
        Public Property _COMANEWS As Date

        Private DETAIL As Date
        Public Property _DETAIL As Date

        Private NURSECARE As Date
        Public Property _NURSECARE As Date

        'เช็คสถานะ ประเภทราคา

        Dim CHKPRC As Integer
        Public Property CHKPRC_ As Integer

        Public Property _ERRMSG As String

        Public Property _CLINICID As String

        Public Property _BCLINICID As String

        Private FINISH As Integer
        Public Property _FINISH As Integer

        'สถานะยาด่วน
        Private F_ALT As Integer
        Public Property _F_ALT As Integer

        'Private AN As Integer
        'Private PRENAME As String
        'Private NAME As String
        'Private LASTNAME As String
        'Private REMARK As String
        Public Property dcl As String
        Public Property doctor As String
        Public Property painscore As String
        Public Property _PSTATUS As Integer
        Public Property _PSTATUSNAME As String

    End Structure
    Private FRNCLINICID As Integer

    Public Property _FRNCLINICID As Integer

        Get
            Return FRNCLINICID
        End Get
        Set(ByVal Value As Integer)
            FRNCLINICID = Value
        End Set
    End Property
    Public Property _VN As VNStruct
        Get
            Return VN
        End Get
        Set(ByVal Value As VNStruct)
            VN = Value
        End Set
    End Property

    Public Sub setDocument(Docid As Integer)
        Dim sql As String
        sql = "INSERT INTO frndocreq (vn,docid,status,usmk) VALUES('" & VN._VN & "'," & Docid & ",1, '" & VN._PROVIDERKEY & "');"
        ConnectionDB = ConnecDBRYH.NewConnection
        ConnectionDB.ExecuteNonQuery(sql)
        ConnectionDB.Dispose()
    End Sub

    Public Sub setDocument(ReqId As String, Docid As Integer, statusDoc As Integer)
        Dim sql As String
        If ReqId.ToString.Trim = "" Then
            sql = "INSERT INTO frndocreq (vn,docid,status,usmk) VALUES('" & VN._VN & "'," & Docid & ",1, '" & VN._PROVIDERKEY & "');"
        Else
            If getDocumentHavInDB(ReqId) = False And statusDoc = 1 Then
                sql = "INSERT INTO frndocreq (vn,docid,status,usmk) VALUES('" & VN._VN & "'," & Docid & ",1, '" & VN._PROVIDERKEY & "');"
            Else
                sql = "UPDATE frndocreq SET `status` = " & statusDoc & " WHERE reqid = '" & ReqId & "';"
            End If
        End If
        ConnectionDB = ConnecDBRYH.NewConnection
        ConnectionDB.ExecuteNonQuery(sql)
        ConnectionDB.Dispose()
    End Sub

    Public Function getDocument(ByVal vn As String) As DataTable
        Dim sql As String
        Dim dt As New DataTable
        ConnectionDB = ConnecDBRYH.NewConnection
        sql = "SELECT frndocreq.`docid`,`docname`,`docname_en` FROM frndocreq "
        sql += "LEFT JOIN (SELECT `docid`,`docname`,`docname_en` FROM  masdocument WHERE `status` = 1) AS masdocument ON frndocreq.`docid` = masdocument.`docid` "
        sql += "WHERE `status` = 1 AND `vn` = '" & vn & "'"
        dt = ConnectionDB.GetTable(sql)
        ConnectionDB.Dispose()
        Return dt

    End Function

    Public Function getDocumentHavInDB(ReqId As String) As Boolean
        Dim sql As String
        Dim dt As New DataTable
        ConnectionDB = ConnecDBRYH.NewConnection
        sql = "SELECT `reqid` FROM  frndocreq "
        sql += "WHERE reqid = '" & ReqId & "';"
        dt = ConnectionDB.GetTable(sql)
        ConnectionDB.Dispose()
        If dt.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function
    ''' <summary>
    ''' บันทึก vn ลงฐานข้อมูล หรือคืนคำสั่ง SQL กลับมา
    ''' </summary>
    ''' <remarks>
    ''' - Input (IsNewVN = เป็นvnเปิดใหม่หรือไม่,OpenByClinic = เปิดโดยรหัสคลีนิค,IsReturnSQL = ส่งค่าsqlกลับมาหรือไม่)
    '''  - Output บันทึก vn ลงฐานข้อมูล
    ''' </remarks>
    ''' <returns>sql</returns>
    Public Function SaveNewVN(IsNewVN As Boolean, OpenByClinic As String, IsReturnSQL As Boolean) As String
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dtpc As New DataTable
        Dim pc As Integer
        sqlBuilder.Append("SELECT `pc` FROM person WHERE `hn` = " & VN._HN & ";")
        dtpc = db.GetTable(sqlBuilder.ToString)
        If dtpc.Rows.Count Then
            pc = dtpc.Rows(0)("pc")
        End If
        sqlBuilder.Clear()
        ' iDate.beforSave()
        If (IsNewVN = True) Then 'เช็คว่าเปิด Vn ใหม่หรือป่าว
            '//////////////////// Insert ลง table frnservice  ////////////////////////
            sqlBuilder.Append("")
            sqlBuilder.Append("INSERT INTO  frnservice(`vn`,`hn`,`penid`,`pstatus`,`apprem`,`date_serv`,`typein`,`intime`,`referinhosp`,`urgid`,`f_doc`) VALUES( ")
            sqlBuilder.Append("'" & VN._VN & "',")
            sqlBuilder.Append("'" & VN._HN & "',")
            sqlBuilder.Append("" & VN._PENID & ",")
            sqlBuilder.Append("" & getPSTATUS(OpenByClinic) & ",")
            sqlBuilder.Append("'" & VN._APPREM & "',")
            sqlBuilder.Append("CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME),")
            If VN._TYPEIN = Nothing Then sqlBuilder.Append("NULL,") Else sqlBuilder.Append("'" & VN._TYPEIN & "',")
            If VN._INTIME = Nothing Then sqlBuilder.Append("NULL,") Else sqlBuilder.Append("'" & VN._INTIME & "',")
            If VN._REFERINHOSP = Nothing Then sqlBuilder.Append("NULL,") Else sqlBuilder.Append("'" & VN._REFERINHOSP & "',")
            If VN._URGID = Nothing Then sqlBuilder.Append("'" & ENVIRONMENTCLASS.MACHINE.Clinic & "', ") Else sqlBuilder.Append("'" & VN._URGID & "',")
            If VN._ISREQUESTDOCUMENT = True Then sqlBuilder.Append("1") Else sqlBuilder.Append("0")
            sqlBuilder.Append(");")
            db.BeginTrans()

            db.ExecuteNonQuery(sqlBuilder.ToString)


            sqlBuilder.Clear()

            sqlBuilder.Append("UPDATE person SET `pc` = " & pc & " + 1 ")
            sqlBuilder.Append("WHERE (`hn` = " & VN._HN & ");")
            db.ExecuteNonQuery(sqlBuilder.ToString)


            sqlBuilder.Clear()

            sqlBuilder.Append("UPDATE  frnappointment SET `vn` = " & VN._VN & " WHERE (`hn` = " & VN._HN & " AND `appointid` = " & VN._APPOINT & "); ")
            db.ExecuteNonQuery(sqlBuilder.ToString)
            db.CommitTrans()

        End If

        If (IsNewVN = True) Then 'เช็คว่าเปิด Vn ใหม่หรือป่าว
            sqlBuilder.Clear()
            '//////////////////// Insert ลง table masclinicseq ////////////////////
            sqlBuilder.Append("UPDATE  masclinicseq SET `SEQ` = `SEQ` +1 ")
            sqlBuilder.Append("WHERE(`CLINIC` = '" & OpenByClinic & "');")

            '//////////////////// Insert ลง table frnclinic  ////////////////////
            '1
            sqlBuilder.Append("INSERT INTO  frnclinic(`clinic`,`cstatus`,`vn`,`cseq`,`usmk`,`doctor`,`fintime`,`f_finish`,`f_lock`) VALUES(")
            sqlBuilder.Append("'" & OpenByClinic & "',")
            sqlBuilder.Append("5,")
            sqlBuilder.Append("" & VN._VN & ",")
            sqlBuilder.Append("(SELECT SEQ FROM  masclinicseq WHERE(clinic = '" & VN._CLINIC & "') ),")
            sqlBuilder.Append("'" & VN._PROVIDERKEY & "',")
            sqlBuilder.Append("NULL,")
            sqlBuilder.Append("curtime(),")
            sqlBuilder.Append("1,")
            sqlBuilder.Append("0")
            sqlBuilder.Append(");SELECT LAST_INSERT_ID();")
            _FRNCLINICID = db.ExecuteScalar(sqlBuilder.ToString)
        End If
        sqlBuilder.Clear()
        '//////////////////// Insert ลง table masclinicseq ////////////////////
        sqlBuilder.Append("UPDATE  masclinicseq SET `SEQ` = `SEQ` +1 ")
        sqlBuilder.Append("WHERE(`CLINIC` = '" & VN._CLINIC & "');")
        '2
        sqlBuilder.Append("INSERT INTO  frnclinic(`clinic`,`cstatus`,`vn`,`cseq`,`sentime`,`usmk`,`doctor`,`bclinic`,`req_card`,`f_lock`) VALUES(")
        sqlBuilder.Append("'" & VN._CLINIC & "',")
        sqlBuilder.Append(" " & ENVIRONMENTCLASS.MASCSTATUS.wait & ",")
        sqlBuilder.Append("" & VN._VN & ",")
        sqlBuilder.Append("(SELECT SEQ FROM  masclinicseq WHERE(clinic = '" & VN._CLINIC & "') ),")
        sqlBuilder.Append("curtime(),")
        sqlBuilder.Append("'" & VN._PROVIDERKEY & "',")
        sqlBuilder.Append("" & VN._DR & ",")
        sqlBuilder.Append("'" & OpenByClinic & "',")
        If VN._ISREQUESTDOCUMENT = True Then sqlBuilder.Append("1,") Else sqlBuilder.Append("0,")
        sqlBuilder.Append("0")
        sqlBuilder.Append(");")

        If IsReturnSQL = False Then
            db.BeginTrans()
            Try
                db.ExecuteNonQuery(sqlBuilder.ToString)
                db.CommitTrans()
            Catch ex As Exception
                Try
                    VN._VN = GetVNCode()
                    db.ExecuteNonQuery(sqlBuilder.ToString)

                Catch e As Exception
                    db.RollbackTrans()
                    MsgBox(ex.ToString)
                End Try

            Finally
                db.Dispose()
                '   iDate.afterSave()
            End Try
        End If
        Return sqlBuilder.ToString
    End Function

    ''' <summary>
    ''' บันทึก vn ลงฐานข้อมูล หรือคืนคำสั่ง SQL กลับมา สำหรับClinicเปิดVNเอง
    ''' </summary>
    ''' <remarks>
    ''' - Input (IsNewVN = เป็นvnเปิดใหม่หรือไม่,IsReturnSQL = ส่งค่าsqlกลับมาหรือไม่)
    '''  - Output บันทึก vn ลงฐานข้อมูล
    ''' </remarks>
    ''' <returns>sql</returns>
    Public Function SaveNewVNForMyClinic(IsNewVN As Boolean, IsReturnSQL As Boolean) As String
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        sqlBuilder.Clear()
        ' iDate.beforSave()
        If (IsNewVN = True) Then 'เช็คว่าเปิด Vn ใหม่หรือป่าว

            '//////////////////// Insert ลง table frnservice  ////////////////////////
            sqlBuilder.Append("INSERT INTO  frnservice(`vn`,`hn`,`date_serv`,`typein`,`intime`,`referinhosp`, `chiefcomp`, `urgid`,`f_doc`) VALUES( ")
            sqlBuilder.Append("'" & VN._VN & "',")
            sqlBuilder.Append("'" & VN._HN & "',")
            sqlBuilder.Append("CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME),")
            If VN._TYPEIN = Nothing Then sqlBuilder.Append("NULL,") Else sqlBuilder.Append("'" & VN._TYPEIN & "',")
            If VN._INTIME = Nothing Then sqlBuilder.Append("NULL,") Else sqlBuilder.Append("'" & VN._INTIME & "',")
            If VN._REFERINHOSP = Nothing Then sqlBuilder.Append("NULL, ") Else sqlBuilder.Append("'" & VN._REFERINHOSP & "',")
            If VN._CHIEFCOMP = Nothing Then sqlBuilder.Append("NULL, ") Else sqlBuilder.Append("'" & VN._CHIEFCOMP & "',")
            If VN._URGID = Nothing Then sqlBuilder.Append("'5', ") Else sqlBuilder.Append("'" & VN._URGID & "',")
            If VN._ISREQUESTDOCUMENT = True Then sqlBuilder.Append("1") Else sqlBuilder.Append("0")
            sqlBuilder.Append(");")

            '//////////////////// Insert ลง table masvn  ////////////////////////
            sqlBuilder.Append("INSERT INTO  masvn(`yr`,`mm`,`dd`,`seq`,`vn`) VALUES(")
            sqlBuilder.Append("year(curdate()),")
            sqlBuilder.Append("month(curdate()),")
            sqlBuilder.Append("day(curdate()),")
            sqlBuilder.Append(" getmax_masvn(),") 'เรียกฟังก์ชั่นใน Mysql
            sqlBuilder.Append("'" & VN._VN & "'")
            sqlBuilder.Append(");")
        End If

        '//////////////////// Insert ลง table masclinicseq ////////////////////
        sqlBuilder.Append("UPDATE  masclinicseq SET `seq` = `seq` +1 ")
        sqlBuilder.Append("WHERE(`clinic` = '" & VN._CLINIC & "');")

        '//////////////////// Insert ลง table frnclinic  ////////////////////
        '1
        sqlBuilder.Append("INSERT INTO  frnclinic(`clinic`,`cstatus`,`vn`,`cseq`,`sentime`,`provider`,`doctor`) VALUES(")
        sqlBuilder.Append("'" & VN._CLINIC & "',")
        sqlBuilder.Append(" " & ENVIRONMENTCLASS.MASCSTATUS.waitdiag & ",")
        sqlBuilder.Append("" & VN._VN & ",")
        sqlBuilder.Append("(SELECT seq FROM  masclinicseq WHERE(clinic = '" & VN._CLINIC & "') ),")
        sqlBuilder.Append("curtime(),")
        sqlBuilder.Append("'" & VN._PROVIDERKEY & "',")
        sqlBuilder.Append("'" & VN._DR & "' ")
        sqlBuilder.Append(");")




        If IsReturnSQL = False Then
            db.BeginTrans()
            Try
                db.ExecuteNonQuery(sqlBuilder.ToString)
                db.CommitTrans()
                MsgBox("บันทึกข้อมูลคนไข้สำเร็จ")
            Catch ex As Exception
                db.RollbackTrans()
                MsgBox(ex.ToString)
            Finally
                db.Dispose()
                '  iDate.afterSave()
            End Try
            Return ""
        Else
            Return sqlBuilder.ToString
        End If
    End Function

    ''' <summary>
    ''' ส่งต่อ VN ไปยังคลีนิคถัดไป
    ''' </summary>
    ''' <remarks>
    ''' - Input (CSTATUS = สถานะคลีนิค)
    '''   - Output บันทึกการส่งต่อคลีนิคลงฐานข้อมูล
    ''' </remarks>
    Public Function SendVN(CSTATUS As Integer) As Boolean
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        sqlBuilder.Clear()
        db.BeginTrans()
        Try
            '  iDate.beforSave() 'ปรับวันที่
            '//////////////////// Insert ลง table masclinicseq ////////////////////
            sqlBuilder.Append("UPDATE  masclinicseq SET `SEQ` = `SEQ` +1 ")
            sqlBuilder.Append("WHERE(`clinic` = '" & VN._CLINIC & "');")
            '//////////////////// Insert ลง table frnclinic  ////////////////////
            sqlBuilder.Append("INSERT INTO  frnclinic(`clinic`,`cstatus`,`vn`,`an`,`cseq`,`sentime`,`usmk`,`doctor`,`bclinic`,`bclinicid`, req_card ,f_lock,f_finish) VALUES(")
            sqlBuilder.Append("'" & VN._CLINIC & "',")
            sqlBuilder.Append("" & ENVIRONMENTCLASS.MASCSTATUS.wait & ",")
            sqlBuilder.Append("" & VN._VN & ",")
            If VN._AN IsNot Nothing Then
                sqlBuilder.Append("" & VN._AN & ",")
            Else
                sqlBuilder.Append("NULL,")
            End If
            sqlBuilder.Append("(SELECT SEQ FROM  masclinicseq WHERE(clinic = '" & VN._CLINIC & "') ),")
            sqlBuilder.Append("curtime(),")
            sqlBuilder.Append("'" & VN._PROVIDERKEY & "',")
            If VN._DR IsNot Nothing And VN._DR <> "" Then
                sqlBuilder.Append("'" & VN._DR & "', ")
            Else
                sqlBuilder.Append("NULL, ")
            End If

            If VN._BCLINIC IsNot Nothing Then
                sqlBuilder.Append("'" & VN._BCLINIC & "' , ")
            Else
                sqlBuilder.Append("NULL ,")
            End If

            If VN._CLINICID IsNot Nothing Then
                sqlBuilder.Append("'" & VN._CLINICID & "' , ")
            Else
                sqlBuilder.Append("NULL ,")
            End If

            sqlBuilder.Append(If(VN._ISREQUESTDOCUMENT = True, " 1 ,", " 0 ,"))
            sqlBuilder.Append(" 0 ,")
            sqlBuilder.Append(" 0 ")
            sqlBuilder.Append("); SELECT LAST_INSERT_ID();")
            _FRNCLINICID = db.ExecuteScalar(sqlBuilder.ToString)
            sqlBuilder.Clear()
            db.CommitTrans()



            db.BeginTrans()

            '/// Update frnservice
            sqlBuilder.Append("UPDATE  frnservice SET `f_doc` = ")
            If VN._ISREQUESTDOCUMENT = True Then sqlBuilder.Append("1") Else sqlBuilder.Append("0")
            sqlBuilder.Append(" WHERE(VN = " & VN._VN & ");")

            If CSTATUS = ENVIRONMENTCLASS.MASCSTATUS.send Or CSTATUS = ENVIRONMENTCLASS.MASCSTATUS.senttodoctor Or CSTATUS = ENVIRONMENTCLASS.MASCSTATUS.complete Then
                '//////////////////// Update สถานะของ sendclinic ลง table frnclinic  ////////////////////
                sqlBuilder.Append("UPDATE  frnclinic SET ")
                sqlBuilder.Append("cstatus = " & CSTATUS & ",fintime = curtime(),fintime = curtime(),f_finish = 1,usmk = " & VN._PROVIDERKEY & " ")
                If VN._CLINICID Is Nothing Then
                    sqlBuilder.Append("WHERE (vn='" & VN._VN & "' AND clinic='" & VN._BCLINIC & "');")
                Else
                    sqlBuilder.Append("WHERE (clinicid = '" & VN._CLINICID & "');")
                End If
            Else
                sqlBuilder.Append("UPDATE  frnclinic SET ")
                sqlBuilder.Append("cstatus = " & CSTATUS & " ,usmk = " & VN._PROVIDERKEY & " ")

                If CSTATUS = ENVIRONMENTCLASS.MASCSTATUS.diag Then
                    sqlBuilder.Append(",doctor = " & VN._DR & " ")
                End If

                If VN._CLINICID Is Nothing Then
                    sqlBuilder.Append("WHERE (vn='" & VN._VN & "' AND clinic='" & VN._BCLINIC & "');")
                Else
                    sqlBuilder.Append("WHERE (clinicid = '" & VN._CLINICID & "');")
                End If
            End If

            db.ExecuteNonQuery(sqlBuilder.ToString)
            db.CommitTrans()
            Return True
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)
            Return False
        Finally
            db.Dispose()
            '    iDate.afterSave() 'ปรับวันที่
        End Try
    End Function

    Public Function SendVNAndBClinic(CSTATUS As Integer) As Boolean

        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()

        db.BeginTrans()
        Try
            '  iDate.beforSave() 'ปรับวันที่
            '//////////////////// Insert ลง table masclinicseq ////////////////////
            sqlBuilder.Append("UPDATE  masclinicseq SET `seq` = `seq` +1 ")
            sqlBuilder.Append("WHERE(`clinic` = '" & VN._CLINIC & "');")
            '//////////////////// Insert ลง table frnclinic  ////////////////////
            sqlBuilder.Append("INSERT INTO  frnclinic (`clinic`,`cstatus`,`vn`,`cseq`,`sentime`,`usmk`,`doctor`,`bclinic`, req_card , bclinicid,f_lock,f_finish) VALUES (")
            sqlBuilder.Append("'" & VN._CLINIC & "',")
            sqlBuilder.Append("" & ENVIRONMENTCLASS.MASCSTATUS.wait & ",")
            sqlBuilder.Append("" & VN._VN & ",")
            sqlBuilder.Append("(SELECT seq FROM  masclinicseq WHERE(clinic = '" & VN._CLINIC & "') ),")
            sqlBuilder.Append("curtime(),")
            sqlBuilder.Append("'" & VN._PROVIDERKEY & "',")
            If VN._DR IsNot Nothing And VN._DR <> "" Then
                sqlBuilder.Append("'" & VN._DR & "', ")
            Else
                sqlBuilder.Append("NULL, ")
            End If

            If VN._BCLINIC IsNot Nothing Then
                sqlBuilder.Append("'" & VN._BCLINIC & "' , ")
            Else
                sqlBuilder.Append("NULL ,")
            End If

            sqlBuilder.Append(If(VN._ISREQUESTDOCUMENT = True, " 1 ,", " 0 ,"))
            sqlBuilder.Append("" & VN._BCLINICID & ",")
            sqlBuilder.Append(" 0 ,")
            sqlBuilder.Append(" 0 ")
            sqlBuilder.Append("); SELECT LAST_INSERT_ID();")
            _FRNCLINICID = db.ExecuteScalar(sqlBuilder.ToString)


            sqlBuilder.Clear()
            db.CommitTrans()



            db.BeginTrans()

            '/// Update frnservice
            sqlBuilder.Append("UPDATE  frnservice SET `f_doc` = ")
            If VN._ISREQUESTDOCUMENT = True Then sqlBuilder.Append("1") Else sqlBuilder.Append("0")
            sqlBuilder.Append(" WHERE(VN = " & VN._VN & ");")

            If CSTATUS = ENVIRONMENTCLASS.MASCSTATUS.send Then
                '//////////////////// Update สถานะของ sendclinic ลง table frnclinic  ////////////////////
                sqlBuilder.Append("UPDATE  frnclinic SET ")
                sqlBuilder.Append("cstatus = " & CSTATUS & ",fintime = curtime(),f_finish = 1,usmk = " & VN._PROVIDERKEY & " ")
                If VN._CLINICID Is Nothing Then
                    sqlBuilder.Append("WHERE (vn='" & VN._VN & "' AND clinic='" & VN._BCLINIC & "');")
                Else
                    sqlBuilder.Append("WHERE (clinicid = '" & VN._CLINICID & "');")
                End If
            Else
                sqlBuilder.Append("UPDATE  frnclinic SET ")
                sqlBuilder.Append("cstatus = " & CSTATUS & " ,usmk = " & VN._PROVIDERKEY & " ")

                If CSTATUS = ENVIRONMENTCLASS.MASCSTATUS.diag Then
                    sqlBuilder.Append(",doctor = " & VN._DR & " ")
                End If

                If VN._CLINICID Is Nothing Then
                    sqlBuilder.Append("WHERE (vn='" & VN._VN & "' AND clinic='" & VN._BCLINIC & "');")
                Else
                    sqlBuilder.Append("WHERE (clinicid = '" & VN._CLINICID & "');")
                End If
            End If

            db.ExecuteNonQuery(sqlBuilder.ToString)
            db.CommitTrans()
            Return True
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)
            Return False
        Finally
            db.Dispose()
            '  iDate.afterSave() 'ปรับวันที่
        End Try
    End Function

    Public Sub SendVN(CSTATUS As Integer, typefin As Boolean, Optional IsIPD As Boolean = False)


        Dim sql As String = ""
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()

        db.BeginTrans()
        sqlBuilder.Clear()
        Try
            '  iDate.beforSave() 'ปรับวันที่
            '//////////////////// Insert ลง table masclinicseq ////////////////////
            sqlBuilder.Append("UPDATE  masclinicseq SET `SEQ` = `SEQ` +1 ")
            sqlBuilder.Append("WHERE(`clinic` = '" & VN._CLINIC & "');")
            '//////////////////// Insert ลง table frnclinic  ////////////////////
            If IsIPD = False Then
                sqlBuilder.Append("INSERT INTO  frnclinic(`clinic`,`cstatus`,`vn`,`cseq`,`sentime`,`usmk`,`doctor`,`bclinic`,f_lock) VALUES(")
                sqlBuilder.Append("'" & VN._CLINIC & "',")
                sqlBuilder.Append("" & ENVIRONMENTCLASS.MASCSTATUS.wait & ",")
                sqlBuilder.Append("" & VN._VN & ",")
            Else
                sqlBuilder.Append("INSERT INTO  frnclinic(`clinic`,`cstatus`,`an`,`vn`,`bclinicid`,`cseq`,`sentime`,`usmk`,`doctor`,`bclinic`,f_lock) VALUES(")
                sqlBuilder.Append("'" & VN._CLINIC & "',")
                sqlBuilder.Append("" & ENVIRONMENTCLASS.MASCSTATUS.wait & ",")
                sqlBuilder.Append("" & VN._AN & ",")
                sqlBuilder.Append("" & VN._VN & ",")
                sqlBuilder.Append("" & VN._CLINICID & ",")
            End If
            sqlBuilder.Append("(SELECT SEQ FROM  masclinicseq WHERE(clinic = '" & VN._CLINIC & "') ),")
            sqlBuilder.Append("curtime(),")
            sqlBuilder.Append("'" & VN._PROVIDERKEY & "',")
            If VN._DR IsNot Nothing And VN._DR <> "" Then
                sqlBuilder.Append("'" & VN._DR & "', ")
            Else
                sqlBuilder.Append("NULL, ")
            End If

            If VN._BCLINIC IsNot Nothing Then
                sqlBuilder.Append("'" & VN._BCLINIC & "' ,")
            Else
                sqlBuilder.Append("NULL,")
            End If
            sqlBuilder.Append(" 0 ")
            sqlBuilder.Append("); SELECT LAST_INSERT_ID();")
            _FRNCLINICID = db.ExecuteScalar(sqlBuilder.ToString)


            sqlBuilder.Clear()
            db.CommitTrans()


            '/// Update frnservice
            If typefin = True Then

                db.BeginTrans()
                sqlBuilder.Append("UPDATE  frnservice SET `f_doc` = ")
                If VN._ISREQUESTDOCUMENT = True Then sqlBuilder.Append("1") Else sqlBuilder.Append("0")
                sqlBuilder.Append(" WHERE(VN = " & VN._VN & ");")
                '//////////////////// Update สถานะของ sendclinic ลง table frnclinic  ////////////////////
                sqlBuilder.Append("UPDATE  frnclinic SET ")
                sqlBuilder.Append("cstatus = " & CSTATUS & ",fintime = curtime(),f_finish = 1 ")
                sqlBuilder.Append("WHERE(VN = " & VN._VN & " AND  clinicid = '" & VN._SENDBYCLINIC & "');")
                sqlBuilder.Clear()

                db.ExecuteNonQuery(sqlBuilder.ToString)
                db.CommitTrans()
            Else

            End If

        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            db.Dispose()
            'iDate.afterSave() 'ปรับวันที่
        End Try
    End Sub

    Public Sub SendVNWithBclinicidTypefin(CSTATUS As Integer, typefin As Boolean)
        Dim sql As String = ""
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()

        db.BeginTrans()
        Try
            'iDate.beforSave() 'ปรับวันที่
            '//////////////////// Insert ลง table masclinicseq ////////////////////
            sql += "UPDATE  masclinicseq SET `SEQ` = `SEQ` +1 "
            sql += "WHERE(`clinic` = '" & VN._CLINIC & "');"
            '//////////////////// Insert ลง table frnclinic  ////////////////////
            sql += "INSERT INTO  frnclinic(`clinic`,`cstatus`,`vn`,`cseq`,`sentime`,`usmk`,`doctor`,`bclinic`,`bclinicid`,f_alt,f_lock) VALUES("
            sql += "'" & VN._CLINIC & "',"
            sql += "" & ENVIRONMENTCLASS.MASCSTATUS.wait & ","
            sql += "" & VN._VN & ","
            sql += "(SELECT SEQ FROM  masclinicseq WHERE(clinic = '" & VN._CLINIC & "') ),"
            sql += "curtime(),"
            sql += "'" & VN._PROVIDERKEY & "',"
            If VN._DR IsNot Nothing And VN._DR <> "" Then
                sql += "'" & VN._DR & "', "
            Else
                sql += "NULL, "
            End If

            If VN._BCLINIC IsNot Nothing Then
                sql += "'" & VN._BCLINIC & "' ,"
            Else
                sql += "NULL,"
            End If

            If VN._BCLINICID IsNot Nothing Then
                sql += "'" & VN._BCLINICID & "' ,"
            Else
                sql += "NULL,"
            End If

            If VN._F_ALT = 1 Then
                sql += "1, "
            Else
                sql += "0, "
            End If

            sql += " 0 "
            sql += "); SELECT LAST_INSERT_ID();"
            _FRNCLINICID = db.ExecuteScalar(sql)
            sql = ""
            db.CommitTrans()
            db.Dispose()

            '/// Update frnservice
            If typefin = True Then
                db = ConnecDBRYH.NewConnection
                db.BeginTrans()
                sql += "UPDATE  frnservice SET `f_doc` = "
                If VN._ISREQUESTDOCUMENT = True Then sql += "1" Else sql += "0"
                sql += " WHERE(VN = " & VN._VN & ");"
                '//////////////////// Update สถานะของ sendclinic ลง table frnclinic  ////////////////////
                sql += "UPDATE  frnclinic SET "
                sql += "cstatus = " & CSTATUS & ",fintime = curtime(),f_finish = 1 "
                sql += "WHERE(VN = " & VN._VN & " AND  clinicid = '" & VN._SENDBYCLINIC & "');"
                sql += ""

                db.ExecuteNonQuery(sql)
                db.CommitTrans()
            Else

            End If

        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            db.Dispose()
            '  iDate.afterSave() 'ปรับวันที่
        End Try
    End Sub

    Public Sub CancelVN(ByVal vn As VNStruct)
        Me._VN = vn
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        Dim sql As String = ""
        db.BeginTrans()
        Try
            '/////////////////////////////////////////////////Update frnclinic //////////////////////////////////////
            If Not vn._BCLINICID = Nothing Then
                sql = "UPDATE frnclinic SET cstatus=" & ENVIRONMENTCLASS.MASCSTATUS.wait & ", doctor= NULL, f_finish=0" & " WHERE clinicid=" & _VN._BCLINICID & ";"
                db.ExecuteNonQuery(sql)
            End If

            sql = "UPDATE frnclinic SET cstatus=" & ENVIRONMENTCLASS.MASCSTATUS.cancel & ",usmk = " & _VN._PROVIDERKEY & " WHERE clinicid=" & _VN._CLINICID & ";"
            db.ExecuteNonQuery(sql)
            '////////////////////////////////////////////////End update frnclinic///////////////////////////////////////
            db.CommitTrans()
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            db.Dispose()
            '    iDate.afterSave() 'ปรับวันที่
        End Try
    End Sub

    Public Sub DoctorReceive(clinicID As String)
        Dim sql As String = ""
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        db.BeginTrans()
        Try
            sql += "UPDATE  frnclinic SET "
            sql += "CSTATUS = " & ENVIRONMENTCLASS.MASCSTATUS.diag & " "
            sql += "WHERE(clinicid = '" & clinicID & "');"
            sql += ""
            db.ExecuteNonQuery(sql)
            db.CommitTrans()
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            db.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' ดึงข้อมูล Assetment ตามเลข VN
    ''' </summary>
    ''' <remarks>
    ''' - Input (vnID = เลข VN)
    '''  - Output บันทึก vn ลงฐานข้อมูล
    ''' </remarks>
    ''' <returns>
    ''' - return dt(VN,CHIEFCOMP,FEELID,FEELING AS FEEL,FEELING AS RFEEL,PLIGHT,PLIGHT_NAME,F_VS,MIND,ICTYPE,ISOLATE_NAME,
    '''                    F_ISOLATE,F_MASK,SICK_BB,HIS_OR,CONSULT,CATE,CATE_NAME,STAKE,TAKE_NAME,TYPEIN ,TYPEINDESC )
    ''' * CHIEFCOMP = อาการสำคัญ
    ''' FEELID = รหัสอารม
    ''' FEELING = id สภาพจิตใจ
    ''' PLIGHT = id สภาพอาการ
    ''' PLIGHT_NAME = สภาพอาการ
    ''' F_VS = Flag วัดสัญญาณชีพ
    ''' MIND = support จิตใจ
    ''' ICTYPE = id แยกพื้นที่
    ''' ISOLATE_NAME = =ชื่อแยกพื้นที่
    ''' F_ISOLATE = Flag แยกพื้นที่
    ''' F_MASK = สวมหน้ากาก
    ''' SICK_BB = การชักในเด็ก
    ''' HIS_OR = ผ่าตัด
    ''' CONSULT = คำปรึกษา
    ''' CATE = แคตตาล็อกผู้ป่วยจาก MASCATE
    ''' CATE_NAME = ชื่อประเภท
    ''' STAKE = สถานะการมา
    ''' TAKE_NAME = ชื่อการมา
    ''' TYPEIN = ประเภทการมารับบริการ
    ''' TYPEINDESC = รายละเอียดการมารับบริการ
    ''' </returns>
    Public Function GetAssetment(vnID As String) As DataTable
        Dim dt As DataTable
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String
        sql = "SELECT fs.vn,fs.chiefcomp,fs.feelid,fe.feeling as feel,rfe.feeling as rfeel,"
        sql += "fs.plight,pl.plight_name,fs.f_vs,tdrug,ttdrug,fs.mind,fs.ictype,ic.isolate_name,"
        sql += "fs.f_isolate,fs.f_mask,fs.sick_bb,fs.his_or,fs.consult,fs.cate,c.cate_name,"
        sql += "fs.stake,s.take_name,fs.typein , mastypein.typeindesc,fs.painscore "
        sql += "FROM (SELECT * FROM  frnservice WHERE(vn = '" & vnID.Trim & "')) AS fs "
        sql += "LEFT JOIN  masfeeling AS fe ON fs.feelid = fe.feelid "
        sql += "LEFT JOIN  masfeeling AS rfe ON fs.rfeelid = rfe.feelid "
        sql += "LEFT JOIN (SELECT * FROM  masplight WHERE(`status` = 1)) AS pl ON fs.plight = pl.plight "
        sql += "LEFT JOIN (SELECT * FROM  masictype WHERE(`status` = 1)) AS ic ON fs.ictype = ic.ictype "
        sql += "LEFT JOIN (SELECT * FROM  mascate WHERE(`status` = 1)) AS c ON fs.cate = c.cate "
        sql += "LEFT JOIN (SELECT * FROM  masstake WHERE(`status` = 1)) AS s ON fs.stake = s.stake "
        sql += "LEFT JOIN (SELECT * FROM  mastypein WHERE(`status` = 1)) AS   mastypein ON mastypein.typeinid = fs.typein ; "
        dt = db.GetTable(sql)
        db.Dispose()
        Return dt
    End Function

    ''' <summary>
    ''' ดึงข้อมูลรายละเอียดทั่วไปตามเลข VN
    ''' </summary>
    ''' <remarks>- Input (vnx = เลข VN)</remarks>
    ''' <returns>
    ''' - return dt(VN,HN,DATE_SERV,INTIME,INTIMEDESC,TYPEIN,TYPEINDESC,CHIEFCOMP,URGID,URGENCY,REFERINHOSP,HOSPNAME
    '''                   ,CLINIC,CLINICNAME,CSTATUS,CSTATUSDESC,RECTIME,SENTIME,FINTIME,DOCTOR
    '''                   ,CONCAT(`NAME`,' ',LNAME) AS DOCTORNAME,PROVIDER AS KEYBY,CONCAT(`NAME`,' ',LNAME) AS  KEYBYNAME)
    ''' * DATE_SERV =
    ''' INTIME = รหัสประเภทการมา ในเวลา นอกเวลา
    ''' INTIMEDESC = รายละเอียดประเภทการมา ในเวลา นอกเวลา
    ''' TYPEIN = ประเภทการมารับบริการ
    ''' TYPEINDESC = รายละเอียดการมารับบริการ
    ''' CHIEFCOMP = อาการสำคัญ
    ''' URGID = รหัสความเร่งด่วนในการรักษา
    ''' URGENCY = ชื่อความเร่งด่วนในการรักษา
    ''' REFERINHOSP = รหัสโรงพยาบาลที่ส่งตัวมา
    ''' HOSPNAME = ชื่อโรงพยาบาลที่ส่งตัวมา
    ''' CLINIC = รหัสคลีนิค
    ''' CLINICNAME = รหัสคลีนิค
    ''' CSTATUS = รหัสสถานะคลีนิค
    ''' CSTATUSDESC = คำอธิบายสถานะคลีนิค
    ''' RECTIME = เวลารับบริการ
    ''' SENTIME = เวลาส่งต่อ
    ''' FINTIME = เวลาสิ้นสุด
    ''' DOCTOR = รหัสแพทย์
    ''' CONCAT(`NAME`,' ',LNAME) AS DOCTORNAME = ชื่อแพทย์
    ''' PROVIDER AS KEYBY = รหัสผู้บันทึกรายการ
    ''' CONCAT(`NAME`,' ',LNAME) = ชื่อผู้บันทึกรายการ
    ''' </returns>
    Public Function GetDataTable(vnx As String) As DataTable

        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        Try
            Dim sql As String = "SELECT fs.vn,fs.hn,fs.date_serv,fs.intime,i.intimedesc,fs.typein,t.typeindesc,fs.chiefcomp,fs.urgid,mu.urgency,fs.referinhosp,h.hospname "
            sql += ",fc.clinic,cl.clinicname,fc.cstatus,cs.cstatus AS cstatusdesc,fc.rectime,fc.sentime,fc.fintime,fc.doctor "
            sql += ",CONCAT(dr.`name`,' ',dr.lname) AS doctorname,fc.usmk AS keyby,CONCAT(pv.`name`,' ',pv.lname) AS  keybyname "
            sql += "FROM (SELECT * FROM  frnservice WHERE(vn = " & vnx & ") ) AS fs "
            sql += "LEFT JOIN  frnclinic AS fc on fs.vn = fc.vn "
            sql += "LEFT JOIN  masclinic AS cl ON fc.clinic = cl.clinic "
            sql += "LEFT JOIN  mashosp AS h ON fs.referinhosp = h.hospid "
            sql += "LEFT JOIN (SELECT * FROM  hospemp WHERE(f_dr = 1)) AS dr ON fc.doctor = dr.empid "
            sql += "LEFT JOIN (SELECT * FROM  mastypein WHERE(`status` = 1)) AS t ON fs.typein = t.typeinid "
            sql += "LEFT JOIN (SELECT * FROM  masintime) AS i ON fs.intime = i.intimeid "
            sql += "LEFT JOIN  mascstatus AS cs ON fc.cstatus = cs.id "
            sql += "LEFT JOIN  masurgency AS mu ON fs.urgid = mu.urgid "
            sql += ""

            dt = db.GetTable(sql)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        db.Dispose()
        Return dt
    End Function

    ''' <summary>
    ''' ดึงข้อมูลและนำข้อมูลไปเก็บใน Property
    ''' </summary>
    ''' <remarks>
    ''' - Input (vnx  = เลข VN,clinicID = รหัสคลีนิค)
    '''   - Output ดึงข้อมูลตาม VN ไปเก็บไว้ใน Property ของ Class
    ''' </remarks>
    Public Sub GetData(vnx As String, clinicID As String)
        Dim sql As String = "SELECT fs.vn, fs.pstatus,ps.ps_name,fs.hn,fs.date_serv,fs.intime,i.intimedesc,fs.typein,t.typeindesc,fs.referinhosp,fs.f_doc,fs.stake,h.hospname "
        sql += ",fc.clinic,cl.clinicname,fc.cstatus,cs.cstatus AS cstatusdesc,fc.rectime,fc.sentime,fc.fintime,fc.doctor,fc.bclinic "
        sql += ",CONCAT(dr.`name`,' ',dr.lname) AS doctorname,fc.usmk AS keyby,CONCAT(pv.`name`,' ',pv.lname) AS  keybyname , dr.dcl ,painscore "

        sql += "FROM (SELECT vn,pstatus,hn,date_serv,intime,typein ,referinhosp,f_doc , stake ,painscore   FROM  frnservice WHERE(vn = " & vnx & ") ) AS fs "
        sql += "LEFT JOIN (SELECT * FROM  frnclinic WHERE(vn = " & vnx & " AND clinicid = " & clinicID & ")) AS fc ON fs.vn = fc.vn "
        sql += "LEFT JOIN  masclinic AS cl ON fc.clinic = cl.clinic "
        sql += "LEFT JOIN  mashosp AS h ON fs.referinhosp = h.hospid "
        sql += "LEFT JOIN (SELECT * FROM  hospemp WHERE(f_dr = 1)) AS dr ON fc.doctor = dr.empid "
        sql += "LEFT JOIN (SELECT * FROM  mastypein WHERE(`status` = 1)) AS t ON fs.typein = t.typeinid "
        sql += "LEFT JOIN (SELECT * FROM  masintime) AS i ON fs.intime = i.intimeid "
        sql += "LEFT JOIN (SELECT * FROM maspstatus WHERE `status` = 1) AS ps ON fs.pstatus = ps.pstatus "
        sql += "LEFT JOIN  mascstatus AS cs ON fc.cstatus = cs.id "
        sql += "LEFT JOIN  hospemp AS pv ON fc.usmk = pv.empid "
        sql += ""
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()

        Try
            dt = db.GetTable(sql)
            If dt.Rows.Count > 0 Then
                VN._VN = dt.Rows(0)(0).ToString
                VN._HN = dt.Rows(0)(3).ToString
                VN._DATE_SERV = dt.Rows(0)(4).ToString
                VN._INTIME = dt.Rows(0)(5).ToString
                VN._INTIMEDESC = dt.Rows(0)(6).ToString
                VN._TYPEIN = dt.Rows(0)(7).ToString
                VN._TYPEINDESC = dt.Rows(0)(8).ToString
                VN._REFERINHOSP = dt.Rows(0)(9).ToString
                VN._REFERINHOSPDESC = dt.Rows(0)(12).ToString
                VN._CLINIC = dt.Rows(0)(13).ToString
                VN._CLINICNAME = (dt.Rows(0)(14).ToString)
                VN._CLINICSTATUS = dt.Rows(0)(15).ToString
                VN._CLINICSTATUSDESC = dt.Rows(0)(16).ToString
                VN._RECTIME = dt.Rows(0)(17).ToString
                VN._SENTIME = dt.Rows(0)(18).ToString
                VN._FINTIME = dt.Rows(0)(19).ToString
                VN._DR = dt.Rows(0)(20).ToString
                VN._DRNAME = dt.Rows(0)(22).ToString
                VN._KEYBY = dt.Rows(0)(23).ToString
                VN._KEYBYNAME = dt.Rows(0)(24).ToString
                VN._ISREQUESTDOCUMENT = Convert.ToBoolean(dt.Rows(0)(10).ToString)
                VN._STAKE = dt.Rows(0)(11).ToString
                VN._BCLINIC = dt.Rows(0)(21).ToString
                '   VN._PSTATUS = FINANCECLASS.Convert2Int(dt.Rows(0)(1))
                '   VN._PSTATUSNAME = FINANCECLASS.Convert2Int(dt.Rows(0)(2))
                VN.painscore = If(IsDBNull(dt.Rows(0)(26).ToString), "", dt.Rows(0)(26).ToString)
                VN.dcl = If(IsDBNull(dt.Rows(0)(25).ToString), "", dt.Rows(0)(25).ToString)
                VN.doctor = If(IsDBNull(dt.Rows(0)(20).ToString), "", dt.Rows(0)(20).ToString)

            Else
                DataClear()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        db.Dispose()
    End Sub

    ''' <summary>
    ''' เคลียร์ข้อมูลที่เก็บใน Property
    ''' </summary>
    ''' <remarks>- Output เคลียร์ข้อมูลจาก Property</remarks>
    Private Sub DataClear()
        VN._HN = Nothing
        VN._VN = Nothing
        VN._DATE_SERV = Nothing
        VN._TYPEIN = Nothing
        VN._TYPEINDESC = Nothing
        VN._INTIME = Nothing
        VN._INTIMEDESC = Nothing
        VN._REFERINHOSP = Nothing
        VN._REFERINHOSPDESC = Nothing
        VN._CLINIC = Nothing
        VN._CLINICNAME = Nothing
        VN._CLINICSTATUS = Nothing
        VN._CLINICSTATUSDESC = Nothing
        VN._SENDBYCLINIC = Nothing
        VN._DR = 0
        VN._DRNAME = Nothing
        VN._PROVIDERKEY = 0
        VN._ISREQUESTDOCUMENT = False
        VN._RECTIME = Nothing
        VN._SENTIME = Nothing
        VN._FINTIME = Nothing
        VN._ISNEWPATIENT = Nothing
        VN._BCLINIC = Nothing

        VN = Nothing
    End Sub
    ''' <summary>
    ''' ดึงข้อมูลการแจ้งเตือนของคนใข้แต่ละคน
    ''' </summary>
    ''' <remarks>
    ''' - Input (hn = เลข HN,vn = เลข VN)
    '''  - Output แสดงข้อมูลใน dgvcomment
    ''' </remarks>
    ''' <returns>
    ''' - return dt(HN,TITLE,COMMENT,ALTDATE,COLOR,PALTID)
    ''' TITLE = ชื่อเรื่อง
    ''' COMMENT = คำอธิบาย
    ''' ALTDATE = วันที่แจ้งเตือน
    ''' COLOR = รหัสสี
    ''' PALTID = Id แจ้งเตือน
    ''' </returns>
    Public Function Showdgvcomment(ByRef hn As String, ByRef VN As String) As DataTable
        ConnectionDB = ConnecDBRYH.NewConnection
        Dim commandSql As String
        Dim dt As DataTable = Nothing
        Try
            commandSql = "Select PN.hn,PN.title,PN.comment,PN.altdate,PN.color,PN.paltid"
            commandSql += " FROM (SELECT * FROM  pnalert  WHERE hn = '" & hn & "' and fcancel = 0 ) as PN "
            commandSql += " JOIN (SELECT * FROM  pnalertclinic WHERE clinicid = " + VN + ") as CL ON PN.paltid = CL.paltid ; "
            dt = ConnectionDB.GetTable(commandSql)
        Catch ex As Exception

        End Try
        Return dt
    End Function
    ''' <summary>
    ''' Generate SQl สำหรับดึงข้อมูล Vitalsign
    ''' </summary>
    ''' <remarks>
    ''' - Input (hn = เลข HN,vn = เลข VN)
    '''  - Output สร้างคำสั่ง SQL
    ''' </remarks>
    ''' <returns>ชุดคำสั่ง SQL</returns>
    Public Shared Function getVitalsign(ByVal vnID As String, ByVal hnID As String) As String
        'ยังขาด อารม( masfeeling)ผูกกับ Frnservice,โรคประจำตัว( condisease)
        Dim sql As String = "SELECT opd.VSID,RESULTVS,VSDESC,LL,HL"
        sql += ",p.NATION,mna.NATIONDESC,fl.FEELING,pc.TELEPHONE AS TELEPHONEPC,pc.MOBILE AS MOBILEPC"
        sql += ",dru.DRUGALLERGY AS 'แพ้ยา',dst.DISEASE AS 'โรคประจำตัว' "
        sql += "FROM (SELECT * FROM  frnvsopd WHERE(VN = " & vnID & ") ) AS opd "
        sql += "LEFT JOIN  (SELECT * FROM  masvstype WHERE(`STATUS` = 1)) AS vst on opd.VSID = vst.VSID "
        sql += "LEFT JOIN  frnservice AS se ON opd.VN = se.VN "
        sql += "LEFT JOIN  masfeeling AS fl ON se.FEELID = fl.FEELID "
        sql += "LEFT JOIN (SELECT * FROM  person WHERE(HN = " & hnID & " AND `STATUS` = 1))  AS p ON se.HN = p.HN "
        sql += "LEFT JOIN (SELECT * FROM  masnation WHERE(`STATUS` = 1)) AS mna ON p.NATION = mna.NATION "
        sql += "LEFT JOIN (SELECT * FROM  personcontact) AS pc ON p.HN = pc.HN "
        sql += "LEFT JOIN (SELECT HN, GROUP_CONCAT(DRUGALLERGY SEPARATOR ',') AS DRUGALLERGY "
        sql += "FROM  drugallergy GROUP BY HN) AS dru ON p.HN = dru.HN "
        sql += "LEFT JOIN (SELECT HN, GROUP_CONCAT(DISEASE SEPARATOR ',') AS DISEASE "
        sql += "FROM  condisease WHERE(`STATUS` = 1) GROUP BY HN) AS dst ON p.HN = dst.HN "
        sql += ""

        Return sql
        'อันเดิมของจูน
        'Return "SELECT frnvsopd.VSID,RESULTVS,VSDESC,LL,HL FROM  frnvsopd join masvstype on frnvsopd.VSID = masvstype.VSID where vn ='" & vnID & "';"
    End Function

    ''' <summary>
    ''' Generate SQl สำหรับดึงข้อมูล Vitalsign
    ''' </summary>
    ''' <remarks>
    ''' - Input (hn = เลข HN,IDVITAL = ID การวัดชีพ)
    '''  - Output สร้างคำสั่ง SQL
    ''' </remarks>
    ''' <returns>ชุดคำสั่ง SQL</returns>
    Public Shared Function GetVitalsignGraph(ByRef HN As String, ByRef IDVITAL As String)

        Dim sql As String = "SELECT frnvsopd.VSID,RESULTVS,VSDESC,LL,HL,frnservice.vn AS VN "
        sql += " FROM  frnvsopd join masvstype on frnvsopd.VSID = masvstype.VSID "
        sql += " JOIN frnservice on frnservice.VN = frnvsopd.vn  "
        sql += " WHERE HN = '" & HN & "' AND masvstype.VSID  ='" & IDVITAL & "' ORDER BY DATE_SERV;"
        Return sql
    End Function
    ''' <summary>
    ''' Generate SQL สำหรับดึงข้อมูล Vitalsign
    ''' </summary>
    ''' <remarks>
    ''' - Input (vnID = เลข VN)
    '''  - Output สร้างคำสั่ง SQL
    ''' </remarks>
    ''' <returns>ชุดคำสั่ง SQL</returns>
    Public Shared Function getVitalSign2(ByVal vnID As String) As String
        'อันนี้ใช้หน้า NCU000
        Return "SELECT frnvsopd.VSID,RESULTVS as Result,VSDESC as Item,LL as Low,HL as High FROM  frnvsopd join masvstype on frnvsopd.VSID = masvstype.VSID where vn ='" & vnID & "';"

    End Function
    ''' <summary>
    ''' Generate SQl สำหรับดึงข้อมูล Vitalsign โดยภาพรวมไม่แยก VN
    ''' </summary>
    ''' <remarks>- Output  สร้างคำสั่ง SQL</remarks>
    ''' <returns>ชุดคำสั่ง SQL</returns>
    Public Shared Function getMasVitalSign()
        Return "SELECT VSID AS ID ,VSDESC AS NAME,LL,HL,UNIT FROM  masvstype WHERE STATUS ='1';"
    End Function
    ''' <summary>
    ''' ดึงข้อมูลทั่วไปของการเปิด VN
    ''' </summary>
    ''' <remarks>
    ''' - Input (VN = เลข VN)
    '''  - Output  สร้างคำสั่ง SQL
    ''' </remarks>
    ''' <returns>ชุดคำสั่ง SQL</returns>
    Public Shared Function GETDATAFRNSERVICE(ByVal VN As String) As DataTable
        Dim sql As String
        sql = "SELECT F_DISCHARGE,F_DIAG,F_DOC,PLIGHT,F_VS,TDRUG,TTDRUG,MIND,RFEELID,ICTYPE,F_ISOLATE,F_MASK,SICK_BB"
        sql += "HIS_OR,CONSULT,CATE,STAKE FROM   frnservice WHERE VN = '" & VN & "';"
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As DataTable = New DataTable
        dt = db.GetTable(sql)
        db.Dispose()
        Return dt
    End Function

    ''' <summary>
    ''' เช็ค VN ว่ามีในระบบแล้วหรือเปล่าถ้ามีจะดึง VN ออกมา
    ''' </summary>
    ''' <remarks>- Input (HN = เลข HN)</remarks>
    ''' <returns>เลข VN ที่มีในระบบ</returns>
    Public Shared Function GetVNCodeAndCheckUserGetOld(ByRef txtHN As String) As String
        Dim vnCode As String = ""
        Dim sql As String = ""
        Dim dt As New DataTable()
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        '//////////////////////// หา VNCode ////////////////////////

        sql = "SELECT VN "
        sql += "FROM  frnservice "
        sql += "WHERE(HN = '" + txtHN.Trim() + "') AND (CAST(DATE_SERV AS date) = curdate())"
        dt = db.GetTable(sql)
        If (dt.Rows.Count > 0) Then
            vnCode = dt.Rows(0)("VN").ToString.Trim()
        Else
            MsgBox("ไม่พบ Record เลข VN เก่า")
        End If

        db.Dispose()
        Return vnCode
    End Function

    ''' <summary>
    ''' สร้างเลข VN ใหม่
    ''' </summary>
    ''' <remarks>- Output ได้เลข VN ใหม่</remarks>
    ''' <returns>เลข VN ใหม่</returns>
    Public Shared Function GetVNCode() As String
        Dim vnCode As String = Nothing
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        Dim sql As String = "SELECT VN FROM  frnservice WHERE(CAST(DATE_SERV AS date) = curdate()) ORDER BY VN DESC LIMIT 1;"
        Dim headCode, runingCode As String
        db.BeginTrans()
        Try
            dt = db.GetTable(sql)
            If dt.Rows.Count > 0 Then
                'vnCode = (Convert.ToInt32(dt.Rows(0)(0)) + 1).ToString()
                runingCode = dt.Rows(0)(0).ToString.Remove(0, 6)
                headCode = dt.Rows(0)(0).ToString.Replace(runingCode, "")
                vnCode = headCode & (Convert.ToInt32(runingCode) + 1).ToString("0000")
            Else
                sql = "SELECT CURDATE();"
                dt = db.GetTable(sql)
                '   Thread.CurrentThread.CurrentCulture = New CultureInfo(MAINFRI.CULTURE)
                vnCode = Convert.ToDateTime(dt.Rows(0)(0)).ToString("yy") & Convert.ToDateTime(dt.Rows(0)(0)).ToString("MM") & Convert.ToDateTime(dt.Rows(0)(0)).ToString("dd") & "0001"
            End If
            db.CommitTrans()
        Catch ex As MySqlException
            db.RollbackTrans()
            MsgBox(ex.ToString)

        Finally
            db.Dispose()
        End Try
        Return vnCode
    End Function
    Public Shared Function GetVNCodeHN(ByRef HN As String) As String
        Dim vnCode As String = Nothing
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        Dim sql As String = "SELECT VN FROM  frnservice WHERE(CAST(DATE_SERV AS date) = curdate()) WHERE HN ='" & HN & "' ORDER BY VN DESC LIMIT 1;"
        Dim headCode, runingCode As String
        db.BeginTrans()
        Try
            dt = db.GetTable(sql)
            If dt.Rows.Count > 0 Then
                'vnCode = (Convert.ToInt32(dt.Rows(0)(0)) + 1).ToString()
                runingCode = dt.Rows(0)(0).ToString.Remove(0, 6)
                headCode = dt.Rows(0)(0).ToString.Replace(runingCode, "")
                vnCode = headCode & (Convert.ToInt32(runingCode) + 1).ToString("0000")
            Else
                sql = "SELECT CURDATE();"
                dt = db.GetTable(sql)
                '    Thread.CurrentThread.CurrentCulture = New CultureInfo(MAINFRI.CULTURE)
                vnCode = Convert.ToDateTime(dt.Rows(0)(0)).ToString("yy") & Convert.ToDateTime(dt.Rows(0)(0)).ToString("MM") & Convert.ToDateTime(dt.Rows(0)(0)).ToString("dd") & "0001"
            End If
            db.CommitTrans()
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)

        Finally
            db.Dispose()
        End Try
        Return vnCode
    End Function

    ''' <summary>
    ''' บันทึกค่า Vitasign จากการคีย์ของ User ลงในระบบ
    ''' </summary>
    ''' <remarks>
    ''' - Input (dgv = datagrid ที่จะแสดง Vitasign, vn = หมายเลข VN, hn หมายเลข HN)
    ''' - Output บันทึกค่า Vitasign จากการคีย์ของ User ลงในระบบ
    ''' </remarks>
    Public Shared Sub saveVitalSign(dgv As SuperGridControl, vn As String, hn As String)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

        If dgv.PrimaryGrid.Rows.Count > 1 Then

            '    Dim datejaja As New FriendsDate()
            ' datejaja.beforSave()
            '
            Dim dtVitalSignID As DataTable
            dtVitalSignID = db.GetTable("SELECT VSID, VSDESC FROM masvstype WHERE STATUS = '1'")

            Dim sqlRow As String = ""

            For i As Integer = 0 To dgv.PrimaryGrid.Rows.Count - 2
                If db.GetTable("SELECT MVSID FROM frnvsopd WHERE MVSID = '" & dgv.GetCell(i, dgv.PrimaryGrid.Columns("idVS").ColumnIndex).Value & "'").Rows.Count = 0 Then
                    'MsgBox("ใส่" & dgv.GetCell(i, dgv.PrimaryGrid.Columns("idVS").ColumnIndex).Value)
                    For j As Integer = 0 To dgv.PrimaryGrid.Columns.Count - 1   'loop หัวตาราง
                        'MsgBox(dgv.GetCell(i, j).Value.ToString)               'ค่าในตาราง
                        'MsgBox(dgv.GetCell(i, j).GridColumn.Name.ToString)     'ชื่อ column
                        If dgv.GetCell(i, j).Value.ToString <> "" Then
                            For k As Integer = 0 To dtVitalSignID.Rows.Count - 1    'loop ชื่อvitalsign
                                If dgv.GetCell(i, j).GridColumn.Name.ToString <> "BP (Systolic)" Or dgv.GetCell(i, j).GridColumn.Name.ToString <> "BP (Diastolic)" Then   'ชื่อคอลัมน์ที่ไม่ต้องการเอาไปประมวลผลให้ใส่ไว้ที่นี่
                                    If dgv.GetCell(i, j).GridColumn.Name.ToString = dtVitalSignID.Rows(k)(1) Or dgv.GetCell(i, j).GridColumn.Name.ToString = "B/P" Then     'check ว่าชื่อคอลัมน์ตรงกับชื่อ vital sign และเช็คว่าเป็น B/P หรือไม่
                                        'MsgBox(dgv.GetCell(i, j).GridColumn.Name.ToString) 'ถ้าตรงกับชื่อ vitalsign
                                        If dgv.GetCell(i, j).GridColumn.Name.ToString = "B/P" Then
                                            ' กรณีคอลัมน์ชื่อ B/P
                                            'If dgv.GetCell(i, j).GridColumn.Name.ToString.IndexOf("/") Then     'ตรวจว่ามีการใส่เครื่องหมาย / หรือไม่ ถ้าไม่ใส่จะออกจาก sub
                                            If dgv.GetCell(i, j).Value.ToString.Contains("/") Then     'ตรวจว่ามีการใส่เครื่องหมาย / หรือไม่ ถ้าไม่ใส่จะออกจาก sub
                                                Dim arr As Array = dgv.GetCell(i, j).Value.ToString.Split("/")

                                                sqlRow += "INSERT INTO frnvsopd (VN, VSID, RESULTVS, PROVIDER, D_UPDATE) VALUES ('" & vn & "', '4', '" & arr(0) & "', '4444', NOW());"
                                                sqlRow += "INSERT INTO frnvsopd (VN, VSID, RESULTVS, PROVIDER, D_UPDATE) VALUES ('" & vn & "', '5', '" & arr(1) & "', '4444', NOW());"

                                                Exit For
                                            Else
                                                MsgBox("ไม่สามารถบันทึก Vital Sign ได้, โปรดกรุณาระบุเครื่องหมาย / ในค่า B/P")
                                                Exit Sub
                                            End If
                                        Else
                                            ' กรณีคอลัมน์ชื่ออื่นๆ
                                            sqlRow += "INSERT INTO frnvsopd (VN, VSID, RESULTVS, PROVIDER, D_UPDATE) VALUES ('" & vn & "', '" & dtVitalSignID.Rows(k)(0) & "', '" & dgv.GetCell(i, j).Value.ToString & "', '4444', NOW());"
                                            Exit For
                                        End If

                                    End If
                                End If
                            Next
                        End If
                    Next
                End If
            Next

            Try
                db.BeginTrans()
                db.ExecuteNonQuery(sqlRow)
                db.CommitTrans()
                '   datejaja.afterSave()
                'MsgBox("บันทึกสัญญาณชีพเรียบร้อยแล้ว")
                selectERVitalGird(hn, vn, dgv)
            Catch ex As Exception
                db.RollbackTrans()
                MsgBox(ex.ToString)
            Finally
                db.Dispose()
            End Try
            'MsgBox(sqlRow)
        End If
    End Sub

    ''' <summary>
    ''' แสดงข้อมูล VitaSign ลงในฐานข้อมูล
    ''' </summary>
    ''' <remarks>
    ''' - Input (hn หมายเลข HN,vn = หมายเลข VN, dgv = datagrid ที่จะแสดง Vitasign)
    '''   - Output แสดงข้อมูล Vitasign จากฐานข้อมูลตาม VN
    ''' </remarks>
    Public Shared Sub selectERVitalGird(hn As String, vn As String, dgv As SuperGridControl)
        'Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

        'Dim selectTorefreshgrid As DataTable
        'selectTorefreshgrid = db.GetTable("SELECT `ERVITAL_ID` AS 'IDERVITAL', `VSTIME` AS 'Time', `BPP` AS 'BP/P', `RT` AS 'R/T', `O2SATPAIN` AS 'O2Sat / Pain Score', `COMANEWS` AS 'Coma Score / News', `DETAIL` AS 'รายละเอียด / อาการผู้ป่วย', `NURSECARE` AS 'Nursing Care' FROM frnervital WHERE VN = '" & vn & "' AND HN = '" & hn & "'")
        'dgv.PrimaryGrid.DataSource = selectTorefreshgrid

        'Dim datejaja As New FriendsDate()
        '  datejaja.culTure_EN()
        '   datejaja.TO_EN_US()

        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

        Dim dt As DataTable
        dt = db.GetTable("SELECT * FROM masvstype WHERE STATUS = '1'")

        Dim sql As String = "SELECT "
        sql += "b.MVSID AS 'idVS', "
        sql += "b.D_UPDATE AS 'วันที่ทำการตรวจ', "

        For i As Integer = 0 To dt.Rows.Count - 1
            sql += "MAX(CASE WHEN VSDESC = '" & dt.Rows(i)("VSDESC") & "' THEN b.RESULTVS END ) AS '" & dt.Rows(i)("VSDESC") & "',"
        Next

        sql += "CONCAT_WS('/', MAX(CASE WHEN VSDESC = 'BP (Systolic)' THEN b.RESULTVS END ), MAX(CASE WHEN VSDESC = 'BP (Diastolic)' THEN b.RESULTVS END )) AS 'B/P', "
        sql += "CONCAT_WS(' ', c.`NAME`, c.`LNAME`) AS 'ผู้ตรวจ' "
        sql += "FROM masvstype a "
        sql += "LEFT JOIN frnvsopd b ON a.VSID = b.VSID "
        sql += "LEFT JOIN provider c ON b.PROVIDER = c.PROVIDER "
        sql += "WHERE b.VN = '" & vn & "' AND a.STATUS = '1' GROUP BY b.D_UPDATE ORDER BY b.D_UPDATE DESC"

        Dim vsDatatable As DataTable
        vsDatatable = db.GetTable(sql)
        dgv.PrimaryGrid.DataSource = vsDatatable

        'datejaja.culTure_EN()
        'datejaja.TO_EN_US()
    End Sub

    ''' <summary>
    ''' Update VitaSign ในฐานข้อมูลตาม VN ใน Property
    ''' </summary>
    ''' <remarks>- Output Update Vitasign ในฐานข้อมูล</remarks>
    Public Sub updateERPatient()
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

        'update frnservice
        Dim sql As String

        sql = "UPDATE frnservice SET "

        If VN._URGID = Nothing Then
            sql += "URGID = '99', "
        Else
            sql += "URGID = '" & VN._URGID & "', "
        End If

        If VN._TYPEIN = Nothing Then
            sql += "TYPEIN = NULL, "
        Else
            sql += "TYPEIN = '" & VN._TYPEIN & "', "
        End If

        If VN._REFERINHOSP = Nothing Then
            sql += "REFERINHOSP = NULL, "
        Else
            sql += "REFERINHOSP = '" & VN._REFERINHOSP & "', "
        End If

        If VN._CHIEFCOMP = Nothing Then
            sql += "CHIEFCOMP = NULL "
        Else
            sql += "CHIEFCOMP = '" & VN._CHIEFCOMP & "' "
        End If

        sql += "WHERE VN = '" & VN._VN & "' AND HN = '" & VN._HN & "';"

        Try
            db.BeginTrans()
            db.ExecuteNonQuery(sql)
            db.CommitTrans()
            MsgBox("บันทึกข้อมูลผู้ป่วยเรียบร้อยแล้ว")
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            db.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' ระบุ HN ไห้ผู้ป่วยกรณีที่ไม่รู้ HN ในตอนแรก
    ''' </summary>
    ''' <remarks>
    ''' - Input (hn = หมายเลข HN, vn หมายเลข VN)
    '''   - Output Update field HN ในฐานข้อมูล
    ''' </remarks>
    Public Sub giveAnonymousHN(ByVal hn As String, ByVal vn As String)
        Dim db = ConnecDBRYH.NewConnection
        Dim sql As String

        sql = "UPDATE frnservice SET HN = '" & hn & "' WHERE VN = '" & vn & "';"
        sql += "UPDATE frnervital SET HN = '" & hn & "' WHERE VN = '" & vn & "';"
        'sql += "UPDATE frnaccident SET HN = '" &  & "' WHERE VN = '" &  & "';"

        Try
            db.BeginTrans()
            db.ExecuteNonQuery(sql)
            db.CommitTrans()
            MsgBox("ระบุ HN ให้ผู้ป่วยสำเร็จ")
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            db.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' ดึงข้อมูล VN โดยใช้เลข HN
    ''' </summary>
    ''' <remarks>
    ''' - Input (HN = เลข HN,ClinicID = รหัสคลีนิค)
    '''  - Output ข้อมูล VN
    ''' </remarks>
    ''' <returns>
    ''' - return dt(hn,vn,cstatus,f_finish)
    ''' * hn = หมายเลข HN
    ''' vn = หมาบเลข VN
    ''' cstatus = รหัสสถานะคลีนิค
    ''' f_finish = สถานะการเสร็จสิ้นกระบวน
    ''' </returns>
    Public Function CheckVN_byHN(ByVal HN As String, ByVal Clinic As String)
        'example
        'Dim VNstr As New VNCLASS
        'VNstr.CheckVN_byHN(HN, txtClinicName.Tag)
        'VNstr._VN._VN

        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As DataTable = Nothing

        Try
            Dim sql As String = "SELECT hn,x.vn,cstatus,clinicid,f_finish "
            sql += "FROM (SELECT DISTINCT HN,VN FROM frnservice s WHERE s.f_discharge = 1 AND hn = " & HN & ") x "
            sql += "JOIN (SELECT vn,cstatus,f_finish,clinicid FROM frnclinic WHERE clinic=" & Clinic & ") y ON x.vn = y.vn;"
            dt = db.GetTable(sql)

            If dt.Rows.Count > 0 Then
                VN._VN = dt.Rows(0).Item("vn")
                VN._HN = HN
                If IsDBNull(dt.Rows(0).Item("cstatus")) = False Then
                    VN._CSTATUS = dt.Rows(0).Item("cstatus")
                    If dt.Rows(0).Item("cstatus") >= 5 Then
                        VN._ERRMSG = ENVIRONMENTCLASS.MASCSTATUS.msgfinish
                    End If
                Else
                    'ไม่ได้มาคลินิกนี้
                    VN._CSTATUS = 0
                    VN._ERRMSG = ENVIRONMENTCLASS.MASCSTATUS.msgnoclinic
                End If

                If IsDBNull(dt.Rows(0).Item("f_finish")) = False Then
                    VN._CINICFINISH = If(dt.Rows(0).Item("f_finish") = 1, True, False)
                End If

                ' หา VN ปัจจุบันที่ f_finish =  false
                For i As Integer = 0 To dt.Rows.Count - 1
                    If dt.Rows(i)("f_finish") IsNot Nothing Then
                        If dt.Rows(i)("f_finish") IsNot DBNull.Value Then
                            If Convert.ToBoolean(dt.Rows(i)("f_finish")) = False Then
                                VN._VN = dt.Rows(i).Item("vn").ToString.Trim
                                VN._CLINICID = dt.Rows(i)("clinicid").ToString.Trim
                                Exit For
                            End If
                        End If
                    End If
                Next i

                ' หา VN ปัจจุบันที่ cstatus =  1 (ความสำคัญสูงกว่า)
                For i As Integer = 0 To dt.Rows.Count - 1
                    '  If FINANCECLASS.Convert2Int(dt.Rows(i)("cstatus")) = 1 Then
                    VN._VN = dt.Rows(i).Item("vn").ToString.Trim
                    VN._CLINICID = dt.Rows(i)("clinicid").ToString.Trim
                    '  Exit For
                    '  End If
                Next i
            Else
                'ไม่ได้เปิด VN
                VN._ERRMSG = ENVIRONMENTCLASS.MASCSTATUS.msgnotvn
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        Return dt

    End Function

    Public Function CheckVN_ByClinicID(ByVal ClinicID As String)
        'example
        'Dim VNstr As New VNCLASS
        'VNstr.CheckVN_byHN(HN, txtClinicName.Tag)
        'VNstr._VN._VN

        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As DataTable = Nothing

        Try
            Dim sql As String = "SELECT hn,x.vn,cstatus,f_finish "
            sql += "FROM (SELECT DISTINCT HN,VN FROM frnservice s ) x "
            sql += "INNER JOIN (SELECT vn,cstatus,f_finish FROM frnclinic WHERE clinicid=" & ClinicID & ") y ON x.vn = y.vn;"
            dt = db.GetTable(sql)

            If dt.Rows.Count > 0 Then
                VN._VN = dt.Rows(0).Item("vn")

                If IsDBNull(dt.Rows(0).Item("cstatus")) = False Then
                    VN._CSTATUS = dt.Rows(0).Item("cstatus")
                    If dt.Rows(0).Item("cstatus") >= 5 Then
                        VN._ERRMSG = ENVIRONMENTCLASS.MASCSTATUS.msgfinish
                    End If
                Else
                    'ไม่ได้มาคลินิกนี้
                    VN._CSTATUS = 0
                    VN._ERRMSG = ENVIRONMENTCLASS.MASCSTATUS.msgnoclinic
                End If

                If IsDBNull(dt.Rows(0).Item("f_finish")) = False Then
                    VN._CINICFINISH = If(dt.Rows(0).Item("f_finish") = 1, True, False)
                End If
            Else
                'ไม่ได้เปิด VN
                VN._ERRMSG = ENVIRONMENTCLASS.MASCSTATUS.msgnotvn
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        Return dt

    End Function

    Public Function CheckVn_Frnservice(ByRef HN As String)
        ConnectionDB = ConnecDBRYH.NewConnection
        Dim dt As DataTable = Nothing
        Dim sql As String
        Try
            sql = "SELECT `vn` FROM frnservice WHERE `f_discharge` = 1 AND `hn` = " & HN & ";"
            dt = ConnectionDB.GetTable(sql)
            If dt.Rows.Count > 0 Then
                VN._VN = dt.Rows(0)("vn")
            End If

        Catch ex As Exception
        End Try
        ConnectionDB.Dispose()

        Return dt

    End Function
    Public Function CheckVn_Frnservice_Death(ByRef HN As String)
        ConnectionDB = ConnecDBRYH.NewConnection
        Dim dt As DataTable = Nothing
        Dim sql As String
        Try
            sql = "SELECT `vn` FROM frnservice WHERE `hn` = " & HN & " ORDER BY `vn` DESC LIMIT 1;"
            dt = ConnectionDB.GetTable(sql)
            If dt.Rows.Count > 0 Then
                VN._VN = dt.Rows(0)("vn")

            End If

        Catch ex As Exception
            ConnectionDB.Dispose()
        End Try

        Return dt

    End Function

    ''' <summary>
    ''' ผูกคู่สัญญาไห้กับผู้ป่วยตาม VN
    ''' </summary>
    ''' <remarks>
    ''' - Input (hn = หมายเลข HN, vn หมายเลข VN)
    '''   - Output Update field HN ในฐานข้อมูล
    ''' </remarks>
    Public Sub AddContectVN(ByVal HN As String, ByVal VN As String)
        Dim db = ConnecDBRYH.NewConnection
        '  Dim datejaja As New FriendsDate
        Dim sql As String

        Try

            sql = "SELECT * FROM  contact WHERE hn='" & HN & "' AND status='1';"
            Dim dt As DataTable = db.GetTable(sql)

            db.BeginTrans()
            '    datejaja.beforSave()
            For i = 0 To dt.Rows.Count - 1
                Dim mtrgtid As String = If(IsDBNull(dt.Rows(i).Item("mtrgtid")), "null", dt.Rows(i).Item("mtrgtid"))
                Dim sbrgtid As String = If(IsDBNull(dt.Rows(i).Item("sbrgtid")), "null", dt.Rows(i).Item("sbrgtid"))
                Dim strgtid As String = If(IsDBNull(dt.Rows(i).Item("strgtid")), "null", dt.Rows(i).Item("strgtid"))
                Dim insid As String = If(IsDBNull(dt.Rows(i).Item("insid")), "null", dt.Rows(i).Item("insid"))
                Dim seqcon As String = If(IsDBNull(dt.Rows(i).Item("seqcon")), "null", dt.Rows(i).Item("seqcon"))
                Dim startdate As String = If(IsDBNull(dt.Rows(i).Item("startdate")), "null", Convert.ToDateTime(dt.Rows(i).Item("startdate")).ToString("yyyy-MM-dd"))
                Dim expiredate As String = If(IsDBNull(dt.Rows(i).Item("expiredate")), "null", Convert.ToDateTime(dt.Rows(i).Item("expiredate")).ToString("yyyy-MM-dd"))
                Dim amount As String = If(IsDBNull(dt.Rows(i).Item("amount")), "null", dt.Rows(i).Item("amount"))
                Dim pconid As String = If(IsDBNull(dt.Rows(i).Item("pconid")), "null", dt.Rows(i).Item("pconid"))
                Dim f_vw As String = If(IsDBNull(dt.Rows(i).Item("f_vw")), "null", dt.Rows(i).Item("f_vw"))
                Dim tmpid As String = ENVIRONMENTCLASS.CONTACT.TMPID

                sql = "INSERT INTO  frncon(hn,vn,seqcon,mtrgtid,strgtid,sbrgtid,insid,startdate,expiredate,amount,d_update,pconid,tmpid,f_vw) "
                sql += "VALUE('" & HN & "','" & VN & "'," & seqcon & "," & mtrgtid & "," & strgtid & "," & sbrgtid & ",'" & insid & "','" & startdate & "','" & expiredate & "'," & amount & ",'" & DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") & "'," & pconid & "," & tmpid & "," & f_vw & ");"
                db.ExecuteNonQuery(sql)
            Next
            db.CommitTrans()
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            db.Dispose()
            '   datejaja.afterSave()
        End Try

    End Sub

    Public Function VNgetSend(ByVal VNx As String, ByVal Clinic As Integer) As Boolean
        Dim db = ConnecDBRYH.NewConnection
        ' Dim datejaja As New FriendsDate
        Dim sql As String

        Try

            sql = "SELECT HN,frnservice.VN,intime,intimedesc,typein,typeindesc,referinhosp,hospname,req_card "
            sql += "FROM (SELECT HN,VN,intime,typein,referinhosp FROM  frnservice WHERE vn='" & VNx & "') frnservice "
            sql += "LEFT JOIN  masintime ON frnservice.intime = masintime.intimeid "
            sql += "LEFT JOIN  mastypein ON frnservice.typein = mastypein.typeinid "
            sql += "LEFT JOIN  mashosp ON frnservice.referinhosp = mashosp.hospname "
            sql += "LEFT JOIN  frnclinic ON frnclinic.vn = frnservice.vn "
            sql += "WHERE clinic = '" & Clinic & "';"
            Dim dt As DataTable = db.GetTable(sql)

            If dt.Rows.Count > 0 Then
                VN._HN = If(IsDBNull(dt.Rows(0).Item("HN")), "", dt.Rows(0).Item("HN"))
                VN._VN = If(IsDBNull(dt.Rows(0).Item("VN")), "", dt.Rows(0).Item("VN"))
                VN._INTIME = If(IsDBNull(dt.Rows(0).Item("intime")), "", dt.Rows(0).Item("intime"))
                VN._INTIMEDESC = If(IsDBNull(dt.Rows(0).Item("intimedesc")), "", dt.Rows(0).Item("intimedesc"))
                VN._TYPEIN = If(IsDBNull(dt.Rows(0).Item("typein")), "", dt.Rows(0).Item("typein"))
                VN._TYPEINDESC = If(IsDBNull(dt.Rows(0).Item("typeindesc")), "", dt.Rows(0).Item("typeindesc"))
                VN._REFERINHOSP = If(IsDBNull(dt.Rows(0).Item("referinhosp")), "", dt.Rows(0).Item("referinhosp"))
                VN._REFERINHOSPDESC = If(IsDBNull(dt.Rows(0).Item("hospname")), "", dt.Rows(0).Item("hospname"))
                VN._ISREQUESTDOCUMENT = If(IsDBNull(dt.Rows(0).Item("req_card")), "", dt.Rows(0).Item("req_card"))
                Return True
            End If

        Catch ex As Exception
            db.Dispose()
        End Try

        Return False

    End Function

    ''' <summary>
    ''' ผู้ป่วย Check In เข้าคลินิก
    ''' </summary>
    ''' <remarks>
    '''   - Input (VN = หมายเลข VN,Clinic = หมายเลขคลินิก)
    '''   - Output Update สถานะการเข้าใช้คลินิกเป็นกำลัง Check in
    ''' </remarks>
    Public Sub setVNcheckin(ByVal VN As String, ByVal ClinicID As Integer, ByVal USMK As String)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        'Dim sql As String = "UPDATE  frnclinic SET rectime=curtime(), usmk='" & USMK & "', cstatus=" & ENVIRONMENTCLASS.MASCSTATUS.waitdiag & " WHERE clinic='" & Clinic & "' AND vn='" & VN & "';"
        Dim sql As String = "UPDATE  frnclinic SET rectime=curtime(), usmk='" & USMK & "', cstatus=" & ENVIRONMENTCLASS.MASCSTATUS.accept & " WHERE clinicid ='" & ClinicID & "';"
        db.ExecuteNonQuery(sql)
        db.Dispose()

    End Sub

    ''' <summary>
    ''' ผู้ป่วย Check In เข้าคลินิก
    ''' </summary>
    ''' <remarks>
    '''   - Input (ClinicID = หมายเลขการเข้าใช้บริการคลินิก)
    '''   - Output Update สถานะการเข้าใช้คลินิกเป็นกำลัง Check in
    ''' </remarks>
    Public Sub setVNcheckin(ByVal ClinicID As Integer, ByVal USMK As String, ByVal dept As Integer, มีไว้งั้นๆด้านบนเหมือนกัน As Integer)
        If ENVIRONMENTCLASS.MACHINE.ClinicCode = ENVIRONMENTCLASS.CLINICCODE.LAB Then

            Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
            Dim sql As String = "UPDATE  frnclinic SET rectime=curtime(), usmk='" & USMK & "', cstatus=" & ENVIRONMENTCLASS.MASCSTATUS.diag & " WHERE clinicid='" & ClinicID & "';"
            db.ExecuteNonQuery(sql)
            db.Dispose()

        ElseIf ENVIRONMENTCLASS.MACHINE.ClinicCode = ENVIRONMENTCLASS.CLINICCODE.XRAY Then
            Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
            Dim sql As String = "UPDATE  frnclinic SET rectime=curtime(), usmk='" & USMK & "', cstatus=" & ENVIRONMENTCLASS.MASCSTATUS.diag & " WHERE clinicid='" & ClinicID & "';"
            db.ExecuteNonQuery(sql)
            db.Dispose()

        Else
            Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
            Dim sql As String = "UPDATE  frnclinic SET rectime=curtime(), usmk='" & USMK & "', cstatus=" & ENVIRONMENTCLASS.MASCSTATUS.wait & " WHERE clinicid='" & ClinicID & "';"
            db.ExecuteNonQuery(sql)
            db.Dispose()

        End If
    End Sub

    ''' <summary>
    ''' ผู้ป่วย Check In เข้าคลินิก
    ''' </summary>
    ''' <remarks>
    '''   - Input จะดึงจาก VNstruct ที่ _VN และ _CLINIC
    '''   - Output Update สถานะการเข้าใช้คลินิกเป็นกำลัง Check in
    ''' </remarks>
    Public Sub setVNcheckin()
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = "UPDATE  frnclinic SET rectime=curtime(), usmk='" & VN._PROVIDERKEY & "', cstatus=" & ENVIRONMENTCLASS.MASCSTATUS.waitdiag & " WHERE clinic='" & VN._CLINIC & "' AND vn='" & VN._VN & "';"
        db.ExecuteNonQuery(sql)
        db.Dispose()
    End Sub
    Public Sub setVNComplete(ByVal ClinicId As Integer, dept As Integer)
        If ENVIRONMENTCLASS.MACHINE.ClinicCode = ENVIRONMENTCLASS.CLINICCODE.LAB Then
            Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
            Dim sql As String = "UPDATE  frnclinic SET fintime = curtime() , f_finish = 1 ,cstatus =" & ENVIRONMENTCLASS.MASCSTATUS.complete & " WHERE clinicid = " & ClinicId & ";"

            db.ExecuteNonQuery(sql)
            db.Dispose()
        ElseIf ENVIRONMENTCLASS.MACHINE.ClinicCode = ENVIRONMENTCLASS.CLINICCODE.XRAY Then
            Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
            Dim sql As String = "UPDATE  frnclinic SET fintime = curtime() , f_finish = 1 ,cstatus =" & ENVIRONMENTCLASS.MASCSTATUS.complete & " WHERE clinicid = " & ClinicId & ";"

            db.ExecuteNonQuery(sql)
            db.Dispose()
            'ไว้เขียนที่Xray

        Else

            'แผนกอื่น
        End If

    End Sub
    Public Sub setVnCancle(ByVal ClinicId As Integer, dept As Integer)
        '
        If ENVIRONMENTCLASS.MACHINE.ClinicCode = ENVIRONMENTCLASS.CLINICCODE.LAB Then
            Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
            Dim sql As String = "UPDATE  frnclinic SET fintime = curtime() , f_finish = 1 ,cstatus =" & ENVIRONMENTCLASS.MASCSTATUS.send & " WHERE clinicid = " & ClinicId & ";"
            db.ExecuteNonQuery(sql)
            db.Dispose()
        ElseIf ENVIRONMENTCLASS.MACHINE.ClinicCode = ENVIRONMENTCLASS.CLINICCODE.XRAY Then

            'ไว้เขียนที่Xray

        Else

            'แผนกอื่น
        End If

    End Sub
    Public Function updateMasPstatus(ByVal vn As String, pstatus As String)
        Dim sql As String
        sql = "UPDATE frnservice SET pstatus = " & pstatus & " WHERE vn = " & vn & ";"
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        db.ExecuteNonQuery(sql)
        db.Dispose()

    End Function

    Public Shared Function UpdatePStatus(ByVal vn As String, pstatus As String) As Boolean
        Dim sql As String
        sql = "UPDATE frnservice SET pstatus = " & pstatus & " WHERE vn = " & vn & ";"
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Try
            db.ExecuteNonQuery(sql)
            db.Dispose()
            Return True
        Catch ex As Exception
            db.Dispose()
            Return False
        End Try

    End Function

    Public Shared Function UpdateCtatus(clinicID As String, cstatus As Integer) As Boolean
        Dim sql As String
        sql = "UPDATE  frnclinic SET cstatus = " & cstatus & " WHERE clinicid = " & clinicID & ";"
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Try
            db.ExecuteNonQuery(sql)
            db.Dispose()
            Return True
        Catch ex As Exception
            db.Dispose()
            Return False
        End Try

    End Function

    'เอาไว้ update rectime ในตาราง เวลาที่ provider รับข้อมูล
    Public Function updateRectime(ByVal CLINICID As String, ByVal CSTATUS As Integer, ByVal USMK As String) As Boolean
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

        Dim dt As DataTable
        dt = db.GetTable("SELECT rectime FROM frnclinic WHERE clinicid = '" & CLINICID & "'")

        If dt.Rows.Count > 0 Then
            If dt.Rows(0)(0) Is DBNull.Value Then
                Dim sql As String
                sql = "UPDATE frnclinic SET rectime = curtime() ,cstatus = " & CSTATUS & ", usmk = '" & USMK & "' WHERE clinicid = '" & CLINICID & "';"

                Try
                    db.BeginTrans()
                    db.ExecuteNonQuery(sql)
                    db.CommitTrans()
                    Return True
                Catch ex As Exception
                    MsgBox(ex.ToString)
                    db.RollbackTrans()
                    Return False
                End Try
            Else
                Return False
            End If
        Else
            Return False
        End If
        db.Dispose()
    End Function

    ''' <summary>
    ''' หานามแฝงภาษาไทยจาก VN ผู้ป่วย
    ''' </summary>
    ''' <remarks>
    '''   - Input : VN
    '''   - Output : นามแฝงผู้ป่วย
    ''' </remarks>
    Public Function getPenName(ByVal VN As String) As String
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Try
            Dim sql As String

            sql = "SELECT CASE WHEN CASE WHEN h.`penid` = 0 THEN CONCAT(p.`name`,' ',p.lname) ELSE CONCAT(penname.`penname`,' ', penname.`penlname`) END = '' THEN '' "
            sql += "ELSE CONCAT_WS('',pre.stprename,CASE WHEN h.`penid` = 0 THEN CONCAT(p.`name`,' ',p.lname) ELSE CONCAT(penname.`penname`,' ', penname.`penlname`) END) END AS 'name' "
            sql += ",h.penid "
            sql += "FROM (SELECT vn,hn,penid FROM  frnservice WHERE vn = '" & VN & "') AS h  "
            sql += "LEFT JOIN  person AS p ON h.hn = p.hn "
            sql += "LEFT JOIN  masprename AS pre ON p.prename = pre.prename "
            sql += "LEFT JOIN (SELECT * FROM  maspenname WHERE `status` = 1) AS penname ON h.penid = penname.penid "

            Dim dt As DataTable = db.GetTable(sql)
            If dt.Rows.Count > 0 Then
                Dim Penname As String = dt.Rows(0).Item(0)
                db.Dispose()
                Return Penname
            End If
        Catch ex As Exception
            'MsgBox(ex.ToString)
        End Try
        db.Dispose()
        Return Nothing
    End Function

    Public Sub getPenName(ByVal VN As String, tControl As Control)
        Dim txt = DirectCast(tControl, DevComponents.DotNetBar.Controls.TextBoxX)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Try
            Dim sql As String
            'sql = "SELECT CONCAT(pre.stprename,CASE WHEN h.`penid` = 0 THEN CONCAT(p.`name`,' ',p.lname) ELSE CONCAT(penname.`penname`,' ', penname.`penlname`) END) AS 'name',h.penid "

            sql = "SELECT CASE WHEN CASE WHEN h.`penid` = 0 THEN CONCAT(p.`name`,' ',p.lname) ELSE CONCAT(penname.`penname`,' ', penname.`penlname`) END = '' THEN '' "
            sql += "ELSE CONCAT_WS('',pre.stprename,CASE WHEN h.`penid` = 0 THEN CONCAT(p.`name`,' ',p.lname) ELSE CONCAT(penname.`penname`,' ', penname.`penlname`) END) END AS 'name' "
            sql += ",h.penid "
            sql += "FROM (SELECT vn,hn,penid FROM  frnservice WHERE vn = '" & VN & "') AS h  "
            sql += "LEFT JOIN  person AS p ON h.hn = p.hn "
            sql += "LEFT JOIN  masprename AS pre ON p.prename = pre.prename "
            sql += "LEFT JOIN (SELECT * FROM  maspenname WHERE `status` = 1) AS penname ON h.penid = penname.penid "
            Dim dt As DataTable = db.GetTable(sql)
            If dt.Rows.Count > 0 Then
                Dim Penname As String = dt.Rows(0).Item(0)
                txt.Text = Penname
                If dt.Rows(0)("penid") Is DBNull.Value Then
                    txt.ForeColor = Color.Black
                ElseIf dt.Rows(0)("penid").ToString.Trim = "" Or dt.Rows(0)("penid").ToString.Trim = "0" Then
                    txt.ForeColor = Color.Black
                Else
                    txt.ForeColor = ENVIRONMENTCLASS.PENNAME.PenNameColor
                End If
                db.Dispose()
            End If
        Catch ex As Exception
            db.Dispose()
            txt.Text = ""
            txt.ForeColor = Color.Black
        End Try

    End Sub

    ''' <summary>
    ''' หานามแฝงภาษาอังกฤษจาก VN ผู้ป่วย
    ''' </summary>
    ''' <remarks>
    '''   - Input : VN
    '''   - Output : นามแฝงผู้ป่วย
    ''' </remarks>
    Public Function getPenNameEN(ByVal VN As String) As String
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Try
            Dim sql As String
            sql = "SELECT CASE WHEN CASE WHEN h.`penid` = 0 THEN CONCAT(p.`ename`,' ',p.elname) ELSE CONCAT(penname.`penname_en`,' ', penname.`penlname_en`) END = '' THEN '' "
            sql += "ELSE CONCAT_WS('',pre.seprename,CASE WHEN h.`penid` = 0 THEN CONCAT(p.`ename`,' ',p.elname) ELSE CONCAT(penname.`penname_en`,' ', penname.`penlname_en`) END) END AS 'name' "
            sql += ",h.penid "
            sql += "FROM (SELECT vn,hn,penid FROM  frnservice WHERE vn = '" & VN & "') AS h  "
            sql += "LEFT JOIN  person AS p ON h.hn = p.hn "
            sql += "LEFT JOIN  masprename AS pre ON p.prename = pre.prename "
            sql += "LEFT JOIN (SELECT * FROM  maspenname WHERE `status` = 1) AS penname ON h.penid = penname.penid "

            Dim dt As DataTable = db.GetTable(sql)
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item(0) IsNot DBNull.Value Then
                    Dim Penname As String = dt.Rows(0).Item(0)
                    db.Dispose()
                    Return Penname
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        db.Dispose()
        Return Nothing
    End Function

    Public Sub getPenNameEN(ByVal VN As String, tControl As Control)
        Dim txt = DirectCast(tControl, DevComponents.DotNetBar.Controls.TextBoxX)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Try
            Dim sql As String
            sql = "SELECT CASE WHEN CASE WHEN h.`penid` = 0 THEN CONCAT(p.`ename`,' ',p.elname) ELSE CONCAT(penname.`penname_en`,' ', penname.`penlname_en`) END = '' THEN '' "
            sql += "ELSE CONCAT_WS('',pre.seprename,CASE WHEN h.`penid` = 0 THEN CONCAT(p.`ename`,' ',p.elname) ELSE CONCAT(penname.`penname_en`,' ', penname.`penlname_en`) END) END AS 'name' "
            sql += ",h.penid "
            sql += "FROM (SELECT vn,hn,penid FROM  frnservice WHERE vn = '" & VN & "') AS h  "
            sql += "LEFT JOIN  person AS p ON h.hn = p.hn "
            sql += "LEFT JOIN  masprename AS pre ON p.prename = pre.prename "
            sql += "LEFT JOIN (SELECT * FROM  maspenname WHERE `status` = 1) AS penname ON h.penid = penname.penid "
            Dim dt As DataTable = db.GetTable(sql)
            If dt.Rows.Count > 0 Then
                Dim Penname As String = dt.Rows(0).Item(0)
                txt.Text = Penname
                If dt.Rows(0)("penid") Is DBNull.Value Then
                    txt.ForeColor = Color.Black
                ElseIf dt.Rows(0)("penid").ToString.Trim = "" Or dt.Rows(0)("penid").ToString.Trim = "0" Then
                    txt.ForeColor = Color.Black
                Else
                    txt.ForeColor = ENVIRONMENTCLASS.PENNAME.PenNameColor
                End If
                db.Dispose()
            End If
        Catch ex As Exception
            db.Dispose()
            txt.Text = ""
            txt.ForeColor = Color.Black
        End Try
    End Sub

    'Public Shared Function HasVN(ByVal HN As String, ByVal ClinicID As Integer) As String

    '    Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
    '    Dim VN As String = Nothing

    '    Try
    '        Dim sql As String = "SELECT DISTINCT HN,c.VN,f_finish "
    '        sql += "FROM  frnservice s JOIN  frnclinic c ON s.vn = c.vn  "
    '        sql += "WHERE hn='" & HN & "' AND f_finish IS null AND clinic = '" & ClinicID & "';"
    '        Dim dt As DataTable = db.GetTable(sql)

    '        If dt.Rows.Count > 0 Then
    '            VN = dt.Rows(0).Item("VN")
    '        End If

    '    Catch ex As Exception
    '        MsgBox(ex.ToString)
    '    End Try

    '    Return VN
    'End Function

    'Public Shared Function HasVN(ByVal HN As String, ByVal ClinicID As Integer) As String

    '    Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
    '    Dim VN As String = Nothing

    '    Try
    '        Dim sql As String = "SELECT DISTINCT HN,c.VN,f_finish "
    '        sql += "FROM  frnservice s JOIN  frnclinic c ON s.vn = c.vn  "
    '        sql += "WHERE hn='" & HN & "' AND f_finish IS null AND clinic = '" & ClinicID & "';"
    '        Dim dt As DataTable = db.GetTable(sql)

    '        If dt.Rows.Count > 0 Then
    '            VN = dt.Rows(0).Item("VN")
    '        End If

    '    Catch ex As Exception
    '        MsgBox(ex.ToString)
    '    End Try

    '    Return VN
    'End Function

    ''' <summary>
    ''' ค่า PSTATUS ของคลินิก
    ''' </summary>
    ''' <remarks>
    '''   - Input (Clinic = หมายเลขคลินิก)
    '''   - Output หมายเลข PSTATUS
    ''' </remarks>
    Public Function getPSTATUS(ByVal Clinic As Integer) As Integer

        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Try
            Dim sql As String = "SELECT pstatus,ps_name,clinic FROM  maspstatus WHERE clinic = '" & Clinic & "' and `status` = 1;"
            Dim dt As DataTable = db.GetTable(sql)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item("pstatus")
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Return Nothing
        db.Dispose()
    End Function

    Public Shared Function IsNotReceiveVN(ByVal vnX As String, clinicX As String) As Boolean
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

        Dim sql As String = "SELECT vn FROM  maspstatus WHERE vn = '" & vnX & "' and `clinic` = '" & clinicX & "' AND cstatus = " & ENVIRONMENTCLASS.MASCSTATUS.wait & ";"
        Dim dt As DataTable = db.GetTable(sql)
        db.Dispose()
        If dt.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Shared Function getPSTATUSByVN(vnX As String) As Integer
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Try
            Dim sql As String = "SELECT pstatus FROM  frnservice WHERE vn = '" & vnX & "';"
            Dim dt As DataTable = db.GetTable(sql)
            db.Dispose()
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item("pstatus")
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Return Nothing

    End Function

    ''' <summary>
    ''' ค่า PSTATUS ของคลินิก
    ''' </summary>
    ''' <remarks>
    '''   - Input (Clinic = หมายเลขคลินิก,VN = หมายเลขการเข้ารักษา)
    '''   - Output บันทึก PSTATUS ลงในฐานข้อมูล VN
    ''' </remarks>
    Public Sub setMyPSTATUS(ByVal VN As String, ByVal Clinic As Integer)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim pstatus As String = ""
        Try
            Dim sql As String = "SELECT pstatus,ps_name,clinic FROM  maspstatus WHERE clinic = '" & Clinic & "';"
            Dim dt As DataTable = db.GetTable(sql)
            If dt.Rows.Count > 0 Then
                pstatus = dt.Rows(0).Item("pstatus")
                sql = "UPDATE  frnservice SET pstatus='" & pstatus & "' WHERE vn='" & VN & "';"
                db.BeginTrans()
                db.ExecuteNonQuery(sql)
                db.CommitTrans()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            db.RollbackTrans()
        End Try
        db.Dispose()
    End Sub

    Public Shared Function GetPenNameDataByVN(vnX As String) As DataTable
        Dim dt As New DataTable
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = "SELECT vn,penid FROM  frnservice "
        sql += "WHERE vn = '" & vnX & "' "
        sql += "AND penid <> 0 AND penid IS NOT NULL;"
        sql += ""
        dt = db.GetTable(sql)
        db.Dispose()
        Return dt
    End Function

    Public Shared Function GetLastVN(vnx As String) As String
        Dim db = ConnecDBRYH.NewConnection
        Dim sql As String = "SELECT vn FROM  frnservice "
        sql += "WHERE hn = (SELECT hn FROM  frnservice WHERE vn = '" & vnx & "') AND vn <> '" & vnx & "' ORDER BY date_serv DESC LIMIT 1;"
        sql += ""
        Dim dtVN = db.GetTable(sql)
        db.Dispose()

        If dtVN.Rows.Count > 0 Then
            Return dtVN.Rows(0)("vn").ToString.Trim
        Else
            Return ""
        End If
    End Function

#Region "lock รายการ"

    'Lock VN ใน FRNSERVICE

    Public Shared Function GetLockStatus(vnX As String) As Boolean
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        Dim sql As String = "SELECT f_lock FROM  frnservice WHERE vn = '" & vnX & "'; "
        dt = db.GetTable(sql)
        db.Dispose()
        If dt.Rows.Count > 0 Then
            Return Convert.ToBoolean(dt.Rows(0)("f_lock"))
        Else
            Return False
        End If

    End Function

    Public Shared Sub LockVn(vnX As String)
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        Dim sql As String = ""
        sql = "UPDATE  frnservice SET "
        sql += "f_lock = 1 "
        sql += "WHERE vn = '" & vnX & "';"
        'sql += "WHERE vn = '" & vnX & "' AND clinic = '" & clinicX & "' ;"
        db.BeginTrans()
        Try
            db.ExecuteNonQuery(sql)
            db.CommitTrans()
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ENVIRONMENTCLASS.DB.MsgUpdateError & ex.Message)
        Finally
            db.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' ปรับสถานะ VN เป็นไม่ล็อค
    ''' </summary>
    ''' <remarks>
    ''' - Input (vn = หมายเลข VN,clinicX = รหัสคลีนิค)
    '''   - Output Update field HN ในฐานข้อมูล
    ''' </remarks>
    Public Shared Sub UnLockVn(vnX As String)
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        Dim sql As String = ""
        sql = "UPDATE  frnservice SET "
        sql += "f_lock = 0 "
        sql += "WHERE vn = '" & vnX & "' ;"
        'sql += "WHERE vn = '" & vnX & "' AND clinic = '" & clinicX & "' ;"
        db.BeginTrans()
        Try
            db.ExecuteNonQuery(sql)
            db.CommitTrans()
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ENVIRONMENTCLASS.DB.MsgUpdateError & ex.Message)
        Finally
            db.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' ปรับสถานะ VN เป็น Lock
    ''' </summary>
    ''' <remarks>
    ''' - Input (vn = หมายเลข VN,clinicX = รหัสคลีนิค)
    '''   - Output Update field HN ในฐานข้อมูล
    ''' </remarks>
    Public Shared Sub LockVn(vnX As String, clinicX As String)
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        Dim sql As String = ""
        sql = "UPDATE  frnclinic SET "
        sql += "f_lock = 1 "
        sql += "WHERE vn = '" & vnX & "';"
        'sql += "WHERE vn = '" & vnX & "' AND clinic = '" & clinicX & "' ;"
        db.BeginTrans()
        Try
            db.ExecuteNonQuery(sql)
            db.CommitTrans()
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ENVIRONMENTCLASS.DB.MsgUpdateError & ex.Message)
        Finally
            db.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' ปรับสถานะ VN เป็นไม่ล็อค
    ''' </summary>
    ''' <remarks>
    ''' - Input (vn = หมายเลข VN,clinicX = รหัสคลีนิค)
    '''   - Output Update field HN ในฐานข้อมูล
    ''' </remarks>
    Public Shared Sub UnLockVn(vnX As String, clinicX As String)
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        Dim sql As String = ""
        sql = "UPDATE  frnclinic SET "
        sql += "f_lock = 0 "
        sql += "WHERE vn = '" & vnX & "' ;"
        'sql += "WHERE vn = '" & vnX & "' AND clinic = '" & clinicX & "' ;"
        db.BeginTrans()
        Try
            db.ExecuteNonQuery(sql)
            db.CommitTrans()
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ENVIRONMENTCLASS.DB.MsgUpdateError & ex.Message)
        Finally
            db.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' ดึงสถานะการ Lock รายการ
    ''' </summary>
    ''' <remarks>- Input (vnX = เลข VN,clinicX = รหัสคลีนิค)</remarks>
    ''' <returns>สถานะการ Lock รายการ</returns>
    Public Shared Function GetLockStatus(vnX As String, clinicX As String) As Boolean
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        Dim sql As String = ""
        Dim IsLock As Boolean = False
        sql = "SELECT f_lock FROM  frnclinic "
        sql += "WHERE  vn = '" & vnX & "' AND clinic = '" & clinicX & "' ;"
        dt = db.GetTable(sql)
        If dt.Rows.Count > 0 Then
            If dt.Rows(0)("f_lock") Is DBNull.Value Then
                IsLock = False
            ElseIf Convert.ToInt32(dt.Rows(0)("f_lock")) = 1 Then
                IsLock = True
            Else
                IsLock = False
            End If
        End If
        Return IsLock
    End Function
#End Region

#Region "VN Queue"
    Private min As Integer = 30 'ช่วงเวลารีเฟรซเป็นวินาที
    Private bgLoadQ As System.ComponentModel.BackgroundWorker
    Private time1 As System.Windows.Forms.Timer
    Private s As Integer = 1 'ตัวแปรนับของ Timer
    Private dgvData As SuperGridControl
    Private clinicQueue As String = ""
#Region "Property"
    Public WriteOnly Property SetRefreshMinute
        Set(value)
            min = value
        End Set
    End Property
    Public ReadOnly Property GetRefreshMinute
        Get
            Return min
        End Get
    End Property

    Public WriteOnly Property SetClinicQueue
        Set(value)
            clinicQueue = value
        End Set
    End Property
    Public ReadOnly Property GetClinicQueue
        Get
            Return clinicQueue
        End Get
    End Property
#End Region

    Private Sub time1_Tick(sender As Object, e As EventArgs)
        If s = 1 Then
            If bgLoadQ.IsBusy = False Then
                bgLoadQ.RunWorkerAsync()
            Else
                bgLoadQ.WorkerSupportsCancellation = True
            End If
        ElseIf s = min Then
            s = 0
        End If
        s += 1
    End Sub

    Private Sub bgLoadQ_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs)
        If bgLoadQ.CancellationPending = True Then
            e.Cancel = True
            Exit Sub
        Else
            e.Result = myWork()
        End If
    End Sub

    Private Sub bgLoadQ_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs)
        If e.Cancelled = False Then
            dgvData.PrimaryGrid.DataSource = DirectCast(e.Result, DataTable)
        End If
    End Sub

    Public Sub LoadQueue(supGrid As SuperGridControl)
        dgvData = supGrid
        If time1 IsNot Nothing Then
            CloseQueue()
        End If
        time1 = New System.Windows.Forms.Timer
        time1.Interval = 1000
        AddHandler time1.Tick, AddressOf Me.time1_Tick
        bgLoadQ = New System.ComponentModel.BackgroundWorker
        AddHandler bgLoadQ.DoWork, AddressOf Me.bgLoadQ_DoWork
        AddHandler bgLoadQ.RunWorkerCompleted, AddressOf Me.bgLoadQ_RunWorkerCompleted
        s = 1
        time1.Enabled = True
    End Sub

    ''' <summary>
    ''' ปิด Timer และ Clear Objectดึงคิว
    ''' </summary>
    ''' <remarks>
    ''' - Input (supGrid = SuperGrid ตัวที่จะแสดงข้อมูล)
    '''   - Output โชว์คิวใน SuperGrid
    ''' </remarks>
    Public Sub CloseQueue()
        time1.Enabled = False
        time1.Dispose()
        bgLoadQ.Dispose()
    End Sub

    Private Function myWork() As DataTable
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable
        Dim sql As String = "SELECT CAST(h.HN AS CHAR(15)) AS HN,CASE WHEN(p.PENNAME = 0) THEN CONCAT(p.`NAME`,' ',p.LNAME) ELSE '" & ENVIRONMENTCLASS.PENNAME.PenNameThai & "' END AS `NAME`,TIMEDIFF(curtime(),c.SENTIME) AS WaitTime,h.VN,c.CLINICID,CASE WHEN(c.CSTATUS IN (2,3)) THEN 'ส่งแล้ว' ELSE '' END AS `Status` "
        sql += "FROM  frnservice AS h "
        sql += "INNER JOIN (SELECT * FROM  frnclinic WHERE CLINIC = " & clinicQueue & " AND CSTATUS IN (1,2,3,4) ) as c ON h.VN = c.VN "
        sql += "LEFT JOIN  person AS p on h.HN = p.HN "
        sql += "ORDER BY c.SENTIME ASC;"
        dt = db.GetTable(sql)
        db.Dispose()
        Return dt
    End Function
#End Region
End Class