﻿Public Class ENDDAYCLASS
    Dim dtPerson As DataTable
    Dim person As New PERSONCLASS
    Private vnStruct As VNCLASS.VNStruct
    Private vn As VNCLASS = New VNCLASS
    Dim labclass As New LABCLASS
    Dim providerpolicies As New PROVIDERCLASS

    Public Function RunEndDay() As Boolean
        Dim db = ConnecDBRYH2.NewConnection
        Dim dt As New DataTable
        Dim UpdateStatus As Boolean = False
        Try
            db.BeginTrans()
            Dim x = getSqlUpdateMasproduct()
            db.ExecuteNonQuery(getSqlUpdateMasPrice()) 'อัพเดจสถานะ active ในตาราร masprice
            db.ExecuteNonQuery(getSqlUpdateMasproduct()) 'อัพเดจราคา active ในตาราร masproduct

            db.CommitTrans()
            UpdateStatus = True
        Catch ex As Exception
            db.RollbackTrans()
            UpdateStatus = False
        Finally
            db.Dispose()
        End Try
        Return UpdateStatus
    End Function
    Public Sub ADDVNSTRUCT(hn As String)
        Dim datejaja As New FriendsDate
        ' สถานะ 1 คือ OPD

        dtPerson = person.getDataPersonTable(hn)

        vnStruct.CHKPRC_ = ENVIRONMENTCLASS.PRCSTAT.PRCOPD
        vnStruct._HN = dtPerson.Rows(0)("HN").ToString
        vnStruct._FTPRENAME = dtPerson.Rows(0)("FTPRENAME").ToString
        vnStruct._NAME = dtPerson.Rows(0)("NAME").ToString
        vnStruct._LNAME = dtPerson.Rows(0)("LNAME").ToString
        vnStruct._SEPRENAME = dtPerson.Rows(0)("SEPRENAME").ToString
        vnStruct._ENAME = dtPerson.Rows(0)("ENAME").ToString
        vnStruct._ELNAME = dtPerson.Rows(0)("ELNAME").ToString
        vnStruct._BIRTH = Convert.ToDateTime(dtPerson.Rows(0)("BIRTH")).ToString("dd/MM/yyyy")
        vnStruct._ABOGROUPDESC = dtPerson.Rows(0)("ABOGROUPDESC").ToString
        vnStruct._MOBILE = dtPerson.Rows(0)("MOBILE").ToString
        vnStruct._TELEPHONEPC = dtPerson.Rows(0)("TELEPHONEPC").ToString
        ' datejaja.Date_Diff(Convert.ToDateTime(dtPerson.Rows(0)("BIRTH")), Convert.ToDateTime(db.GetTable("SELECT current_timestamp()").Rows(0)(0)))
        vnStruct._BIRTH = dtPerson.Rows(0)("BIRTH").ToString
        vnStruct._AGEYEAR = datejaja._year
        vnStruct._AGEMONTH = datejaja._month
        vnStruct._AGEDAY = datejaja._day
        '  vnStruct.name = Convert.ToString(CLINICT.Text).Trim
        vnStruct._โรคประจำตัว = ""
        vnStruct._ISNEWPATIENT = dtPerson.Rows(0)("IsNewPatient").ToString
        vnStruct._PROVIDERKEY = 2


    End Sub
    Public Function ReadOrder()
        Dim sql As String
        Dim sql1 As String
        sql = "SELECT * FROM interfacedb.lab_master WHERE lab_status =3 AND report_date = '" & Date.Now.Year & Date.Now.ToString("-MM-dd") & "' AND report_time > '" & Date.Now.AddHours(-2).ToString("HH:mm:ss") & "' "
        Dim dt1 As New DataTable
        Dim db = ConnecDBRYH2.NewConnection
        Dim dt2 As New DataTable
        db.GetTable(sql, dt1)
        Dim sql3 As String
        For i As Integer = 0 To dt1.Rows.Count - 1
            sql3 = "UPDATE prdorder SET ord_status = 2 WHERE orderid = " & dt1.Rows(i)("hisorder").ToString & "; "
            sql3 += "UPDATE prdorderdt SET f_chkin = 1 WHERE orderid = " & dt1.Rows(i)("hisorder").ToString & ";  "
            db.ExecuteNonQuery(sql3)
            sql1 = "SELECT * FROM interfacedb.lab_detail WHERE lab_order_number = " & dt1.Rows(i)("lab_order_number") & " AND lab_result <> '';"
            db.GetTable(sql1, dt2)
            sql3 = ""
            For j As Integer = 0 To dt2.Rows.Count - 1
                sql3 += "UPDATE prdordelabresult SET prdordelabresult.value = '" & dt2.Rows(j)("lab_result") & "' WHERE serial = " & dt2.Rows(j)("serial") & " AND hn = " & dt1.Rows(i)("hn") & " ;"
            Next
            If sql3 <> "" Then
                db.ExecuteNonQuery(sql3)

            End If

        Next


        sql = "SELECT hn,lab_order_number FROM interfacedb.lab_master WHERE   report_date = '" & Date.Now.Year & Date.Now.ToString("-MM-dd") & "' AND lab_status = 4   AND report_time > '" & Date.Now.AddHours(-2).ToString("HH:mm:ss") & "'   "
      
        db.GetTable(sql, dt1)
        For i As Integer = 0 To dt1.Rows.Count - 1
          
            sql1 = "SELECT lab_result,serial FROM interfacedb.lab_detail WHERE lab_order_number = " & dt1.Rows(i)("lab_order_number") & " ;"
            db.GetTable(sql1, dt2)
            sql3 = ""
            For j As Integer = 0 To dt2.Rows.Count - 1
                sql3 += "UPDATE prdordelabresult SET prdordelabresult.value = '" & dt2.Rows(j)("lab_result") & "' WHERE serial = " & dt2.Rows(j)("serial") & " AND hn = " & dt1.Rows(i)("hn") & " ;"
            Next
            db.ExecuteNonQuery(sql3)

        Next
    


    End Function
    Public Function getOrderInsertFoodAuto1()
        Dim sql As String
        Dim sql1 As String
        Dim doccode As String
        Dim genid As String
        genid = getGenerateId("foodorder", "idfoodorder")

        sql = "SELECT * FROM  (SELECT * FROM frnshift WHERE  status = 1   GROUP BY an ) frnshift JOIN  ( SELECT * FROM frnadmission where f_discharge = 1 ) AS  frnadmission ON frnadmission.an = frnshift.an JOIN ( SELECT * FROM sroomitem  WHERE stid = 3  ) AS   sroomitem ON frnshift.bedsid = sroomitem.bedsid JOIN masclinic ON masclinic.clinic = sroomitem.clinic   ;"

        Dim db = ConnecDBRYH2.NewConnection
        Dim dt As New DataTable
        dt = db.GetTable(sql)
        sql = "idfoodorder , "
        sql1 = " " & genid & ","
        sql += "fooddate , "
        sql1 += "NOW() , "
        sql += "doccode  "
        doccode = GENDOC.getIDform("frmFOOD000")
        sql1 += " '" & doccode & "' "
        Dim sql3 As String
        sql3 = "INSERT INTO foodorder ( " & sql & ") VALUES (" & sql1 & "); "
        db.ExecuteNonQuery(sql3)
        For i As Integer = 0 To dt.Rows.Count - 1
            sql = "idfoodorder , "
            sql1 = " " & genid & ", "
            sql += "hn , "
            sql1 += " " & dt.Rows(i)("hn").ToString & "  , "
            sql += "vn , "
            sql1 += " " & dt.Rows(i)("vn").ToString & ", "
            sql += " an , "
            sql1 += " " & dt.Rows(i)("an").ToString & ", "
            sql += " fodid , "
            sql1 += " " & If(IsDBNull(dt.Rows(i)("fodid")), "NULL", dt.Rows(i)("fodid").ToString) & ", "
            sql += "fodremark , "
            sql1 += " '" & dt.Rows(i)("fodremark").ToString & "' , "
            sql += "wardadmit , "
            sql1 += "  " & dt.Rows(i)("wardadmit").ToString & " ,  "
            sql += "clinicname , "
            sql1 += "  '" & dt.Rows(i)("clinicname").ToString & "' ,  "
            sql += "bedsname , "
            sql1 += "  '" & dt.Rows(i)("bedsname").ToString & "' ,  "
            sql += "bedsid ,"
            sql1 += "  " & dt.Rows(i)("bedsid").ToString & "  , "
            sql += "clinic "
            sql1 += "  " & dt.Rows(i)("wardadmit").ToString & "   "

            sql3 = "INSERT INTO foodorderlist ( " & sql & ") VALUES (" & sql1 & "); "
            db.ExecuteNonQuery(sql3)

        Next
    End Function

    Public Function getOrderFoodMorning()
        Dim sql As String
        Dim sql1 As String
        sql = "SELECT * FROM  (SELECT * FROM frnshift WHERE  status = 1   GROUP BY an ) frnshift JOIN  ( SELECT * FROM frnadmission where f_discharge = 1 ) AS  frnadmission ON frnadmission.an = frnshift.an JOIN sroomitem ON frnshift.bedsid = sroomitem.bedsid  ;"

        Dim db = ConnecDBRYH2.NewConnection
        Dim dt As New DataTable
        dt = db.GetTable(sql)
        For i As Integer = 0 To dt.Rows.Count - 1
            vnStruct = New VNCLASS.VNStruct
            vnStruct._VN = dt.Rows(i)("vn").ToString
            vnStruct._AN = dt.Rows(i)("an").ToString

            vnStruct._BCLINIC = dt.Rows(i)("wardadmit").ToString

            vnStruct._SENDBYCLINIC = dt.Rows(i)("wardadmit").ToString
            vnStruct._CLINICID = dt.Rows(i)("wardadmit").ToString
            ADDVNSTRUCT(dt.Rows(i)("hn").ToString)


            'sql = "SELECT code, prdname FROM fditem JOIN masproduct ON masproduct.prdcode = fditem.prdcodeb WHERE fodid = " & dt.Rows(i)("fodid").ToString & " "

            If IsDBNull(dt.Rows(i)("fodid")) = False Then
                If dt.Rows(i)("fodid").ToString <> "" Then
                    If dt.Rows(i)("fodid").ToString <> "4" Then
                        sql = "SELECT masproduct.prdcode AS 'PRDCODE' ,"
                        sql += " masproduct.prdcat AS 'PRDCAT', ccode AS 'CCODE',rcode AS 'RCODE', iccode AS  'ICCODE', ircode AS 'IRCODE', hccode AS  'HCCODE',hrcode AS  'HRCODE', masproduct.opdprc AS 'OPDPRC', masproduct.ipdprc  AS 'IPDPRC' , s_unit AS 'S_UNIT', f_stk as 'F_STK', 0 as 'TOTAL_QTY' , f_lot as 'F_LOT' FROM  (SELECT * FROM frnshift WHERE  an = " & dt.Rows(i)("an").ToString & " AND status = 1 GROUP BY an ) frnshift JOIN  ( SELECT * FROM frnadmission  WHERE an = " & dt.Rows(i)("an").ToString & " AND date_reqdisc IS NULL   ) AS  frnadmission ON frnadmission.an = frnshift.an JOIN sroomitem ON frnshift.bedsid = sroomitem.bedsid   JOIN    fditem ON frnadmission.fodid = fditem.fodid      JOIN masproduct ON masproduct.prdcode = fditem.prdcodeb WHERE fditem.fodid = " & dt.Rows(i)("fodid").ToString & " "
                        providerpolicies.GETHOSEM("ya2p", "PER000")
                        Dim CAT1_ORDER_DS As DataSet = New ORDERSET
                        db.GetTable(sql, CAT1_ORDER_DS.Tables("OPERORDER"))
                        labclass.SetVN = vnStruct
                        labclass.InsertOrder(CAT1_ORDER_DS, dt.Rows(i)("wardadmit").ToString, providerpolicies, ENVIRONMENTCLASS.GETTYPERCCODE.IPD, "OPERORDER", ENVIRONMENTCLASS.OPERID.OPER)
                    End If

                End If



            End If


        Next


    End Function
    Public Function getOrderSeq()
        Dim THculture As New System.Globalization.CultureInfo("th-TH", True)
        Dim sql As String
        Dim sql1 As String
        sql = "yr, "
        sql1 = " " & Date.Now.ToString("yy", THculture) & ", "
        sql += "mm, "
        sql1 += " " & Date.Now.ToString("MM").ToString & " , "
        sql += "seq , "
        sql1 += "'0000001', "
        sql += "year , "
        sql1 += " " & Date.Now.Year & " , "
        sql += "d_update  "
        sql1 += " NOW() "
        Dim sql3 As String
        sql3 = "INSERT INTO masordseq ( " & sql & " ) VALUES  ( " & sql1 & ")"

        Dim condb As ConnecDBRYH
        condb = ConnecDBRYH.NewConnection
        condb.ExecuteNonQuery(sql3)

    End Function
    Public Function getOrderMasVn()
        Dim iDate As New FriendsDate() 'object จัดการวันที่
        iDate.TO_TH()
        Dim sql As String
        Dim sql1 As String

        sql = "UPDATE masan SET mm = " & DateTime.Now.ToString("%M") & " , seq = 0  , an =" & Date.Now.ToString("yyMMdd") & "0001 "
      
        sql1 = "UPDATE masvn SET mm = " & DateTime.Now.ToString("%m") & " , seq = 0  , dd =  " & Date.Now.ToString("dd").ToString & " ,vn =" & Date.Now.ToString("yyMMdd") & "0001 "
        Dim db = ConnecDBRYH2.NewConnection
        db.ExecuteNonQuery(sql)
        db.ExecuteNonQuery(sql1)

    End Function
    Public Function getOrderFoodDayTime()
        Dim sql As String
        Dim sql1 As String
        sql = "SELECT * FROM  (SELECT * FROM frnshift WHERE status = 1 GROUP BY an ) frnshift JOIN  ( SELECT * FROM frnadmission  where f_discharge = 1 ) AS  frnadmission ON frnadmission.an = frnshift.an JOIN sroomitem ON frnshift.bedsid = sroomitem.bedsid  ;"

        Dim db = ConnecDBRYH2.NewConnection
        Dim dt As New DataTable
        dt = db.GetTable(sql)
        For i As Integer = 0 To dt.Rows.Count - 1
            vnStruct = New VNCLASS.VNStruct
            vnStruct._VN = dt.Rows(i)("vn").ToString
            vnStruct._AN = dt.Rows(i)("an").ToString

            vnStruct._BCLINIC = dt.Rows(i)("wardadmit").ToString

            vnStruct._SENDBYCLINIC = dt.Rows(i)("wardadmit").ToString
            vnStruct._CLINICID = dt.Rows(i)("wardadmit").ToString
            ADDVNSTRUCT(dt.Rows(i)("hn").ToString)


            'sql = "SELECT code, prdname FROM fditem JOIN masproduct ON masproduct.prdcode = fditem.prdcodeb WHERE fodid = " & dt.Rows(i)("fodid").ToString & " "

            If IsDBNull(dt.Rows(i)("fodid")) = False Then
                If dt.Rows(i)("fodid").ToString <> "" Then
                    If dt.Rows(i)("fodid").ToString <> "4" Then
                        sql = "SELECT masproduct.prdcode AS 'PRDCODE' ,  masproduct.prdcat AS 'PRDCAT', ccode AS 'CCODE',rcode AS 'RCODE', iccode AS  'ICCODE', ircode AS 'IRCODE', hccode AS  'HCCODE',hrcode AS  'HRCODE', masproduct.opdprc AS 'OPDPRC', masproduct.ipdprc  AS 'IPDPRC' , s_unit AS 'S_UNIT', f_stk as 'F_STK', 0 as 'TOTAL_QTY' , f_lot as 'F_LOT' FROM  (SELECT * FROM frnshift WHERE  an = " & dt.Rows(i)("an").ToString & "  AND status = 1    GROUP BY an ) frnshift JOIN  ( SELECT * FROM frnadmission  WHERE an = " & dt.Rows(i)("an").ToString & " AND date_reqdisc IS NULL  ) AS  frnadmission ON frnadmission.an = frnshift.an JOIN sroomitem ON frnshift.bedsid = sroomitem.bedsid JOIN      fditem ON frnadmission.fodid = fditem.fodid JOIN masproduct ON masproduct.prdcode = fditem.prdcodel WHERE fditem.fodid = " & dt.Rows(i)("fodid").ToString & " "
                        providerpolicies.GETHOSEM("ya2p", "PER000")
                        Dim CAT1_ORDER_DS As DataSet = New ORDERSET
                        db.GetTable(sql, CAT1_ORDER_DS.Tables("OPERORDER"))
                        labclass.SetVN = vnStruct
                        If CAT1_ORDER_DS.Tables("OPERORDER").Rows.Count > 0 Then
                            labclass.InsertOrder(CAT1_ORDER_DS, dt.Rows(i)("wardadmit").ToString, providerpolicies, ENVIRONMENTCLASS.GETTYPERCCODE.IPD, "OPERORDER", ENVIRONMENTCLASS.OPERID.OPER)
                        End If

                    End If

                End If



            End If


        Next
    End Function
    Public Sub getOrderMinmax()
        Dim sql As String
        sql = "SELECT prdcode FROM masproduct WHERE f_stk = 1 AND status =1 ;"
        Dim dt As New DataTable
        Dim sql2 As String
        Dim sql3 As String = ""
        Dim dt1 As New DataTable
        Dim first As Boolean = False
        Dim total As Double


        Dim connect As ConnecDBRYH = ConnecDBRYH.NewConnection
        connect.GetTable(sql, dt)
        sql = "DELETe  FROM forecastproduct;"
        connect.ExecuteNonQuery(sql)
        connect.Dispose()
        sql3 = "INSERT INTO forecastproduct (stkid,prdcode,productmax,productmin,productavg) VALUES "
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim connect1 As ConnecDBRYH = ConnecDBRYH.NewConnection
            dt1 = New DataTable
            sql2 = "SELECT * FROM forecastUseStock WHERE date <= '" & Date.Now.AddMonths(-1).Year & Date.Now.AddMonths(-1).ToString("-MM") & "' AND date >= '" & Date.Now.AddMonths(-7).Year & Date.Now.AddMonths(-7).ToString("-MM") & "' AND prdcode =" & dt.Rows(i)("prdcode").ToString & ";"
            connect1.GetTable(sql2, dt1)
            connect1.Dispose()
            total = 0
            For j As Integer = 0 To dt1.Rows.Count - 1
                total += Convert.ToDouble(dt1.Rows(j)("qtytotal"))
            Next
            If dt1.Rows.Count > 0 Then
                If first = False Then
                    sql3 += " (1,'" & dt.Rows(i)("prdcode").ToString & "' ,  '" & Math.Round((total / dt1.Rows.Count), 0, MidpointRounding.AwayFromZero) * 1.1 & "',  '" & Math.Round((total / dt1.Rows.Count), 0, MidpointRounding.AwayFromZero) * 0.9 & "',  '" & Math.Round((total / dt1.Rows.Count), 0, MidpointRounding.AwayFromZero) & "')  "
                    first = True
                Else
                    sql3 += " ,  (1,'" & dt.Rows(i)("prdcode").ToString & "' ,  '" & Math.Round((total / dt1.Rows.Count), 0, MidpointRounding.AwayFromZero) * 1.1 & "',  '" & Math.Round((total / dt1.Rows.Count), 0, MidpointRounding.AwayFromZero) * 0.9 & "',  '" & Math.Round((total / dt1.Rows.Count), 0, MidpointRounding.AwayFromZero) & "') "

                End If

            End If
            'Else
            '    If dt1.Rows.Count > 0 Then
            '        sql3 += " (1,'" & dt.Rows(i)("prdcode").ToString & "' ,  '" & Math.Round((total / dt1.Rows.Count), 0, MidpointRounding.AwayFromZero) * 1.1 & "',  '" & Math.Round((total / dt1.Rows.Count), 0, MidpointRounding.AwayFromZero) * 0.9 & "',  '" & Math.Round((total / dt1.Rows.Count), 0, MidpointRounding.AwayFromZero) & "'), "

            '    End If

        Next


        sql3 += " ;"
        connect = ConnecDBRYH.NewConnection
        connect.ExecuteNonQuery(sql3)
    End Sub
    Public Function getOrderFoodDayEvening()
        Dim sql As String
        Dim sql1 As String
        sql = "SELECT * FROM    ( SELECT * FROM frnadmission where f_discharge = 1 ) AS  frnadmission  JOIN (SELECT * FROM frnshift WHERE status = 1   GROUP BY an ) AS frnshift  ON frnadmission.an = frnshift.an JOIN sroomitem ON frnshift.bedsid = sroomitem.bedsid  ;"

        Dim db = ConnecDBRYH2.NewConnection
        Dim dt As New DataTable
        dt = db.GetTable(sql)
        For i As Integer = 0 To dt.Rows.Count - 1
            vnStruct = New VNCLASS.VNStruct
            vnStruct._VN = dt.Rows(i)("vn").ToString
            vnStruct._AN = dt.Rows(i)("an").ToString

            vnStruct._BCLINIC = dt.Rows(i)("clinic").ToString

            vnStruct._SENDBYCLINIC = dt.Rows(i)("clinic").ToString
            vnStruct._CLINICID = dt.Rows(i)("clinic").ToString
            ADDVNSTRUCT(dt.Rows(i)("hn").ToString)


            'sql = "SELECT code, prdname FROM fditem JOIN masproduct ON masproduct.prdcode = fditem.prdcodeb WHERE fodid = " & dt.Rows(i)("fodid").ToString & " "

            If IsDBNull(dt.Rows(i)("fodid")) = False Then
                If dt.Rows(i)("fodid").ToString <> "" Then
                    If dt.Rows(i)("fodid").ToString <> "4" Then
                        sql = "SELECT masproduct.prdcode AS 'PRDCODE' ,"
                        sql += " masproduct.prdcat AS 'PRDCAT', ccode AS 'CCODE',rcode AS 'RCODE', iccode AS  'ICCODE', ircode AS 'IRCODE', hccode AS  'HCCODE',hrcode AS  'HRCODE', masproduct.opdprc AS 'OPDPRC', masproduct.ipdprc  AS 'IPDPRC' , s_unit AS 'S_UNIT', f_stk as 'F_STK', 0 as 'TOTAL_QTY' , f_lot as 'F_LOT' FROM  (SELECT * FROM frnshift WHERE  an = " & dt.Rows(i)("an").ToString & "  AND status = 1    GROUP BY an ) frnshift JOIN  ( SELECT * FROM frnadmission  WHERE an = " & dt.Rows(i)("an").ToString & " AND date_reqdisc IS NULL ) AS  frnadmission ON frnadmission.an = frnshift.an JOIN sroomitem ON frnshift.bedsid = sroomitem.bedsid JOIN   fditem ON frnadmission.fodid = fditem.fodid JOIN masproduct ON masproduct.prdcode = fditem.prdcoded WHERE fditem.fodid = " & dt.Rows(i)("fodid").ToString & " "
                        providerpolicies.GETHOSEM("ya2p", "PER000")
                        Dim CAT1_ORDER_DS As DataSet = New ORDERSET
                        db.GetTable(sql, CAT1_ORDER_DS.Tables("OPERORDER"))
                        labclass.SetVN = vnStruct
                        labclass.InsertOrder(CAT1_ORDER_DS, dt.Rows(i)("wardadmit").ToString, providerpolicies, ENVIRONMENTCLASS.GETTYPERCCODE.IPD, "OPERORDER", ENVIRONMENTCLASS.OPERID.OPER)
                    End If

                End If



            End If


        Next
    End Function
    Public Function getClearMasClinic()
        Dim sql As String
        sql = "UPDATE masclinicseq SET seq = 1"
        Dim db = ConnecDBRYH2.NewConnection
        db.ExecuteNonQuery(sql)

    End Function

    Public Function getOrderRoom()
        Dim sql As String
        Dim sql1 As String
        sql = "SELECT * FROM  (SELECT * FROM frnshift WHERE status = 1  GROUP BY an ) frnshift JOIN  ( SELECT * FROM frnadmission  where f_discharge = 1 ) AS  frnadmission ON frnadmission.an = frnshift.an JOIN sroomitem ON frnshift.bedsid = sroomitem.bedsid JOIN pckproduct ON pckproduct.pckpid = sroomitem.pckpid ;"

        Dim db = ConnecDBRYH2.NewConnection
        Dim dt As New DataTable
        db.GetTable(sql, dt)
        For i As Integer = 0 To dt.Rows.Count - 1
            vnStruct = New VNCLASS.VNStruct
            vnStruct._VN = dt.Rows(i)("vn").ToString
            vnStruct._AN = dt.Rows(i)("an").ToString

            vnStruct._BCLINIC = dt.Rows(i)("wardadmit").ToString

            vnStruct._SENDBYCLINIC = dt.Rows(i)("wardadmit").ToString
            vnStruct._CLINICID = dt.Rows(i)("wardadmit").ToString
            ADDVNSTRUCT(dt.Rows(i)("hn").ToString)


            sql = "SELECT masproduct.prdcode AS 'PRDCODE' ,  masproduct.prdcat AS 'PRDCAT', ccode AS 'CCODE',rcode AS 'RCODE', iccode AS  'ICCODE', ircode AS 'IRCODE', hccode AS  'HCCODE',hrcode AS  'HRCODE', pcklistproduct.opdprc AS 'OPDPRC', pcklistproduct.ipdprc  AS 'IPDPRC' , s_unit AS 'S_UNIT', f_stk as 'F_STK', 0 as 'TOTAL_QTY' , f_lot as 'F_LOT' FROM  (SELECT * FROM frnshift WHERE  an = " & dt.Rows(i)("an").ToString & "  AND status = 1    GROUP BY an ) frnshift JOIN  ( SELECT * FROM frnadmission  WHERE an = " & dt.Rows(i)("an").ToString & "  ) AS  frnadmission ON frnadmission.an = frnshift.an JOIN sroomitem ON frnshift.bedsid = sroomitem.bedsid JOIN pckproduct ON pckproduct.pckpid = sroomitem.pckpid JOIN pcklistproduct ON pcklistproduct.pckpid = pckproduct.pckpid JOIN masproduct ON masproduct.prdcode = pcklistproduct.prdcode"
            providerpolicies.GETHOSEM("ya2p", "PER000")
            Dim CAT1_ORDER_DS As DataSet = New ORDERSET
            db.GetTable(sql, CAT1_ORDER_DS.Tables("OPERORDER"))
            labclass.SetVN = vnStruct
            labclass.InsertOrder(CAT1_ORDER_DS, dt.Rows(i)("wardadmit").ToString, providerpolicies, ENVIRONMENTCLASS.GETTYPERCCODE.IPD, "OPERORDER", ENVIRONMENTCLASS.OPERID.OPER)


        Next




        'Dim dt As New DataTable
        'Dim condb As ConnecDBRYH = ConnecDBRYH.NewConnection
        'condb.GetTable(sql, dt)

    End Function
    Private Function getSqlUpdateMasPrice() As String
        Dim sql = "UPDATE masprice "
        sql += "SET `status` = 0 "
        sql += "WHERE `status` = 1 "
        sql += "AND DATE_FORMAT(CURRENT_TIMESTAMP(),'%Y%m%d')  >  DATE_FORMAT(en,'%Y%m%d') ;"
        sql += ""
        Return sql
    End Function

    Private Function getSqlUpdateMasproduct() As String
        Dim sql = "UPDATE masproduct AS t1 "
        sql += "INNER JOIN "
        sql += "(SELECT p1.prdcode,p1.st,p1.en,p1.opd_price,p1.ipd_price  FROM "
        sql += "(SELECT prdcode,st,en,opd_price,ipd_price FROM masprice "
        sql += "WHERE `status` = 1 AND DATE_FORMAT(CURRENT_TIMESTAMP(),'%Y%m%d')  >=  DATE_FORMAT(st,'%Y%m%d') "
        sql += "AND DATE_FORMAT(CURRENT_TIMESTAMP(),'%Y%m%d')  <=  DATE_FORMAT(en,'%Y%m%d') "
        'sql += "AND prdcode = 1027 "
        sql += ""
        sql += ") AS p1 "
        sql += "INNER JOIN "


        sql += "( "
        sql += "SELECT DISTINCT prdcode,st "
        sql += "FROM masprice "
        sql += "WHERE st IN "
        sql += "( "
        sql += " SELECT max(st) "
        sql += " FROM masprice "
        sql += " WHERE `status` = 1 "
        sql += " AND DATE_FORMAT(CURRENT_TIMESTAMP(),'%Y%m%d')  >=  DATE_FORMAT(st,'%Y%m%d') "
        sql += " AND DATE_FORMAT(CURRENT_TIMESTAMP(),'%Y%m%d')  <=  DATE_FORMAT(en,'%Y%m%d') "
        sql += " GROUP BY prdcode ORDER BY prdcode DESC "
        sql += " ) AND `status` = 1 "
        sql += ") AS p2 "
        sql += "ON p1.prdcode = p2.prdcode AND p1.st = p2.st "
        sql += ""
        sql += "INNER JOIN "
        sql += "("
        sql += "SELECT prdcode,max(opd_price) AS op "
        sql += "FROM masprice "
        sql += "WHERE `status` = 1 "
        sql += "AND DATE_FORMAT(CURRENT_TIMESTAMP(),'%Y%m%d')  >=  DATE_FORMAT(st,'%Y%m%d') "
        sql += "AND DATE_FORMAT(CURRENT_TIMESTAMP(),'%Y%m%d')  <=  DATE_FORMAT(en,'%Y%m%d') "
        sql += "GROUP BY prdcode "
        sql += ") AS p3 "
        sql += "ON p1.prdcode = p3.prdcode AND p1.opd_price = p3.op "
        sql += ""
        sql += ""
        sql += ""


        sql += ") AS tbx "
        sql += "ON t1.prdcode = tbx.prdcode "
        sql += "SET t1.opdprc = tbx.opd_price "
        sql += ",t1.ipdprc = tbx.ipd_price "
        sql += ""
        Return sql
    End Function


    Public Function CutStock() As String
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable
        Dim sqlX As String = ""
        Try
            db.BeginTrans()

            sqlX += "DROP TEMPORARY TABLE IF EXISTS tbx;"
            sqlX += "CREATE TEMPORARY TABLE IF NOT EXISTS tbx "
            sqlX += "AS ("
            sqlX += GetSqlPrdOrderDTMuchCut()




            sqlX += ");"
            sqlX += ""
            sqlX += "INSERT INTO  frnprdstk (lotid,doctype,dateprd,stkid,prdcode,`status`,cvf,qtytotal,qty,unitid,d_update,f_lock) "
            sqlX += "SELECT * FROM tbx; "
            'sqlX += "DROP TEMPORARY TABLE IF EXISTS tbx; "
            sqlX += ""
            db.ExecuteNonQuery(sqlX)

            sqlX = "SELECT * FROM tbx;"
            dt = db.GetTable(sqlX)
            Dim dtOnhand As New DataTable
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim stkid = dt.Rows(i)("stkid").ToString.Trim
                Dim prdcode = dt.Rows(i)("prdcode").ToString.Trim
                Dim lotid = dt.Rows(i)("lotid").ToString.Trim
                Dim qtytotal = dt.Rows(i)("qtytotal").ToString.Trim
                Dim unitid = dt.Rows(i)("unitid").ToString.Trim

                sqlX = "SELECT onhandid FROM  stockonhand WHERE stkcode = '" & stkid & "' AND prdcode = '" & prdcode & "' AND  lotid = '" & lotid & "';"
                dtOnhand = db.GetTable(sqlX)
                If dtOnhand.Rows.Count > 0 Then
                    sqlX = "UPDATE  stockonhand SET "
                    sqlX += "qtytotal = qtytotal + " & Convert.ToDecimal(dt.Rows(i)("qtytotal")) & " "
                    sqlX += "WHERE onhandid = '" & dtOnhand.Rows(0)("onhandid").ToString.Trim & "' ;"
                    db.ExecuteNonQuery(sqlX)
                Else
                    sqlX = "INSERT INTO  stockonhand(stkcode,prdcode,lotid,qtytotal,unitid) VALUES( "
                    sqlX += "'" & stkid & "',"
                    sqlX += "'" & prdcode & "',"
                    sqlX += "'" & lotid & "',"
                    sqlX += "'" & qtytotal & "',"
                    sqlX += "'" & unitid & "' "
                    sqlX += ");"
                    sqlX += ""
                    db.ExecuteNonQuery(sqlX)
                End If
            Next i

            'dt = New DataTable
            'dt = db.GetTable(GetSqlPrdOrderDTMuchCutNotSum)
            Dim dtUpStatus As New DataTable
            dtUpStatus = db.GetTable(GetSqlPrdOrderDTMuchCutNotSum)
            For i As Integer = 0 To dtUpStatus.Rows.Count - 1
                sqlX = "UPDATE  prdorderdt SET "
                sqlX += "f_stk = 1 "
                sqlX += "WHERE `id` = '" & dtUpStatus.Rows(i)("id").ToString.Trim & "' ;"
                db.ExecuteNonQuery(sqlX)
            Next i
            'Dim x = GetSqlPrdOrderDTMuchCutNotSum()

            db.CommitTrans()
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)
            MsgBox("มีปัญหา...")
        Finally
            db.Dispose()

        End Try

        Return sqlX

    End Function
    Public Sub getStockOnhandaily()
        Dim sql As String
        sql = "SELECT stkcode,stockonhand.prdcode,SUM(qtytotal) AS qtytotal,stockonhand.unitid , masproduct.avg_cost AS price FROM stockonhand JOIN masproduct ON masproduct.prdcode = stockonhand.prdcode GROUP BY stockonhand.stkcode,stockonhand.prdcode "
        Dim sql1 As String
        sql1 = "SELECT stkcode,stockonhand.prdcode,SUM(qtytotal) AS qtytotal,lotid,unitid , masproduct.avg_cost  AS price FROM stockonhand  JOIN masproduct  ON masproduct.prdcode = stockonhand.prdcode GROUP BY stockonhand.lotid,stockonhand.prdcode "
        Dim connect As ConnecDBRYH
        connect = ConnecDBRYH.NewConnection
        Dim dt As New DataTable
        dt = connect.GetTable(sql)
        Dim sql2 As String
        sql2 = ""
        Dim first As Boolean = True
        sql2 = " INSERT INTO stockonhanddaily (stkcode,prdcode,qtytotal,unitid,price,datetime) VALUES "
        For i = 0 To dt.Rows.Count - 1
            If first = True Then
                sql2 += " ('" & dt.Rows(i)("stkcode").ToString & "','" & dt.Rows(i)("prdcode").ToString & "','" & dt.Rows(i)("qtytotal").ToString & "'," & If(IsDBNull(dt.Rows(i)("unitid")), "null", "'" & dt.Rows(i)("unitid")).ToString & "'" & ",'" & dt.Rows(i)("price").ToString & "',NOW() ) "
                first = False
            Else
                sql2 += " , ('" & dt.Rows(i)("stkcode").ToString & "','" & dt.Rows(i)("prdcode").ToString & "','" & dt.Rows(i)("qtytotal").ToString & "'," & If(IsDBNull(dt.Rows(i)("unitid")), "null", "'" & dt.Rows(i)("unitid")).ToString & "'" & ",'" & dt.Rows(i)("price").ToString & "',NOW() ) "
            End If

        Next
        sql2 += ";"
        first = True
        connect.ExecuteNonQuery(sql2)
        connect = ConnecDBRYH.NewConnection
        dt = New DataTable
        dt = connect.GetTable(sql1)
        sql2 = "INSERT INTO stockonhanddailydt (stkcode,prdcode,qtytotal,unitid,lotid,datetime,price) VALUES "

        For i = 0 To dt.Rows.Count - 1
            If first = True Then
                sql2 += " ( '" & dt.Rows(i)("stkcode").ToString & "','" & dt.Rows(i)("prdcode").ToString & "','" & dt.Rows(i)("qtytotal").ToString & "'," & If(IsDBNull(dt.Rows(i)("unitid")), "null", dt.Rows(i)("unitid")).ToString & "," & If(IsDBNull(dt.Rows(i)("lotid")), "null", dt.Rows(i)("lotid")).ToString & " ,NOW() , '" & dt.Rows(i)("price").ToString & "') "
                first = False
            Else
                sql2 += " , ( '" & dt.Rows(i)("stkcode").ToString & "','" & dt.Rows(i)("prdcode").ToString & "','" & dt.Rows(i)("qtytotal").ToString & "'," & If(IsDBNull(dt.Rows(i)("unitid")), "null", dt.Rows(i)("unitid")).ToString & "," & If(IsDBNull(dt.Rows(i)("lotid")), "null", dt.Rows(i)("lotid")).ToString & " ,NOW() , '" & dt.Rows(i)("price").ToString & "') "
            End If

        Next

        sql2 += ";"

        connect.ExecuteNonQuery(sql2)


    End Sub

    Private Function GetSqlPrdOrderDTMuchCut() As String
        Dim sql As String = ""
        sql += "SELECT lotid "
        sql += ",5 AS doctype,'" & Date.Now.Year & Date.Now.ToString("-MM-dd HH:mm:ss") & "' AS dateprd,stkid,prdcode,1 AS `status`,1 AS cvf"
        sql += ",SUM(qty) * (-1) AS qtytotal,SUM(qty) , unitid,CURRENT_TIMESTAMP() AS d_update,0 AS f_lock "
        sql += "FROM ("
        sql += "SELECT pdt.prdcode,prd.stkid,pdt.lotid,mp.f_lot,pdt.unitid,pdt.qty FROM "
        sql += "(SELECT orderid,prdcode,chkin_date,lotid,qty,f_stk,unitid FROM  prdorderdt WHERE f_comp = 1 AND f_cancel = 0 AND lotid IS NOT NULL AND f_stk = 1 ) AS pdt "
        sql += "INNER JOIN (SELECT orderid,stkid FROM  prdorder WHERE stkid IS NOT NULL) AS prd ON pdt.orderid = prd.orderid "
        sql += "INNER JOIN (SELECT prdcode,f_stk,f_lot FROM  masproduct WHERE f_stk = 1) AS mp ON pdt.prdcode = mp.prdcode "
        sql += ") AS tbx "
        sql += "GROUP BY lotid "
        sql += ",5"
        sql += ",'" & Date.Now.Year & Date.Now.ToString("-MM-dd HH:mm:ss") & "'"
        sql += ",stkid"
        sql += ",prdcode"
        sql += ",1"
        sql += ",1"
        sql += ",unitid"
        sql += ",CURRENT_TIMESTAMP()"
        Return sql
    End Function

    Private Function GetSqlPrdOrderDTMuchCutNotSum() As String
        Dim sql As String = ""
        sql += "SELECT DISTINCT `id` "
        sql += "FROM ("
        sql += "SELECT pdt.`id`,pdt.prdcode,prd.stkid,pdt.lotid,mp.f_lot,pdt.unitid,pdt.qty FROM "
        sql += "(SELECT `id`,orderid,prdcode,chkin_date,lotid,qty,f_stk,unitid FROM  prdorderdt WHERE f_comp = 1 AND f_cancel = 0 AND lotid IS NOT NULL) AS pdt "
        sql += "INNER JOIN (SELECT orderid,stkid FROM  prdorder WHERE stkid IS NOT NULL) AS prd ON pdt.orderid = prd.orderid "
        sql += "INNER JOIN (SELECT prdcode,f_stk,f_lot FROM  masproduct WHERE f_stk = 1) AS mp ON pdt.prdcode = mp.prdcode "
        sql += ") AS tbx "

        Return sql
    End Function

End Class
