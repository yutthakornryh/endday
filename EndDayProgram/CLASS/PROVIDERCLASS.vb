﻿Imports System.Globalization

<System.Runtime.InteropServices.GuidAttribute("EDE655F0-8F66-4F30-8367-247287A74233")> Public Class PROVIDERCLASS
    Dim sql As String = "SELECT CONCAT(`name`,' ',lname) AS `provider` FROM  hospemp "
    Dim cultures As CultureInfo = New CultureInfo("en-US")

    Dim sql1 As String = "SELECT hospemp.empid , usrpolicy.upid , usrpolicy.empid , usrpolicy.prgid , usrpolicy.padd AS PADD  , usrpolicy.pdel AS PDEL , usrpolicy.pupd  AS PUPD , usrpolicy.pprt  AS PPRT , psee AS PSEE, prgcode ,prgname ,usreq , name ,lname , hospemp.clinic ,masclinic.stkid  FROM  usrpolicy JOIN  masprg ON masprg.prgid  = usrpolicy.prgid   JOIN hospemp ON hospemp.empid = usrpolicy.empid JOIN masclinic ON masclinic.clinic = hospemp.clinic "

    Dim dt As DataTable

    'เช็ค login User
    Public Property STKID As String
    Public Property checklogin_user_ As Boolean

    Public Property LOGINUSERMSG_ As String

    Public Property policies_usr_ As Boolean

    Public Property user_ As Boolean

    'ID EMP
    Public Property txtusername_tag_ As String

    Public Property txtusername_ As String

    Public Property btnlogout_ As Boolean

    Public Property _CHECKUSRFAIL As Boolean

    Public Property _CHECKUSRFAILMSG As String

    Public Property PADD_ As Boolean

    Public Property PADDMSG_ As String

    Public Property PDEL_ As Boolean

    Private PDELMSG As String
    Public Property PDELMSG_ As String

    Private PUPD As Boolean
    Public Property PUPD_ As Boolean
    Private PUPDMSG As String
    Public Property PUPDMSG_ As String

    Public Property PPRT_ As Boolean
    Private PPRTMSG As String
    Public Property PPRTMSG_ As String

    Public Property PSEE_ As Boolean
    Private PSEEMSG As String
    Public Property PSEEMSG_ As String

    Public Property CLINIC As String

    Public Sub New()
        CHECKMSG()
    End Sub

    ''' <summary>
    ''' ต้องการดึงข้อมูลรหัสพนักงาน
    ''' </summary>
    Public Function GETEMPID(USERSEQ As String) As String
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As DataTable = New DataTable
        Dim sql2 As String = sql1 + "  WHERE hospemp.usreq ='" & MySql.Data.MySqlClient.MySqlHelper.EscapeString(USERSEQ) & "';"
        dt = db.GetTable(sql2)
        db.Dispose()
        Return dt.Rows(0).Item("empid")
    End Function

    ''' <summary>
    ''' ต้องการดึงข้อมูลรหัสแพทย์พยาบาล
    ''' </summary>
    Public Function GETPROVIDER(USERSEQ As String) As Integer
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As DataTable = New DataTable
        Dim sql2 As String = sql1 + "  WHERE hospemp.usreq ='" & MySql.Data.MySqlClient.MySqlHelper.EscapeString(USERSEQ) & "';"
        dt = db.GetTable(sql2)
        db.Dispose()
        Try
            Return If(dt.Rows(0).Item("empid") Is DBNull.Value, 0, dt.Rows(0).Item("empid"))

        Catch ex As Exception

        End Try

    End Function

    ''' <summary>
    ''' ดึงข้อมูลคนไข้ของหมอคนนั้น
    ''' </summary>
    ''' <param name="CLICNICID">clinicid</param>
    Public Function GETDoctorZone(CLICNICID As String) As DataTable

        Dim dt As DataTable = New DataTable
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        sql = ""
        sql += "SELECT p.empid AS 'PROVIDER',p.registerno AS 'REGISTERNO',CONCAT(p.`name`,' ',p.lname) AS DOCTORNAME,CASE WHEN `Wait` IS NULL THEN 0 ELSE `Wait` END AS `Wait`,CASE WHEN `Complete` IS NULL THEN 0 ELSE `Complete` END AS `Complete`,p.f_login AS 'F_LOGIN',p.clinic,dcl FROM (SELECT * FROM hospemp WHERE f_dr = 1 AND `status` = 1) AS p LEFT JOIN (SELECT vn,doctor,COUNT(vn) AS Wait FROM frnclinic WHERE  cstatus IN(1,2,4) AND clinic = 6  AND sentime > '" & Now.ToString("yyyy-", cultures) & Now.ToString("MM-dd") & " 00:00:00' AND sentime < '" & Now.ToString("yyyy-", cultures) & Now.ToString("MM-dd") & " 23:59:59'    GROUP BY doctor) AS w ON p.empid = w.doctor LEFT JOIN (SELECT vn,doctor,COUNT(vn) AS Complete  FROM frnclinic WHERE cstatus IN(3,5,6 ) AND clinic = 6  AND sentime > '" & Now.ToString("yyyy-", cultures) & Now.ToString("MM-dd") & " 00:00:00' AND sentime < '" & Now.ToString("yyyy-", cultures) & Date.Now.ToString("MM-dd") & " 23:59:59'  GROUP BY doctor) AS s ON p.empid = s.doctor ORDER BY p.f_login DESC;"


        sql += ""
        dt = db.GetTable(sql)
        Return dt
    End Function

    ''' <summary>
    ''' ดึงข้อมูลคนไข้ของหมอคนนั้น
    ''' </summary>
    Public Function GETDoctorZoneALL() As DataTable
        Dim dt As DataTable = New DataTable
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection


        Dim sql As String = ""
        sql += "SELECT p.empid AS 'PROVIDER',p.registerno AS 'REGISTERNO',CONCAT(p.`name`,' ',p.lname) AS DOCTORNAME,CASE WHEN `Wait` IS NULL THEN 0 ELSE `Wait` END AS `Wait`,CASE WHEN `Complete` IS NULL THEN 0 ELSE `Complete` END AS `Complete`,p.f_login AS 'F_LOGIN',p.clinic,dcl FROM (SELECT * FROM hospemp WHERE f_dr = 1 AND `status` = 1) AS p LEFT JOIN (SELECT vn,doctor,COUNT(vn) AS Wait FROM frnclinic WHERE  cstatus IN(1,2,4) AND clinic = 6  AND sentime > '" & Now.ToString("yyyy-", cultures) & Date.Now.ToString("MM-dd") & " 00:00:00' AND sentime < '" & Now.ToString("yyyy-", cultures) & Date.Now.ToString("MM-dd") & " 23:59:59'    GROUP BY doctor) AS w ON p.empid = w.doctor LEFT JOIN (SELECT vn,doctor,COUNT(vn) AS Complete  FROM frnclinic WHERE cstatus IN(3,5,6 ) AND clinic = 6  AND sentime > '" & Now.ToString("yyyy-", cultures) & Date.Now.ToString("MM-dd") & " 00:00:00' AND sentime < '" & Now.ToString("yyyy-", cultures) & Date.Now.ToString("MM-dd") & " 23:59:59'  GROUP BY doctor) AS s ON p.empid = s.doctor ORDER BY p.f_login DESC;"
        dt = db.GetTable(sql)
        Return dt
    End Function

    ''' <summary>
    ''' ดึงข้อมูลหมอที่ Online
    ''' </summary>
    ''' <param name="CLICNICID">clinicid</param>


    ' function GETHOSEM มีสองแบบ แก้บัคเบื้องต้น ส่ง IDUSER REQUEST กับ ชื่อ FORM.TAG ส่งมาเพื่อ ใช้เช็คสิทธิ์ในการเข้าใช้งาน

    Public Sub LOGOUT(PRGNAME As String)
        btnlogout_ = False
        txtusername_ = Nothing
        txtusername_tag_ = Nothing
        checklogin_user_ = False
        LOADDEFAULTPOLICIES(PRGNAME)
    End Sub
    ''' <summary>
    ''' เมื่อไม่มีการlogin Defaultของการใช้งานโปรแกรม
    ''' </summary>
    Public Sub LOADDEFAULTPOLICIES(PRGNAME As String)
        Dim sql2 As String = sql1 + "WHERE prgcode = '" & PRGNAME & "' AND  hospemp.empid = 1 ;"
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        dt = db.GetTable(sql2)
        If dt.Rows.Count <= 0 Then
            PADD_ = False
            PDEL_ = False
            PUPD_ = False
            PPRT_ = False
            PSEE_ = False
            _CHECKUSRFAIL = True
            ' _objloginusr._btnlogout = True

        Else
            PADD_ = Convert.ToBoolean(dt.Rows(0)("padd"))
            PDEL_ = Convert.ToBoolean(dt.Rows(0)("pdel"))
            PUPD_ = Convert.ToBoolean(dt.Rows(0)("pupd"))
            PPRT_ = Convert.ToBoolean(dt.Rows(0)("pprt"))
            PSEE_ = Convert.ToBoolean(dt.Rows(0)("psee"))
            txtusername_ = dt.Rows(0)("name") & " " & dt.Rows(0)("lname")
            txtusername_tag_ = dt.Rows(0)("empid")
        End If
    End Sub
    ''' <summary>
    ''' เช็คสิทธิการใช้งานโปรแกรม
    ''' </summary>
    ''' <remarks>
    ''' เมื่อส่งมาจะเก็บไว้ในตัวแปรต่างๆไว้ให้เรียกใช้
    ''' #login,#user,#request
    ''' </remarks>
    Public Sub GETHOSEM(IDUSER As String, PRGNAME As String)

        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql2 As String = sql1 + " WHERE  usreq = '" & MySql.Data.MySqlClient.MySqlHelper.EscapeString(IDUSER) & "' AND  prgcode ='" & PRGNAME & "';"
        dt = db.GetTable(sql2)
        If dt.Rows.Count <= 0 Then
            Dim sqlcheck As String = sql1 + " WHERE usreq = '" & MySql.Data.MySqlClient.MySqlHelper.EscapeString(IDUSER) & "' ; "
            Dim dtcheckuser As New DataTable
            Dim db1 As ConnecDBRYH = ConnecDBRYH.NewConnection
            dtcheckuser = db1.GetTable(sqlcheck)
            If dtcheckuser.Rows.Count <= 0 Then
                user_ = False
            Else
                user_ = True
            End If
            PADD_ = False
            PDEL_ = False
            PUPD_ = False
            PPRT_ = False
            PSEE_ = False
            _CHECKUSRFAIL = True
            ' _objloginusr._btnlogout = True
            btnlogout_ = False
            txtusername_ = Nothing
            txtusername_tag_ = Nothing
            policies_usr_ = False
            checklogin_user_ = False
            LOADDEFAULTPOLICIES(PRGNAME)
        Else
            STKID = If(IsDBNull(dt.Rows(0)("stkid")), "null", dt.Rows(0)("stkid"))

            user_ = True
            PADD_ = Convert.ToBoolean(dt.Rows(0)("padd"))
            PDEL_ = Convert.ToBoolean(dt.Rows(0)("pdel"))
            PUPD_ = Convert.ToBoolean(dt.Rows(0)("pupd"))
            PPRT_ = Convert.ToBoolean(dt.Rows(0)("pprt"))
            PSEE_ = Convert.ToBoolean(dt.Rows(0)("psee"))
            _CHECKUSRFAIL = False
            btnlogout_ = True
            policies_usr_ = True
            checklogin_user_ = True
            txtusername_ = dt.Rows(0)("name") & " " & dt.Rows(0)("lname")
            txtusername_tag_ = dt.Rows(0)("empid")
            CLINIC = dt.Rows(0)("clinic")
        End If
        'เรียก function ใส่ข้อความ MSG
        CHECKMSG()
        db.Dispose()
        '   Return objloginusr
    End Sub

    Public Function CheckUserPrgEvent(IDUSER As String, PRGNAME As String, EventAction As String) As Boolean

        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql2 As String = sql1 + " WHERE  hospemp.empid = '" & MySql.Data.MySqlClient.MySqlHelper.EscapeString(IDUSER) & "' AND  prgcode ='" & PRGNAME & "';"
        dt = db.GetTable(sql2)
        If dt.Rows.Count <= 0 Then
            Return False
        Else

            If EventAction = "PADD" Then
                Return Convert.ToBoolean(dt.Rows(0)("padd"))

            ElseIf EventAction = "PDEL" Then
                Return Convert.ToBoolean(dt.Rows(0)("pdel"))

            ElseIf EventAction = "PUPD" Then
                Return Convert.ToBoolean(dt.Rows(0)("pupd"))

            ElseIf EventAction = "PPRT" Then
                Return Convert.ToBoolean(dt.Rows(0)("pprt"))

            ElseIf EventAction = "PSEE" Then
                Return Convert.ToBoolean(dt.Rows(0)("psee"))

            Else
                Return False
            End If
        End If
    End Function

    ''' <summary>
    ''' ฟังชันนี้ทำเสร็จในคลาสProviderเป้นการเช็คการคืนค่าMSGข้อความ
    ''' </summary>
    ''' <remarks>เป็นการเช็คว่าเวลาล้อกอินมีปัญหาตรงไหน จะเก็บไว้ในข้อความ</remarks>
    ''' นำข้อความาใส่ ใน MSG ไว้แสดงผล สิทธิ์ การเข้าใช้งาน

    Public Sub CHECKMSG()
        If PADD_ = False Then
            PADDMSG_ = ENVIRONMENTCLASS.USERUSEEVENT.ADDMSG_F
        ElseIf PADD_ = True Then
            PADDMSG_ = ENVIRONMENTCLASS.USERUSEEVENT.ADDMSG_T
        End If

        If PDEL_ = False Then
            PDELMSG_ = ENVIRONMENTCLASS.USERUSEEVENT.DELMSG_F
        ElseIf PDEL_ = True Then
            PDELMSG = ENVIRONMENTCLASS.USERUSEEVENT.DELMSG_T
        End If

        If PUPD_ = False Then
            PUPDMSG_ = ENVIRONMENTCLASS.USERUSEEVENT.EDITMSG_F
        ElseIf PUPD_ = True Then
            PUPDMSG_ = ENVIRONMENTCLASS.USERUSEEVENT.EDITMSG_T
        End If

        If PPRT_ = False Then
            PPRTMSG_ = ENVIRONMENTCLASS.USERUSEEVENT.PRINTMSG_F
        ElseIf PPRT_ = True Then
            PPRTMSG_ = ENVIRONMENTCLASS.USERUSEEVENT.PRINTMSG_T
        End If

        If PSEE_ = False Then
            PSEEMSG_ = ENVIRONMENTCLASS.USERUSEEVENT.VIEWMSG_F
        ElseIf PSEE_ = True Then
            PSEEMSG_ = ENVIRONMENTCLASS.USERUSEEVENT.VIEWMSG_T
        End If

        If _CHECKUSRFAIL = False Then
            _CHECKUSRFAILMSG = ""
        ElseIf _CHECKUSRFAIL = True Then

            If user_ = True Then
                If policies_usr_ = False Then
                    _CHECKUSRFAILMSG = ENVIRONMENTCLASS.USERUSEEVENT.USERFAILMSG_POLICIES_TH

                Else
                    _CHECKUSRFAILMSG = ENVIRONMENTCLASS.USERUSEEVENT.USERFAILMSG_TH_F
                End If
            ElseIf user_ = False Then
                _CHECKUSRFAILMSG = ENVIRONMENTCLASS.USERUSEEVENT.USERFAILMSG_NOTREGIST_TH
            End If

        End If
        If checklogin_user_ = False Then
            LOGINUSERMSG_ = ENVIRONMENTCLASS.USERUSEEVENT.USERCANNOT_LOGIN_TH
        End If

    End Sub

End Class