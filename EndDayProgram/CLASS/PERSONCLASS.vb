﻿Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Drawing
Imports System.Windows.Forms
Imports MySql.Data.MySqlClient
Imports System.Data
'Class ของเวชระเบียน
Public Class PERSONCLASS
#Region "SetCmd"

    Public Property getCmdSelectAddress As String
        Get

            Dim cmd As String = "SELECT addresdec1,addresstype,postcode,telephone,mobile,housetype,ampid,codechangwat,tmbid,country"

            Return cmd
        End Get
        Set(ByVal Value As String)

        End Set
    End Property

    Public Property getCmdSelectPerson As String
        Get

            Dim cmd As String = "SELECT hn,cid,prename,`name`,lname,hn,sex,birth,race,nation,religion,education,abogroup,rhgroup,passport,typep,ename,emname,elname,`mstatus`,occupation,father,fname,mother,mname,email,penname,vstatus,contact_address,emname,drugalert,discharge,ddischarge,f_card,penid,pc"
            Return cmd
        End Get
        Set(ByVal Value As String)

        End Set
    End Property
    Public Property getCmdSelectDischarge As String
        Get

            Dim cmd As String = "SELECT dischgid,discharge,status"

            Return cmd
        End Get
        Set(ByVal Value As String)

        End Set
    End Property

#End Region

    Dim x As Date
    Private mySqlCommand As New MySqlCommand
    Private mySqlAdaptor As New MySqlDataAdapter
    Private mySqlReader As MySqlDataReader
    Private idlast As String
    '///////////////// ประกาศตัวแปร Table Person////////////
#Region "Variable Person"
    Private HOSPCODE As String = ""
    Private HN As UInt64 '
    Private CID As String '
    Private TYPEP As Integer '
    Private PRENAME As Integer '
    Private NAME As String '
    Private LNAME As String '
    Private ENAME As String '
    Private EMNAME As String '
    Private ELNAME As String '
    Private PENNAME As Integer '
    Private BIRTH As String '
    Private SEX As Integer '
    Private MSTATUS As Integer '
    Private OCCUPATION As Integer '
    Private RACE As Integer '
    Private NATION As Integer '
    Private RELIGION As Integer '
    Private EDUCATION As Integer '
    Private FATHER As String '
    Private FNAME As String '
    Private MOTHER As String '
    Private MNAME As String '
    Private COUPLE As String '
    Private CNAME As String
    Private VSTATUS As Integer '
    Private DISCHARGE As Integer '
    Private DDISCHARGE As Date '
    Private ABOGROUP As Integer '
    Private RHGROUP As Integer '
    Private PASSPORT As String '
    Private EMAIL As String '
    Private FDATE As DateTime '
    Private DRUGALERT As Integer '
    Private LOCATION As Integer '
    Private D_UPDATE As String '
    Private LOCATIONPHR As Integer
    Private F_CARD As Integer
    Private PENID As Integer
    Private PC As Integer
    Private CONTACT_ADDRESS As Integer ' การติดต่อ
    Private USMK As Integer

#End Region
    '///////////////// ประกาศตัวแปร Table Address //////////////////////
#Region "Variable Address"
    Private ADDRESSTYPE As Integer ' ประเภทของที่อยู่ ดึงมาจาก MASADDRESSTYPE
    Private HOUSETYPE As Integer ' ลักษณะที่อยู่ ดึงมาจาก MASHOUSETYPE
    Private ADDRESDEC1 As String ' ที่อยู่ 1
    Private ADDRESDEC2 As String ' ที่อยุ่ 2
    Private ADDRESDEC3 As String ' ที่อยู่ 3
    Private CHANGWAT1 As String = "0" ' จังหวัด 1
    Private CHANGWAT2 As String = "0" ' จังหวัด 2
    Private CHANGWAT3 As String = "0" ' จังหวัด 3
    Private AMPUR1 As String = "0" ' ตำบล 1
    Private AMPUR2 As String = "0" ' ตำบล 2
    Private AMPUR3 As String = "0" ' ตำบล 3
    Private TMB1 As String = "0" ' ตำบล 1
    Private TMB2 As String = "0" ' ตำบล 2
    Private TMB3 As String = "0" ' ตำบล 3
    Private VILLAGE1 As String = "0" ' หมูบ้าน 1
    Private VILLAGE2 As String = "0" ' หมูบ้าน 2
    Private VILLAGE3 As String = "0" ' หมูบ้าน 3
    Private POSTCODE1 As String ' รหัสไปรษณีย์1
    Private POSTCODE2 As String ' รหัสไปรษณีย์2
    Private POSTCODE3 As String ' รหัสไปรษณีย์3
    Private COUNTRY As Integer ' ประเทศ ดึงมาจาก MASCOUNTRY
    Private COUNTRY2 As Integer
    Private COUNTRY3 As Integer
    Private TELEPHONE As String ' เบอร์โทรสัพท์บ้าน
    Private TELEPHONE2 As String
    Private MOBILE As String ' เบอร์โทรศัพท์มือถือ
    Private ADDRESSID As Integer
#End Region
    '///////////////// ประกาศตัวแปร Table PERSONCONTACT //////////////////////
    'PC = PERSONCONTACT
#Region "ประกาศตัวแปร Table PERSONCONTACT"
    Private NAMEPC As String
    Private LNAMEPC As String
    Private TELEPHONEPC As String
    Private MOBILEPC As String
    Private RELATION As Integer
#End Region
    '///////////////// ประกาศตัวแปร Table PNALERT //////////////////////
#Region "ประกาศตัวแปร Table PNALERT"
    Private COLOR As String
    Private TITLE As String
    Private ALTDATE As String
    Private PALTID As Integer
    Private COMMENT As String
    Private CLINICID As Integer
#End Region
    '/////////////// พี่บอยเพิ่มตัวแปร ///////////////////
#Region "พี่บอยเพิ่มตัวแปร"
    Private STPRENAME As String
    Private SEPRENAME As String
    Private BIRTH2 As Date
    Private RELATIONNAME As String
    Private ABOGROUPDESC As String
    Private AgeYear As Integer
    Private AgeMonth As Integer
    Private AgeDay As Integer
    'Private ISNEWPATIENT As Boolean 'ตัวแปรคนใข้ใหม่
    Private ISPENNAME As Boolean 'ตัวแปรนามแฝง
#End Region
    '///////////////  HISFRNSLOG ///////////////////
#Region "HISFRNSLOG"
    Private DATE_ACT As String
    Private ACT As String
    Private OLD_VALUE As String
    Private NEW_VALUE As String
    Private USERID As Integer
    Private PRGID As String
#End Region
    '/////////////// HISPNRENAME ///////////////////
#Region "HISPNRENAME"
    Private REPRENAME As String
    Private RENAME As String
    Private REENAME As String
    Private REEMNAME As String
    Private RELNAME As String
    Private REELNAME As String
    Private REMARK As String
    Private REPROVIDER As String
#End Region
    '-------------เพิ่มตัวแปร person data---------------
#Region "เพิ่มตัวแปร person data"
    Private MSTATUSDESC As String
    Private NATIONDESC As String
    Private RACEDESC As String
    Private RELIGIONDESC As String
    Private EDUCATEDESC As String
    Private OCCUPATIONDESC As String
    Private RHGROUPDESC As String
    Private VSTATUSDESC As String
    Private COUNTRYDESC As String
    Private COUNTRYDESC2 As String
    Private COUNTRYDESC3 As String
    Private SUBDISTRICTDESC As String
    Private SUBDISTRICTDESC2 As String
    Private SUBDISTRICTDESC3 As String
    Private AMPURDESC As String
    Private AMPURDESC2 As String
    Private AMPURDESC3 As String
    Private CHANGWATDESC As String
    Private CHANGWATDESC2 As String
    Private CHANGWATDESC3 As String
#End Region
    Structure Duplicate
        Public _HN As String
        Public _dulpi As Boolean
        Public _Name As String
        Public _Lastname As String
        Public _CID As String
        Public _VN As String
        Public _AN As String
        Public _STAT As String
    End Structure
    Private HN_Duplicate As Duplicate
    Dim USCulture As New System.Globalization.CultureInfo("en-US", True)
#Region "Set Variable"
    Public Property Duplicate_ As Duplicate
        Get
            Return HN_Duplicate
        End Get
        Set(ByVal Value As Duplicate)
            HN_Duplicate = Value
        End Set
    End Property
    Public Property PC_() As Integer
        Get
            Return PC
        End Get
        Set(ByVal Value As Integer)
            PC = Value
        End Set
    End Property
    Public Property COUNTRY3_() As Integer
        Get
            Return COUNTRY3
        End Get
        Set(ByVal Value As Integer)
            COUNTRY3 = Value
        End Set
    End Property
    Public Property COUNTRY2_() As Integer
        Get
            Return COUNTRY2
        End Get
        Set(ByVal Value As Integer)
            COUNTRY2 = Value
        End Set
    End Property
    Public Property PENID_() As Integer
        Get
            Return PENID
        End Get
        Set(ByVal Value As Integer)
            PENID = Value
        End Set
    End Property
    Public Property LOCATIONPHR_() As Integer
        Get
            Return LOCATIONPHR
        End Get
        Set(ByVal Value As Integer)
            LOCATIONPHR = Value
        End Set
    End Property
    Public Property USMK_() As Integer
        Get
            Return USMK
        End Get
        Set(ByVal Value As Integer)
            USMK = Value
        End Set
    End Property
    Public Property CHANGWAT1_() As String
        Get
            Return CHANGWAT1
        End Get
        Set(ByVal Value As String)
            CHANGWAT1 = Value
        End Set
    End Property
    Public Property CHANGWAT2_ As String
        Get
            Return CHANGWAT2
        End Get
        Set(ByVal Value As String)
            CHANGWAT2 = Value
        End Set
    End Property
    Public Property CHANGWAT3_ As String
        Get
            Return CHANGWAT3
        End Get
        Set(ByVal Value As String)
            CHANGWAT3 = Value
        End Set
    End Property

    Public Property AMPUR1_ As String
        Get
            Return AMPUR1
        End Get
        Set(ByVal Value As String)
            AMPUR1 = Value
        End Set
    End Property
    Public Property AMPUR2_ As String
        Get
            Return AMPUR2
        End Get
        Set(ByVal Value As String)
            AMPUR2 = Value
        End Set
    End Property
    Public Property AMPUR3_ As String
        Get
            Return AMPUR3
        End Get
        Set(ByVal Value As String)
            AMPUR3 = Value
        End Set
    End Property
    Public Property TMB1_ As String
        Get
            Return TMB1
        End Get
        Set(ByVal Value As String)
            TMB1 = Value
        End Set
    End Property
    Public Property TMB2_ As String
        Get
            Return TMB2
        End Get
        Set(ByVal Value As String)
            TMB2 = Value
        End Set
    End Property
    Public Property TMB3_ As String
        Get
            Return TMB3
        End Get
        Set(ByVal Value As String)
            TMB3 = Value
        End Set
    End Property
    Public Property REELNAME_() As String
        'Get คืนค่า
        Get
            Return REELNAME
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            REELNAME = Value
        End Set
    End Property
    Public Property REENAME_() As String
        'Get คืนค่า
        Get
            Return REENAME
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            REENAME = Value
        End Set
    End Property
    Public Property REPROVIDER_() As String
        'Get คืนค่า
        Get
            Return REPROVIDER
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            REPROVIDER = Value
        End Set
    End Property
    Public Property REMARK_() As String
        'Get คืนค่า
        Get
            Return REMARK
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            REMARK = Value
        End Set
    End Property
    Public Property RELNAME_() As String
        'Get คืนค่า
        Get
            Return RELNAME
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            RELNAME = Value
        End Set
    End Property
    Public Property REEMNAME_() As String
        'Get คืนค่า
        Get
            Return REEMNAME
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            REEMNAME = Value
        End Set
    End Property
    Public Property RENAME_() As String
        'Get คืนค่า
        Get
            Return RENAME
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            RENAME = Value
        End Set
    End Property
    Public Property REPRENAME_() As String
        'Get คืนค่า
        Get
            Return REPRENAME
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            REPRENAME = Value
        End Set
    End Property
    Public Property PRGID_() As String
        'Get คืนค่า
        Get
            Return PRGID
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            PRGID = Value
        End Set
    End Property
    Public Property USERID_() As Integer
        'Get คืนค่า
        Get
            Return USERID
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As Integer)
            USERID = Value
        End Set
    End Property
    Public Property NEW_VALUE_() As String
        'Get คืนค่า
        Get
            Return NEW_VALUE
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            NEW_VALUE = Value
        End Set
    End Property
    Public Property OLD_VALUE_() As String
        'Get คืนค่า
        Get
            Return OLD_VALUE
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            OLD_VALUE = Value
        End Set
    End Property
    Public Property ACT_() As String
        'Get คืนค่า
        Get
            Return ACT
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            ACT = Value
        End Set
    End Property
    Public Property DATE_ACT_() As String
        'Get คืนค่า
        Get
            Return DATE_ACT
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            DATE_ACT = Value
        End Set
    End Property
    Public Property CLINICID_() As Integer
        'Get คืนค่า
        Get
            Return CLINICID
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As Integer)
            CLINICID = Value
        End Set
    End Property
    Public Property COMMENT_() As String
        'Get คืนค่า
        Get
            Return COMMENT
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            COMMENT = Value
        End Set
    End Property
    Public Property ADDRESSID_() As Integer
        'Get คืนค่า
        Get
            Return ADDRESSID
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As Integer)
            ADDRESSID = Value
        End Set
    End Property
    Public Property CONTACT_ADDRESS_() As Integer
        'Get คืนค่า
        Get
            Return CONTACT_ADDRESS
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As Integer)
            CONTACT_ADDRESS = Value
        End Set
    End Property
    Public Property POSTCODE3_() As String
        'Get คืนค่า
        Get
            Return POSTCODE3
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            POSTCODE3 = Value
        End Set
    End Property
    Public Property TELEPHONE2_() As String
        'Get คืนค่า
        Get
            Return TELEPHONE2
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            TELEPHONE2 = Value
        End Set
    End Property
    Public Property ADDRESDEC3_() As String
        'Get คืนค่า
        Get
            Return ADDRESDEC3
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            ADDRESDEC3 = Value
        End Set
    End Property
    Public Property PALTID_() As Integer
        'Get คืนค่า
        Get
            Return PALTID
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As Integer)
            PALTID = Value
        End Set
    End Property
    Public Property ALTDATE_() As String
        'Get คืนค่า
        Get
            Return ALTDATE
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            ALTDATE = Value
        End Set
    End Property
    Public Property TITLE_() As String
        'Get คืนค่า
        Get
            Return TITLE
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            TITLE = Value
        End Set
    End Property
    Public Property COLOR_() As String
        'Get คืนค่า
        Get
            Return COLOR
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            COLOR = Value
        End Set
    End Property
    Public Property RELATION_() As Integer
        'Get คืนค่า
        Get
            Return RELATION
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As Integer)
            RELATION = Value
        End Set
    End Property
    Public Property MOBILEPC_() As String
        'Get คืนค่า
        Get
            Return MOBILEPC
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            MOBILEPC = Value
        End Set
    End Property
    Public Property TELEPHONEPC_() As String
        'Get คืนค่า
        Get
            Return TELEPHONEPC
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            TELEPHONEPC = Value
        End Set
    End Property
    Public Property LNAMEPC_() As String
        'Get คืนค่า
        Get
            Return LNAMEPC
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            LNAMEPC = Value
        End Set
    End Property
    Public Property NAMEPC_() As String
        'Get คืนค่า
        Get
            Return NAMEPC
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            NAMEPC = Value
        End Set
    End Property
    Public Property MOBILE_() As String
        'Get คืนค่า
        Get
            Return MOBILE
        End Get
        'Set รับค่าและทำงาน
        Set(ByVal Value As String)
            MOBILE = Value
        End Set
    End Property
    Public Property TELEPHONE_() As String
        Get
            Return TELEPHONE
        End Get
        Set(ByVal Value As String)
            TELEPHONE = Value
        End Set
    End Property
    Public Property COUNTRY_() As Integer
        Get
            Return COUNTRY
        End Get
        Set(ByVal Value As Integer)
            COUNTRY = Value
        End Set
    End Property
    Public Property POSTCODE2_() As String
        Get
            Return POSTCODE2
        End Get
        Set(ByVal Value As String)
            POSTCODE2 = Value
        End Set
    End Property
    Public Property POSTCODE1_() As String
        Get
            Return POSTCODE1
        End Get
        Set(ByVal Value As String)
            POSTCODE1 = Value
        End Set
    End Property
    Public Property VILLAGE3_() As String
        Get
            Return VILLAGE3
        End Get
        Set(ByVal Value As String)
            VILLAGE3 = Value
        End Set
    End Property
    Public Property VILLAGE2_() As String
        Get
            Return VILLAGE2
        End Get
        Set(ByVal Value As String)
            VILLAGE2 = Value
        End Set
    End Property
    Public Property VILLAGE1_() As String
        Get
            Return VILLAGE1
        End Get
        Set(ByVal Value As String)
            VILLAGE1 = Value
        End Set
    End Property
    Public Property ADDRESDEC2_() As String
        Get
            Return ADDRESDEC2
        End Get
        Set(ByVal Value As String)
            ADDRESDEC2 = Value
        End Set
    End Property
    Public Property ADDRESDEC1_() As String
        Get
            Return ADDRESDEC1
        End Get
        Set(ByVal Value As String)
            ADDRESDEC1 = Value
        End Set
    End Property
    Public Property HOUSETYPE_() As Integer
        Get
            Return HOUSETYPE
        End Get
        Set(ByVal Value As Integer)
            HOUSETYPE = Value
        End Set
    End Property
    Public Property ADDRESSTYPE_() As Integer
        Get
            Return ADDRESSTYPE
        End Get
        Set(ByVal Value As Integer)
            ADDRESSTYPE = Value
        End Set
    End Property
    Public Property MNAME_() As String
        Get
            Return MNAME
        End Get
        Set(ByVal Value As String)
            MNAME = Value
        End Set
    End Property
    Public Property FNAME_() As String
        Get
            Return FNAME
        End Get
        Set(ByVal Value As String)
            FNAME = Value
        End Set
    End Property
    Public Property EMNAME_() As String
        Get
            Return EMNAME
        End Get
        Set(ByVal Value As String)
            EMNAME = Value
        End Set
    End Property
    Public Property F_CARD_() As Boolean
        Get
            Return F_CARD
        End Get
        Set(ByVal Value As Boolean)
            If Value = True Then
                F_CARD = 1
            Else
                F_CARD = 0
            End If

        End Set
    End Property
    Public Property PENNAME_() As Boolean
        Get
            Return PENNAME
        End Get
        Set(ByVal Value As Boolean)
            If Value = True Then
                PENNAME = 1
            Else
                PENNAME = 0
            End If

        End Set
    End Property
    Public Property DISCHARGE_() As Integer
        Get
            Return DISCHARGE
        End Get
        Set(ByVal Value As Integer)
            DISCHARGE = Value
        End Set
    End Property
    Public Property MOTHER_() As String
        Get
            Return MOTHER
        End Get
        Set(ByVal Value As String)
            Dim MOTHERARRAY() As String
            'split คือ ตัดสิ่งที่เรากำหนด
            'trim คือ ตัดช่องว่าง
            'ก่อน.Split value =  1-9999-99999-99-9
            MOTHERARRAY = Value.Split("-")
            'หลัง.Split MASTHERARRAY(4)=1
            'MOTHERARRAY(3)=9999
            'MOTHERARRAY(2)=99999
            'MOTHERARRAY(1)=99
            'MOTHERARRAY(0)=9
            If MOTHERARRAY.Length > 1 Then

                MOTHER = MOTHERARRAY(0) + MOTHERARRAY(1) + MOTHERARRAY(2) + MOTHERARRAY(3) + MOTHERARRAY(4)
            Else
                MOTHER = MOTHERARRAY(0)
            End If
            MOTHER = MOTHER.Trim
        End Set
    End Property
    Public Property FATHER_() As String
        Get
            Return FATHER
        End Get
        Set(ByVal Value As String)
            Dim FATHERARRAY() As String
            'split คือ ตัดสิ่งที่เรากำหนด
            'trim คือ ตัดช่องว่าง
            'ก่อน.Split value =  1-9999-99999-99-9
            FATHERARRAY = Value.Split("-")
            'หลัง.Split FATHERARRAY(4)=1
            'FATHERARRAY(3)=9999
            'FATHERARRAY(2)=99999
            'FATHERARRAY(1)=99
            'FATHERARRAY(0)=9
            If FATHERARRAY.Length > 1 Then
                FATHER = FATHERARRAY(0) + FATHERARRAY(1) + FATHERARRAY(2) + FATHERARRAY(3) + FATHERARRAY(4)
            Else
                FATHER = FATHERARRAY(0)
            End If
            FATHER = FATHER.Trim
        End Set
    End Property
    Public Property COUPLE_() As String
        Get
            Return COUPLE
        End Get
        Set(ByVal Value As String)
            Dim COUPLEARRAY() As String
            'split คือ ตัดสิ่งที่เรากำหนด
            'trim คือ ตัดช่องว่าง
            'ก่อน.Split value =  1-9999-99999-99-9
            COUPLEARRAY = Value.Split("-")
            'หลัง.Split FATHERARRAY(4)=1
            'FATHERARRAY(3)=9999
            'FATHERARRAY(2)=99999
            'FATHERARRAY(1)=99
            'FATHERARRAY(0)=9
            If COUPLEARRAY.Length > 1 Then
                COUPLE = COUPLEARRAY(0) + COUPLEARRAY(1) + COUPLEARRAY(2) + COUPLEARRAY(3) + COUPLEARRAY(4)
            Else
                COUPLE = COUPLEARRAY(0)
            End If
            COUPLE = COUPLE.Trim
        End Set
    End Property
    'Public Property COUPLE_() As String
    '    Get
    '        Return COUPLE
    '    End Get
    '    Set(ByVal Value As String)
    '        COUPLE = Value
    '    End Set
    'End Property
    Public Property CNAME_() As String
        Get
            Return CNAME
        End Get
        Set(ByVal Value As String)
            CNAME = Value
        End Set
    End Property
    Public Property LOCATION_() As Integer
        Get
            Return LOCATION
        End Get
        Set(ByVal Value As Integer)
            LOCATION = Value
        End Set
    End Property
    Public Property DRUGALERT_() As Boolean
        Get
            Return DRUGALERT
        End Get
        Set(ByVal Value As Boolean)
            If Value = True Then
                DRUGALERT = 1
            Else
                DRUGALERT = 0
            End If
        End Set
    End Property
    Public Property EMAIL_() As String
        Get
            Return EMAIL
        End Get
        Set(ByVal Value As String)
            EMAIL = Value
        End Set
    End Property
    Public Property TYPEP_() As Integer
        Get
            Return TYPEP
        End Get
        Set(ByVal Value As Integer)
            TYPEP = Value
        End Set
    End Property
    Public Property ELNAME_() As String
        Get
            Return ELNAME
        End Get
        Set(ByVal Value As String)
            ELNAME = Value
        End Set
    End Property
    Public Property ENAME_() As String
        Get
            Return ENAME
        End Get
        Set(ByVal Value As String)
            ENAME = Value
        End Set
    End Property
    Public Property FDATE_() As DateTime
        Get
            Return FDATE
        End Get
        Set(ByVal Value As DateTime)
            FDATE = Value
        End Set
    End Property

    Public Property D_UPDATE_() As String
        Get
            Return D_UPDATE
        End Get
        Set(ByVal Value As String)
            D_UPDATE = Value
        End Set
    End Property
    Public Property RHGROUP_() As Integer
        Get
            Return RHGROUP
        End Get
        Set(ByVal Value As Integer)
            RHGROUP = Value
        End Set
    End Property
    Public Property ABOGROUP_() As Integer
        Get
            Return ABOGROUP
        End Get
        Set(ByVal Value As Integer)
            ABOGROUP = Value
        End Set
    End Property
    Public Property DDISCHARGE_() As Date
        Get
            Return DDISCHARGE
        End Get
        Set(ByVal Value As Date)
            DDISCHARGE = Value
        End Set
    End Property

    Public Property EDUCATION_() As Integer
        Get
            Return EDUCATION
        End Get
        Set(ByVal Value As Integer)
            EDUCATION = Value
        End Set
    End Property
    Public Property RELIGION_() As Integer
        Get
            Return RELIGION
        End Get
        Set(ByVal Value As Integer)
            RELIGION = Value
        End Set
    End Property
    Public Property NATION_() As Integer
        Get
            Return NATION
        End Get
        Set(ByVal Value As Integer)
            NATION = Value
        End Set
    End Property
    Public Property RACE_() As Integer
        Get
            Return RACE
        End Get
        Set(ByVal Value As Integer)
            RACE = Value
        End Set
    End Property
    Public Property OCCUPATION_() As Integer
        Get
            Return OCCUPATION
        End Get
        Set(ByVal Value As Integer)
            OCCUPATION = Value
        End Set
    End Property
    Public Property MSTATUS_() As Integer
        Get
            Return MSTATUS
        End Get
        Set(ByVal Value As Integer)
            MSTATUS = Value
        End Set
    End Property
    Public Property BIRTH_() As String
        Get
            Return BIRTH
        End Get
        Set(ByVal Value As String)
            BIRTH = Value
        End Set
    End Property
    Public Property SEX_() As Integer
        Get
            Return SEX
        End Get
        Set(ByVal Value As Integer)
            SEX = Value
        End Set
    End Property
    Public Property HN_() As UInt64
        Get
            Return HN
        End Get
        Set(ByVal Value As UInt64)
            HN = Value
        End Set
    End Property
    Public Property LNAME_() As String
        Get
            Return LNAME
        End Get
        Set(ByVal Value As String)
            LNAME = Value
        End Set
    End Property
    Public Property NAME_() As String
        Get
            Return NAME
        End Get
        Set(ByVal Value As String)
            NAME = Value
        End Set
    End Property
    Public Property PRENAME_() As Integer
        Get
            Return PRENAME
        End Get
        Set(ByVal Value As Integer)
            PRENAME = Value
        End Set
    End Property
    Public Property VSTATUS_() As Integer
        Get
            Return VSTATUS
        End Get
        Set(ByVal Value As Integer)
            VSTATUS = Value
        End Set
    End Property
    Public Property PASSPORT_() As String
        Get
            Return PASSPORT
        End Get
        Set(ByVal Value As String)
            PASSPORT = Value
        End Set
    End Property
    Public Property HOSPCODE_() As String
        Get
            Return HOSPCODE
        End Get
        Set(ByVal Value As String)
            HOSPCODE = Value
        End Set
    End Property

    Public Property CID_() As String
        Get
            Return CID
        End Get
        Set(ByVal Value As String)
            Dim CIDARRAY() As String
            'split คือ ตัดสิ่งที่เรากำหนด
            'trim คือ ตัดช่องว่าง
            'ก่อน.Split value =  1-99999-9999-99-9
            CIDARRAY = Value.Split("-")
            'หลัง.Split FATHERARRAY(4)=1
            'CIDARRAY(3)=9999
            'CIDARRAY(2)=99999
            'CIDARRAY(1)=99
            'CIDARRAY(0)=9
            If CIDARRAY.Length > 1 Then
                CID = CIDARRAY(0) + CIDARRAY(1) + CIDARRAY(2) + CIDARRAY(3) + CIDARRAY(4)
            Else
                CID = CIDARRAY(0)
            End If
            CID = CID.Trim
        End Set
    End Property
    Public ReadOnly Property HOSCODES As String
        Get
            Return HOSPCODE
        End Get
    End Property

    '///////////////////// พี่บอยเพิ่ม Properties ////////////////////
    Public Property STPRENAME_() As String
        Get
            Return STPRENAME
        End Get
        Set(ByVal Value As String)
            STPRENAME = Value
        End Set
    End Property
    Public Property SEPRENAME_() As String
        Get
            Return SEPRENAME
        End Get
        Set(ByVal Value As String)
            SEPRENAME = Value
        End Set
    End Property
    Public Property BIRTH2_() As Date
        Get
            Return BIRTH2
        End Get
        Set(ByVal Value As Date)
            BIRTH2 = Value
        End Set
    End Property
    Public Property RELATIONNAME_() As String
        Get
            Return RELATIONNAME
        End Get
        Set(ByVal Value As String)
            RELATIONNAME = Value
        End Set
    End Property
    Public Property ABOGROUPDESC_() As String
        Get
            Return ABOGROUPDESC
        End Get
        Set(ByVal Value As String)
            ABOGROUPDESC = Value
        End Set
    End Property
    Public Property AGEMONTH_() As String
        Get
            Return AgeMonth
        End Get
        Set(ByVal Value As String)
            AgeMonth = Value
        End Set
    End Property
    Public Property AGEDAY_() As String
        Get
            Return AgeDay
        End Get
        Set(ByVal Value As String)
            AgeDay = Value
        End Set
    End Property
    Public Property AGEYEAR_() As String
        Get
            Return AgeYear
        End Get
        Set(ByVal Value As String)
            AgeYear = Value
        End Set
    End Property
    'Public Property ISNEWPATIENT_() As Boolean
    '    Get
    '        Return ISNEWPATIENT
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        ISNEWPATIENT = Value
    '    End Set
    'End Property
    Public Property ISPENNAME_() As Boolean
        Get
            Return ISPENNAME
        End Get
        Set(ByVal Value As Boolean)
            ISPENNAME = Value
        End Set
    End Property

    Public Property MSTATUSDESC_() As String
        Get
            Return MSTATUSDESC
        End Get
        Set(ByVal Value As String)
            MSTATUSDESC = Value
        End Set
    End Property

    Public Property NATIONDESC_() As String
        Get
            Return NATIONDESC
        End Get
        Set(ByVal Value As String)
            NATIONDESC = Value
        End Set
    End Property

    Public Property RACEDESC_() As String
        Get
            Return RACEDESC
        End Get
        Set(ByVal Value As String)
            RACEDESC = Value
        End Set
    End Property

    Public Property RELIGIONDESC_() As String
        Get
            Return RELIGIONDESC
        End Get
        Set(ByVal Value As String)
            RELIGIONDESC = Value
        End Set
    End Property

    Public Property EDUCATEDESC_() As String
        Get
            Return EDUCATEDESC
        End Get
        Set(ByVal Value As String)
            EDUCATEDESC = Value
        End Set
    End Property

    Public Property OCCUPATIONDESC_() As String
        Get
            Return OCCUPATIONDESC
        End Get
        Set(ByVal Value As String)
            OCCUPATIONDESC = Value
        End Set
    End Property

    Public Property RHGROUPDESC_() As String
        Get
            Return RHGROUPDESC
        End Get
        Set(ByVal Value As String)
            RHGROUPDESC = Value
        End Set
    End Property

    Public Property VSTATUSDESC_() As String
        Get
            Return VSTATUSDESC
        End Get
        Set(ByVal Value As String)
            VSTATUSDESC = Value
        End Set
    End Property

    Public Property COUNTRYDESC_() As String
        Get
            Return COUNTRYDESC
        End Get
        Set(ByVal Value As String)
            COUNTRYDESC = Value
        End Set
    End Property

    Public Property COUNTRYDESC2_() As String
        Get
            Return COUNTRYDESC2
        End Get
        Set(ByVal Value As String)
            COUNTRYDESC2 = Value
        End Set
    End Property

    Public Property COUNTRYDESC3_() As String
        Get
            Return COUNTRYDESC3
        End Get
        Set(ByVal Value As String)
            COUNTRYDESC3 = Value
        End Set
    End Property

    Public Property SUBDISTRICTDESC_() As String
        Get
            Return SUBDISTRICTDESC
        End Get
        Set(ByVal Value As String)
            SUBDISTRICTDESC = Value
        End Set
    End Property

    Public Property SUBDISTRICTDESC2_() As String
        Get
            Return SUBDISTRICTDESC2
        End Get
        Set(ByVal Value As String)
            SUBDISTRICTDESC2 = Value
        End Set
    End Property

    Public Property SUBDISTRICTDESC3_() As String
        Get
            Return SUBDISTRICTDESC3
        End Get
        Set(ByVal Value As String)
            SUBDISTRICTDESC3 = Value
        End Set
    End Property

    Public Property AMPURDESC_() As String
        Get
            Return AMPURDESC
        End Get
        Set(ByVal Value As String)
            AMPURDESC = Value
        End Set
    End Property

    Public Property AMPURDESC2_() As String
        Get
            Return AMPURDESC2
        End Get
        Set(ByVal Value As String)
            AMPURDESC3 = Value
        End Set
    End Property

    Public Property AMPURDESC3_() As String
        Get
            Return AMPURDESC3
        End Get
        Set(ByVal Value As String)
            AMPURDESC3 = Value
        End Set
    End Property

    Public Property CHANGWATDESC_() As String
        Get
            Return CHANGWATDESC
        End Get
        Set(ByVal Value As String)
            CHANGWATDESC = Value
        End Set
    End Property

    Public Property CHANGWATDESC2_() As String
        Get
            Return CHANGWATDESC2
        End Get
        Set(ByVal Value As String)
            CHANGWATDESC2 = Value
        End Set
    End Property

    Public Property CHANGWATDESC3_() As String
        Get
            Return CHANGWATDESC3
        End Get
        Set(ByVal Value As String)
            CHANGWATDESC3 = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' บันทึกข้อมูลคนไข้ใหม่
    ''' </summary>
    ''' <remarks>การบันทึกข้อมูลคนไข้ใหม่จะดึงข้อมูลมาจากตัวแปร ใน Person class ทั้งหมดไม่มีการส่งค่า มา เพราะฉะนั้นในหน้าบันทึกข้อมูล ต้องมีการเก็บข้อมูลลงในตัวแปร ต่างๆให้ครบถ้วน  #บันทึกข้อมูลผู้ป่วย,#บันทึกข้อมูลคนไข้</remarks>
    Public Function savePerson() As Boolean
        Dim ConTran As ConnecDBRYH = ConnecDBRYH.NewConnection
        ConTran.BeginTrans()
        Try
            Dim commandTxt1 As String
            Dim commandTxt2 As String
            Dim commandTxt3 As String
            mySqlCommand = New MySqlCommand
            mySqlCommand.Parameters.Clear()
            'If NAME <> "" And LNAME <> "" Then
            FDATE = DateTime.Now

            commandTxt1 += "`hn`,"
            commandTxt2 += "" & HN & ","
            commandTxt1 += "`cid`,"
            commandTxt2 += "'" & CID & "',"
            commandTxt1 += "`typep`,"
            commandTxt2 += "" & TYPEP & ","
            commandTxt1 += "`prename`,"
            commandTxt2 += "" & PRENAME & ","
            commandTxt1 += "`name`,"
            commandTxt2 += "'" & NAME & "',"
            commandTxt1 += "`lname`,"
            commandTxt2 += "'" & LNAME & "',"
            commandTxt1 += "`ename`,"
            commandTxt2 += "'" & ENAME & "',"
            commandTxt1 += "`emname`,"
            commandTxt2 += "'" & EMNAME & "',"
            commandTxt1 += "`elname`,"
            commandTxt2 += "'" & ELNAME & "',"
            commandTxt1 += "`penname`,"
            commandTxt2 += "" & PENNAME & ","
            commandTxt1 += "`birth`,"
            commandTxt2 += "'" & BIRTH & "',"
            commandTxt1 += "`sex`,"
            commandTxt2 += "" & SEX & ","
            commandTxt1 += "`mstatus`,"
            commandTxt2 += "" & MSTATUS & ","
            commandTxt1 += "`occupation`,"
            commandTxt2 += "" & OCCUPATION & ","
            commandTxt1 += "`race`,"
            commandTxt2 += "" & RACE & ","
            commandTxt1 += "`nation`,"
            commandTxt2 += "" & NATION & ","
            commandTxt1 += "`religion`,"
            commandTxt2 += "" & RELIGION & ","
            commandTxt1 += "`education`,"
            commandTxt2 += "" & EDUCATION & ","
            commandTxt1 += "`father`,"
            commandTxt2 += "'" & FATHER & "',"
            commandTxt1 += "`fname`,"
            commandTxt2 += "'" & FNAME & "',"
            commandTxt1 += "`mother`,"
            commandTxt2 += "'" & MOTHER & "',"
            commandTxt1 += "`mname`,"
            commandTxt2 += "'" & MNAME & "',"
            commandTxt1 += "`couple`,"
            commandTxt2 += "'" & COUPLE & "',"
            commandTxt1 += "`cname`,"
            commandTxt2 += "'" & CNAME & "',"
            commandTxt1 += "`vstatus`,"
            commandTxt2 += "" & VSTATUS & ","
            commandTxt1 += "`abogroup`,"
            commandTxt2 += "" & ABOGROUP & ","
            commandTxt1 += "`rhgroup`,"
            commandTxt2 += "" & RHGROUP & ","
            commandTxt1 += "`passport`,"
            commandTxt2 += "'" & PASSPORT & "',"
            commandTxt1 += "`email`,"
            commandTxt2 += "'" & EMAIL & "',"
            commandTxt1 += "`drugalert`,"
            commandTxt2 += "" & DRUGALERT & ","
            commandTxt1 += "`d_update`,"
            commandTxt2 += "current_timestamp(),"
            commandTxt1 += "`contact_address`,"
            commandTxt2 += "" & CONTACT_ADDRESS & ","
            commandTxt1 += "`fdate`,"
            commandTxt2 += "current_timestamp(),"
            commandTxt1 += "`usmk`,"
            commandTxt2 += "" & USMK & ","
            commandTxt1 += "`discharge`,"
            commandTxt2 += "" & DISCHARGE & ","
            commandTxt1 += "`location`,"
            commandTxt2 += "" & LOCATIONPHR & ","
            'commandTxt1 += "`f_card`,"
            'commandTxt2 += "" & F_CARD & ","
            commandTxt1 += "`penid`"
            commandTxt2 += "" & PENID & ""
            commandTxt3 = "INSERT INTO  person (" & commandTxt1 & ") VALUES (" & commandTxt2 & ");"
            ConTran.ExecuteNonQuery(commandTxt3)

            'commandTxt1 = "INSERT INTO  PERSON"
            'commandTxt1 += "(hn,cid,typep,prename,`name`,lname,ename,emname,elname,penname,birth,sex,mstatus,occupation,race,nation,religion,education,father,fname,mother,mname,couple,vstatus,abogroup,rhgroup,passport,email,drugalert,location,d_update,contact_address,fdate,usmk,discharge)"
            'commandTxt1 += " VALUES ('" & HN & "','" & CID & "','" & TYPEP & "','" & PRENAME & "','" & NAME & "','" & LNAME & "','" & ENAME & "','','" & ELNAME & "','" & PENNAME & "','" & BIRTH & "','" & SEX & "','" & MSTATUS & "','" & OCCUPATION & "','" & RACE & "','" & NATION & "','" & RELIGION & "','" & EDUCATION & "','" & FATHER & "','" & FNAME & "','" & MOTHER & "','" & MNAME & "','" & COUPLE & "','" & VSTATUS & "','" & ABOGROUP & "','" & RHGROUP & "','" & PASSPORT & "','" & EMAIL & "','" & DRUGALERT & "','" & LOCATION & "',current_timestamp(),'" & CONTACT_ADDRESS & "',current_timestamp(),'" & USMK & "','" & ENVIRONMENTCLASS.MASDISCHARGE.ไม่จำหน่าย & "',);SELECT LAST_INSERT_ID()"

            'idlast = ConTran.ExecuteScalar(commandTxt1)

            'Insert Address ADDRESSTYPE1
            Dim sql1 As String
            Dim sql2 As String
            Dim sql3 As String
            sql1 += "hn,"
            sql2 += "" & HN & ","
            sql1 += "addresstype,"
            sql2 += "1,"
            sql1 += "housetype,"
            sql2 += "'" & HOUSETYPE & "',"
            sql1 += "addresdec1,"
            sql2 += "'" & ADDRESDEC1 & "',"
            sql1 += "ampid,"
            sql2 += "" & AMPUR1 & ","
            sql1 += "postcode,"
            sql2 += "'" & POSTCODE1 & "',"
            sql1 += "country,"
            sql2 += "" & COUNTRY & ","
            sql1 += "telephone,"
            sql2 += "'" & TELEPHONE & "',"
            sql1 += "mobile,"
            sql2 += "'" & MOBILE & "',"
            sql1 += "d_update,"
            sql2 += "current_timestamp(),"
            sql1 += "status,"
            sql2 += "1,"
            sql1 += "codechangwat,"
            sql2 += "" & CHANGWAT1 & ","
            sql1 += "tmbid"
            sql2 += "" & TMB1 & ""

            sql3 = "INSERT INTO  address (" & sql1 & ") VALUES ( " & sql2 & ");"
            ConTran.ExecuteNonQuery(sql3)

            'Insert Address ADDRESSTYPE2
            sql1 = ""
            sql2 = ""
            sql3 = ""
            If ADDRESDEC2 <> "" Then
                sql1 += "hn,"
                sql2 += "" & HN & ","
                sql1 += "addresstype,"
                sql2 += "2,"
                sql1 += "housetype,"
                sql2 += "0,"
                sql1 += "addresdec1,"
                sql2 += "'" & ADDRESDEC2 & "',"
                sql1 += "ampid,"
                sql2 += "" & AMPUR2 & ","
                sql1 += "postcode,"
                sql2 += "'" & POSTCODE2 & "',"
                sql1 += "country,"
                sql2 += "" & COUNTRY2 & ","
                sql1 += "d_update,"
                sql2 += "current_timestamp(),"
                sql1 += "status,"
                sql2 += "1,"
                sql1 += "codechangwat,"
                sql2 += "" & CHANGWAT2 & ","
                sql1 += "tmbid"
                sql2 += "" & TMB2 & ""
                sql3 = "INSERT INTO address (" & sql1 & ") VALUES ( " & sql2 & ");"
                ConTran.ExecuteNonQuery(sql3)
            End If

            'INSERT Adress ADDRESSTYPE3
            sql1 = ""
            sql2 = ""
            sql3 = ""
            If ADDRESDEC3 <> "" Then
                sql1 += "hn,"
                sql2 += "" & HN & ","
                sql1 += "addresstype,"
                sql2 += "3,"
                sql1 += "housetype,"
                sql2 += "0,"
                sql1 += "addresdec1,"
                sql2 += "'" & ADDRESDEC3 & "',"
                sql1 += "ampid,"
                sql2 += "" & AMPUR3 & ","
                sql1 += "postcode,"
                sql2 += "'" & POSTCODE3 & "',"
                sql1 += "country,"
                sql2 += "" & COUNTRY3 & ","
                sql1 += "d_update,"
                sql2 += "current_timestamp(),"
                sql1 += "status,"
                sql2 += "1,"
                sql1 += "codechangwat,"
                sql2 += "'" & CHANGWAT3 & "',"
                sql1 += "telephone,"
                sql2 += "'" & TELEPHONE2 & "',"
                sql1 += "tmbid"
                sql2 += "" & TMB3 & ""
                sql3 = "INSERT INTO address (" & sql1 & ") VALUES (" & sql2 & ");"
                ConTran.ExecuteNonQuery(sql3)

            End If
            'Insert PERSONCONTACT
            sql1 = ""
            sql2 = ""
            sql3 = ""
            sql1 += "hn,"
            sql2 += "" & HN & ","
            sql1 += "`name`,"
            sql2 += "'" & NAMEPC & "',"
            sql1 += "telephone,"
            sql2 += "'" & TELEPHONEPC & "',"
            sql1 += "mobile,"
            sql2 += "'" & MOBILEPC & "',"
            sql1 += "relation,"
            sql2 += "" & RELATION & ","
            sql1 += "d_update"
            sql2 += "current_timestamp()"
            sql3 = "INSERT INTO personcontact (" & sql1 & ") VALUES ( " & sql2 & ");"
            ConTran.ExecuteNonQuery(sql3)

            'เพิ่มคู่สัญญาเริ่มต้นให้ผู้ป่วย
            sql1 = ""
            sql2 = ""
            sql3 = ""
            sql1 += "hn,"
            sql2 += "" & HN & ","
            sql1 += "mtrgtid,"
            sql2 += "1,"
            sql1 += "strgtid,"
            sql2 += "0,"
            sql1 += "sbrgtid,"
            sql2 += "0,"
            sql1 += "ptrgtid,"
            sql2 += "0,"
            sql1 += "startdate,"
            sql2 += "current_timestamp(),"
            sql1 += "expiredate,"
            sql2 += "'2099-01-01',"
            sql1 += "ae,"
            sql2 += "0,"
            sql1 += "opd,"
            sql2 += "0,"
            sql1 += "ipd,"
            sql2 += "0,"
            sql1 += "amount,"
            sql2 += "0,"
            sql1 += "d_update,"
            sql2 += "current_timestamp(),"
            sql1 += "status,"
            sql2 += "1,"
            sql1 += "conname,"
            sql2 += "'ชำระเงินสด',"
            sql1 += "`seqcon`,"
            sql2 += "0,"
            sql1 += "`insid`"
            sql2 += "''"

            sql3 = "INSERT INTO contact (" & sql1 & ") VALUES ( " & sql2 & ");"
            ConTran.ExecuteNonQuery(sql3)

            'เพิ่มคู่สัญญาเริ่มต้นให้ผู้ป่วย
            sql1 = ""
            sql2 = ""
            sql3 = ""
            sql1 += "hn,"
            sql2 += "" & HN & ","
            sql1 += "mtrgtid,"
            sql2 += "1,"
            sql1 += "strgtid,"
            sql2 += "0,"
            sql1 += "sbrgtid,"
            sql2 += "0,"
            sql1 += "ptrgtid,"
            sql2 += "0,"
            sql1 += "startdate,"
            sql2 += "current_timestamp(),"
            sql1 += "expiredate,"
            sql2 += "'2099-01-01',"
            sql1 += "ae,"
            sql2 += "0,"
            sql1 += "opd,"
            sql2 += "0,"
            sql1 += "ipd,"
            sql2 += "0,"
            sql1 += "amount,"
            sql2 += "0,"
            sql1 += "d_update,"
            sql2 += "current_timestamp(),"
            sql1 += "status,"
            sql2 += "1,"
            sql1 += "conname,"
            sql2 += "'ค้างชำระ',"
            sql1 += "`seqcon`,"
            sql2 += "0,"
            sql1 += "`insid`,"
            sql2 += "'',"
            sql1 += "`f_vw`"
            sql2 += "0"

            sql3 = "INSERT INTO contact (" & sql1 & ") VALUES ( " & sql2 & ");"
            ConTran.ExecuteNonQuery(sql3)
            ConTran.CommitTrans()
            MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return True
        Catch ex As Exception
            ConTran.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            ConTran.Dispose()
        End Try
        Return False
    End Function
    ''' <summary>
    ''' ตั้งค่า Default  ตัวแปร ต่างๆใน Person Class ใหม่
    ''' </summary>
    ''' <remarks>#reset,#clear</remarks>
    Public Sub clearVariable()
        'เคลียร์ค่าก่อนคลิก
        idlast = ""
        HOSPCODE = ""
        HN = Nothing
        CID = ""
        TYPEP = 0
        PRENAME = 0
        NAME = ""
        LNAME = ""
        ENAME = ""
        EMNAME = ""
        ELNAME = ""
        PENNAME = 0
        BIRTH = Date.Now
        SEX = 0
        MSTATUS = 0
        OCCUPATION = 0
        RACE = 0
        NATION = 0
        RELIGION = 0
        EDUCATION = 0
        FATHER = ""
        FNAME = ""
        MOTHER = ""
        MNAME = ""
        COUPLE = ""
        CNAME = ""
        VSTATUS = 0
        DISCHARGE = ENVIRONMENTCLASS.MASDISCHARGE.ไม่จำหน่าย
        DDISCHARGE = Date.Now
        ABOGROUP = 0
        RHGROUP = 0
        PASSPORT = ""
        EMAIL = ""
        FDATE = DateTime.Now
        DRUGALERT = 0
        LOCATION = 0
        D_UPDATE = ""
        ADDRESSTYPE = 0
        HOUSETYPE = 0
        ADDRESDEC1 = ""
        ADDRESDEC2 = ""
        VILLAGE1 = 0
        VILLAGE2 = 0
        VILLAGE3 = 0
        CHANGWAT1 = 0
        CHANGWAT2 = 0
        CHANGWAT3 = 0
        AMPUR1 = 0
        AMPUR2 = 0
        AMPUR3 = 0

        POSTCODE1 = ""
        COUNTRY = 0
        COUNTRY2 = 0
        COUNTRY3 = 0
        TELEPHONE = ""
        MOBILE = ""
        AgeYear = 0
        AgeMonth = 0
        AgeDay = 0
        TMB1 = 0
        TMB2 = 0
        TMB3 = 0
        NAMEPC = ""
        LNAMEPC = ""
        MOBILEPC = ""
        TELEPHONEPC = ""
        RELATION = 0
        SUBDISTRICTDESC = ""
        AMPURDESC = ""
        CHANGWATDESC = ""
        SUBDISTRICTDESC2 = ""
        AMPURDESC2 = ""
        CHANGWATDESC2 = ""
        SUBDISTRICTDESC3 = ""
        AMPURDESC3 = ""
        CHANGWATDESC3 = ""
    End Sub
    '////////////////////////  UPDATE Person  ////////////////////////
    Public Sub updatePerson()
        Dim ConTran2 As ConnecDBRYH = ConnecDBRYH.NewConnection
        ConTran2.BeginTrans()
        Try
            Dim commandTxt As String
            Dim dt As New DataTable
            mySqlCommand = New MySqlCommand
            mySqlCommand.Parameters.Clear()
            'UPDATE PERSON
            commandTxt = "UPDATE  person SET cid = '" & CID & "', typep = '" & TYPEP & "',prename = '" & PRENAME & "', `name` = '" & NAME & "',lname = '" & LNAME & "', "
            commandTxt += " ename = '" & ENAME & "', emname = '" & EMNAME & "', elname = '" & ELNAME & "', penname = '" & PENNAME & "', birth = '" & BIRTH & "',sex = '" & SEX & "', "
            commandTxt += " mstatus = '" & MSTATUS & "', occupation= '" & OCCUPATION & "',race = '" & RACE & "',nation = '" & NATION & "',religion = '" & RELIGION & "' ,"
            commandTxt += " education = '" & EDUCATION & "', father = '" & FATHER & "',fname = '" & FNAME & "',mother = '" & MOTHER & "', mname = '" & MNAME & "', "
            commandTxt += " couple = '" & COUPLE & "', vstatus = '" & VSTATUS & "',abogroup = '" & ABOGROUP & "',rhgroup = '" & RHGROUP & "',passport = '" & PASSPORT & "', "
            commandTxt += " email = '" & EMAIL & "',location = '" & LOCATIONPHR & "',contact_address = '" & CONTACT_ADDRESS & "',drugalert = '" & DRUGALERT & "',usmk = '" & USMK & "',"
            commandTxt += " `d_update` = current_timestamp(),`penid` = " & PENID & " WHERE hn = '" & HN & "';"
            mySqlCommand.CommandText = commandTxt
            idlast = ConTran2.ExecuteScalar_Parameter(mySqlCommand)
            mySqlCommand.Parameters.Clear()
            commandTxt = "SELECT * FROM  personcontact WHERE hn='" & HN & "';"
            Dim dtmem As DataTable = ConTran2.GetTable(commandTxt)
            If dtmem.Rows.Count > 0 Then
                commandTxt = "UPDATE  personcontact SET `name` = '" & NAMEPC & "',telephone = '" & TELEPHONEPC & "',mobile = '" & MOBILEPC & "', relation = '" & RELATION & "', d_update = current_timestamp() WHERE hn = '" & HN & "';"
            Else
                commandTxt = "INSERT INTO  personcontact(hn,name,telephone,mobile,relation,d_update) VALUES "
                commandTxt += "('" & HN & "','" & NAMEPC & "','" & TELEPHONEPC & "','" & MOBILEPC & "'," & RELATION & ",'" & Date.Now.ToString("yyyy-MM-dd hh:mm:ss") & "');"
            End If
            mySqlCommand.CommandText = commandTxt
            idlast = ConTran2.ExecuteScalar_Parameter(mySqlCommand)
            ConTran2.CommitTrans()
            MessageBox.Show("แก้ไขข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            ConTran2.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            ConTran2.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' ข้อความแจ้งเตือน
    ''' </summary>
    ''' <remarks>#แจ้งเตือนข้อความ,#ข้อความแจ้งเตือน</remarks>
    'Public Sub saveTitle(f As PER003)
    '    Dim ConTran As ConnecDBRYH = ConnecDBRYH.NewConnection
    '    ConTran.BeginTrans()
    '    Try
    '        Dim sql As String = ""
    '        sql += "INSERT INTO  pnalert"
    '        sql += "( hn,color,title,altdate,comment,usmk_alt) "
    '        sql += "VALUES ('" + f.txtHn.Text.Trim.Replace("-", "") + "','" + f.ColorPickEdit1.Color.R.ToString + "," + f.ColorPickEdit1.Color.G.ToString + "," + f.ColorPickEdit1.Color.B.ToString + "','" + f.txtTitle.Text + "',current_timestamp(),'" + f.txtComment.Text + "','" & f.txtNameUserReq.Tag & "');"
    '        For i As Integer = 0 To f.dgvClinic.Rows.Count - 1
    '            If Convert.ToBoolean(f.dgvClinic.Rows(i).Cells("chkClinic").Value) = True Then
    '                sql += "INSERT INTO pnalertclinic"
    '                sql += "(paltid,clinicid)"
    '                sql += "VALUES ((SELECT paltid FROM pnalert ORDER BY paltid DESC LIMIT 1),'" + f.dgvClinic.Rows(i).Cells("CLID").Value.ToString.Trim + "');"
    '            End If
    '        Next i
    '        ConTran.ExecuteNonQuery(sql)
    '        ConTran.CommitTrans()
    '        MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    Catch ex As Exception
    '        ConTran.RollbackTrans()
    '        MsgBox(ex.ToString)
    '    Finally
    '        ConTran.Dispose()
    '    End Try

    'End Sub
    ''' <summary>
    ''' ข้อความแจ้งเตือนที่สามารถบอกได้ว่าที่ แผนกไหน โดยการเพืมข้อมูลผ่าน function นี้
    ''' </summary>
    ''' <remarks>#แจ้งเตือนแผนก</remarks>
    'Public Sub saveClinic(f As PER003, ByVal PALTID As String)
    '    Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
    '    Dim sql As String
    '    Dim dt As New DataTable
    '    Dim check As Boolean = True
    '    sql = "SELECT  `paltc`,`paltid`,`clinicid` FROM  pnalertclinic WHERE  `paltid` = '" & PALTID & "'  "
    '    dt = db.GetTable(sql)
    '    For i As Integer = 0 To f.dgvClinic.Rows.Count - 1
    '        For j As Integer = 0 To dt.Rows.Count - 1
    '            If dt.Rows(j)("clinicid").ToString = f.dgvClinic.Rows(i).Cells("CLID").Value.ToString Then
    '                If Convert.ToBoolean(f.dgvClinic.Rows(i).Cells("chkClinic").Value) = False Then
    '                    db = ConnecDBRYH.NewConnection
    '                    sql = "DELETE FROM  pnalertclinic WHERE `paltc` = '" & dt.Rows(j)("paltc") & "' ;"
    '                    db.ExecuteNonQuery(sql)
    '                    db.Dispose()

    '                End If
    '                check = False
    '                Exit For
    '            Else
    '                check = True
    '            End If
    '        Next
    '        If check = True Then
    '            sql = ""
    '            If Convert.ToBoolean(f.dgvClinic.Rows(i).Cells("chkClinic").Value) = True Then
    '                db = ConnecDBRYH.NewConnection
    '                sql += "INSERT INTO  pnalertclinic"
    '                sql += "(paltid,clinicid)"
    '                sql += "VALUES ('" & PALTID & "','" & f.dgvClinic.Rows(i).Cells("CLID").Value.ToString & "');"
    '                db.ExecuteNonQuery(sql)
    '                db.Dispose()
    '            End If
    '        End If
    '    Next i

    'End Sub
    ''เช็คซ้ำ HN
    Public ReadOnly Property CHK_DUPLICATE As Duplicate
        Get
            Return HN_Duplicate
        End Get
    End Property
    ''' <summary>
    ''' ดึงข้อมูลผู้ป่วย ในหน้า form ต่างๆๆที่แสดง
    ''' </summary>
    ''' <remarks>ในหน้าต่างๆแสดงที่อยู่ต่างๆ #ข้อมูลผู้ป่วย,#ข้อมูลคนไข้,#person</remarks>

    Public Function getDataPersonTable(hnx As String) As DataTable

        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        Dim sql As String = "SELECT DISTINCT CAST(p.hn AS CHAR(15)) AS HN,p.cid,p.typep,p.prename,p.`name`,p.lname,p.ename,p.emname,p.elname,p.penname"
        sql += ",mpre.stprename,mpre.seprename,mpre.ftprename,mpre.feprename,p.sex,msex.sexdesc"
        sql += ",p.birth,p.race,mra.nationdesc AS RACEDESC,p.nation,ad.telephone,ad.mobile,p.cid "
        sql += ",mna.nationdesc,p.religion,mre.religiondesc"
        sql += ",CASE WHEN(p.drugalert = 1) THEN 'แพ้ยา' ELSE 'ไม่แพ้ยา' END AS drugalert, p.drugalert as DRUGALERTSTAT"
        sql += ",mabo.abogroupdesc,mrh.rhgroupdesc"
        'บุคคลอ้างอิง
        sql += ",pc.`name` AS NAMEPC,pc.telephone AS TELEPHONEPC,pc.mobile AS MOBILEPC,pc.relation,mr.reltdesc "
        sql += ",CASE WHEN(date_format(p.fdate,'%Y-%m-%d') = curdate()) THEN 1 ELSE 0 END AS IsNewPatient  , penname.penid , penname.penname as 'Pname' ,penname.penlname , penname.penname_en ,penname.penlname_en " 'สถานะคนใข้ใหม่
        sql += "FROM (SELECT * FROM  person WHERE(hn = " & hnx & " AND `status` = 1)) AS p "
        sql += "LEFT JOIN (SELECT * FROM  masprename WHERE(`status` = 1)) AS mpre ON p.prename = mpre.prename "
        sql += "LEFT JOIN (SELECT * FROM  massex WHERE(`status` = 1)) AS msex ON p.sex = msex.sex "
        sql += "LEFT JOIN (SELECT * FROM  masnation WHERE(`status` = 1)) AS mra ON p.nation = mra.nation "
        sql += "LEFT JOIN (SELECT * FROM  masnation WHERE(`status` = 1)) AS mna ON p.nation = mna.nation "
        sql += "LEFT JOIN (SELECT * FROM  masreligion WHERE(`status` = 1)) AS mre ON p.religion = mre.religion "
        sql += "LEFT JOIN (SELECT * FROM  masabogroup WHERE(`status` = 1)) AS mabo ON p.abogroup = mabo.abogroup "
        sql += "LEFT JOIN (SELECT * FROM  masrhgroup WHERE(`status` = 1)) AS mrh ON p.rhgroup = mrh.rhgroup "

        sql += "LEFT JOIN (SELECT * FROM  address WHERE(mobile IS NOT NULL AND mobile <> '') and hn =" & hnx & " ) AS ad ON p.hn = ad.hn "
        'บุคคลอ้างอิง
        sql += "LEFT JOIN (SELECT * FROM  personcontact WHERE hn = " & hnx & ") AS pc ON p.hn = pc.hn "
        sql += "LEFT JOIN (SELECT * FROM  masrelation WHERE(`status` = 1)) AS mr ON pc.relation = mr.relation "
        sql += " LEFT JOIN (SELECT * FROM  maspenname WHERE `status` = 1) AS penname ON p.penid = penname.penid"

        If hnx.Trim <> "" Then
            dt = db.GetTable(sql)
        End If
        db.Dispose()

        Return dt
    End Function
    ''' <summary>
    ''' ดึงข้อมูล ผู้ป่วย person
    ''' </summary>
    ''' <remarks>มี stored procedure  ดึงข้อมูลผู้ป่วย ใส่ ในตัวแปร ต่างๆ ใน person class</remarks>
    Public Sub getDataPerson(ByVal hnx As String)
        'ดึงข้อมูล Person
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        dt = db.GetTable("CALL  STP_GETPERSON_BYHN('" + hnx + "')") 'เรียก store procedure
        ' ใส่ข้อมูลลงตัวแปร
        If dt.Rows.Count > 0 Then
            'HOSPCODE = dt.Rows(0)("").ToString()
            HN = Convert.ToUInt64(dt.Rows(0)("hn"))
            If dt.Rows(0)("cid").ToString() Is Nothing Then
                CID = ""
            Else

                CID = dt.Rows(0)("cid").ToString()
            End If

            TYPEP = CheckNullInt(dt.Rows(0)("typep"))
            PRENAME = CheckNullInt(dt.Rows(0)("prename"))
            STPRENAME = dt.Rows(0)("stprename").ToString()
            SEPRENAME = dt.Rows(0)("seprename").ToString()
            NAME = dt.Rows(0)("name").ToString()
            LNAME = dt.Rows(0)("lname").ToString()
            If dt.Rows(0)("cid").ToString() Is Nothing Then
                ENAME = ""
            Else
                ENAME = dt.Rows(0)("ename").ToString()
            End If

            EMNAME = dt.Rows(0)("emname").ToString()
            ELNAME = dt.Rows(0)("elname").ToString()
            PENNAME = CheckNullInt(dt.Rows(0)("penname"))
            BIRTH = dt.Rows(0)("birth").ToString()
            BIRTH2 = Convert.ToDateTime(dt.Rows(0)("birth"))
            SEX = CheckNullInt(dt.Rows(0)("sex"))
            MSTATUS = CheckNullInt(dt.Rows(0)("mstatus"))       'สถานะการแต่งงาน
            MSTATUSDESC = dt.Rows(0)("mstatusdesc").ToString()
            OCCUPATION = CheckNullInt(dt.Rows(0)("occupation")) 'อาชีพ
            OCCUPATIONDESC = dt.Rows(0)("occupationdesc").ToString()
            RACE = CheckNullInt(dt.Rows(0)("race"))             'เชื้อชาติ
            RACEDESC = dt.Rows(0)("racedesc").ToString()
            NATION = CheckNullInt(dt.Rows(0)("nation"))         'สัญชาติ
            NATIONDESC = dt.Rows(0)("nationdesc").ToString()
            RELIGION = CheckNullInt(dt.Rows(0)("religion"))     'ศาสนา
            RELIGIONDESC = dt.Rows(0)("RELIGIONDESC").ToString()
            EDUCATION = CheckNullInt(dt.Rows(0)("education"))   'การศึกษา
            EDUCATEDESC = dt.Rows(0)("educationdesc").ToString()
            FATHER = dt.Rows(0)("father").ToString()
            FNAME = dt.Rows(0)("fname").ToString()
            MOTHER = dt.Rows(0)("mother").ToString()
            MNAME = dt.Rows(0)("mname").ToString()
            COUPLE = dt.Rows(0)("couple").ToString()
            If dt.Rows(0)("vstatus") Is DBNull.Value Then       'สถานะ vip
                VSTATUS = 2
                VSTATUSDESC = dt.Rows(0)("vstatusdesc").ToString()
            Else
                VSTATUS = Convert.ToUInt32(dt.Rows(0)("vstatus"))
                VSTATUSDESC = dt.Rows(0)("vstatusdesc").ToString()
            End If
            DISCHARGE = CheckNullInt(dt.Rows(0)("discharge"))
            DDISCHARGE = checkNullDate(dt.Rows(0)("daydischarge"))
            ABOGROUP = CheckNullInt(dt.Rows(0)("abogroup"))     'กรุ๊ปเลือด
            ABOGROUPDESC = dt.Rows(0)("abogroupdesc").ToString()
            RHGROUP = CheckNullInt(dt.Rows(0)("rhgroup"))       'หมู่เลือด rhgroup
            RHGROUPDESC = dt.Rows(0)("rhgroupdesc").ToString()
            PASSPORT = dt.Rows(0)("passport").ToString()
            EMAIL = dt.Rows(0)("email").ToString()
            FDATE = checkNullDate(dt.Rows(0)("fdate"))
            DRUGALERT = CheckNullInt(dt.Rows(0)("drugalert"))
            LOCATION = CheckNullInt(dt.Rows(0)("location"))
            D_UPDATE = dt.Rows(0)("d_update").ToString()
            If dt.Rows(0)("contact_address") Is DBNull.Value Then
                CONTACT_ADDRESS = 2
            Else
                CONTACT_ADDRESS = Convert.ToUInt32(dt.Rows(0)("contact_address"))
            End If
            CONTACT_ADDRESS = CheckNullInt(dt.Rows(0)("contact_address"))
            ' ข้อมูลการ Contact
            TELEPHONE = dt.Rows(0)("telephone").ToString()
            MOBILE = dt.Rows(0)("mobile").ToString()
            ADDRESSTYPE = CheckNullInt(dt.Rows(0)("addresstype")) ' ประเภทของที่อยู่ ดึงมาจาก MASADDRESSTYPE
            HOUSETYPE = CheckNullInt(dt.Rows(0)("housetype")) ' ลักษณะที่อยู่ ดึงมาจาก MASHOUSETYPE
            ADDRESDEC1 = dt.Rows(0)("addresdec1").ToString() ' ที่อยู่ 1
            If dt.Rows.Count > 1 Then ADDRESDEC2 = dt.Rows(1)("addresdec1").ToString() Else ADDRESDEC2 = Nothing ' ที่อยุ่ 2
            If dt.Rows.Count > 2 Then ADDRESDEC3 = dt.Rows(2)("addresdec1").ToString() Else ADDRESDEC3 = Nothing ' ที่อยู่ 3
            VILLAGE1 = dt.Rows(0)("village").ToString() ' ตำบล 1
            SUBDISTRICTDESC = dt.Rows(0)("tambondesc").ToString
            AMPURDESC = dt.Rows(0)("ampurdesc").ToString
            CHANGWATDESC = dt.Rows(0)("changwatdesc").ToString
            If dt.Rows.Count > 1 Then
                VILLAGE2 = dt.Rows(1)("village").ToString()
                SUBDISTRICTDESC2 = dt.Rows(1)("tambondesc").ToString
                AMPURDESC2 = dt.Rows(1)("ampurdesc").ToString
                CHANGWATDESC2 = dt.Rows(1)("changwatdesc").ToString
            Else
                VILLAGE2 = Nothing ' ตำบล 2
                SUBDISTRICTDESC2 = Nothing
                AMPURDESC2 = Nothing
                CHANGWATDESC2 = Nothing
            End If
            If dt.Rows.Count > 2 Then
                VILLAGE3 = dt.Rows(2)("village").ToString()
                SUBDISTRICTDESC3 = dt.Rows(2)("tambondesc").ToString
                AMPURDESC3 = dt.Rows(2)("ampurdesc").ToString
                CHANGWATDESC3 = dt.Rows(2)("changwatdesc").ToString
            Else
                VILLAGE3 = Nothing ' ตำบล 3
                SUBDISTRICTDESC3 = Nothing
                AMPURDESC3 = Nothing
                CHANGWATDESC3 = Nothing
            End If
            POSTCODE1 = dt.Rows(0)("postcode").ToString()  ' รหัสไปรษณีย์1
            If dt.Rows.Count > 1 Then POSTCODE2 = dt.Rows(1)("postcode").ToString() Else POSTCODE2 = Nothing ' รหัสไปรษณีย์2
            If dt.Rows.Count > 2 Then POSTCODE3 = dt.Rows(2)("postcode").ToString() Else POSTCODE3 = Nothing ' รหัสไปรษณีย์3
            If dt.Rows(0)("country") Is DBNull.Value Then
                COUNTRY = Nothing
            Else
                COUNTRY = Convert.ToUInt32(dt.Rows(0)("country")) ' ประเทศ ดึงมาจาก MASCOUNTRY
                COUNTRYDESC = dt.Rows(0)("countrydesc").ToString
            End If
            For i As Integer = 0 To dt.Rows.Count - 1
                If CheckNullInt(dt.Rows(i)("addresstype")) = 1 Then
                    TELEPHONE = dt.Rows(i)("telephone").ToString ' เบอร์โทรสัพท์บ้าน
                    MOBILE = dt.Rows(i)("mobile").ToString  ' เบอร์โทรศัพท์มือถือ
                ElseIf CheckNullInt(dt.Rows(i)("addresstype")) = 3 Then
                    TELEPHONE2 = dt.Rows(i)("telephone").ToString ' เบอร์โทรสัพท์ที่ทำงาน
                End If
            Next i
            ADDRESSID = CheckNullInt(dt.Rows(0)("addressid"))
            'Personal Ref
            NAMEPC = dt.Rows(0)("namepc").ToString()    'ผู้ติดต่อฉุกเฉิน
            TELEPHONEPC = dt.Rows(0)("telephonepc").ToString()      'โทรศัพท์ติดต่อฉุกเฉิน
            MOBILEPC = dt.Rows(0)("mobilepc").ToString()
            RELATIONNAME = dt.Rows(0)("reltdesc").ToString()        'ความสัมพันธ์
            RELATION = CheckNullInt(dt.Rows(0)("relation"))
            'ISNEWPATIENT = Convert.ToBoolean(dt.Rows(0)("IsNewPatient"))
            ISPENNAME = Convert.ToBoolean(dt.Rows(0)("penname"))

            ' Dim fd As New FriendsDate()
            'fd.Date_Diff(BIRTH2, Convert.ToDateTime(db.GetTable("SELECT curdate()").Rows(0)(0)))
            'AgeYear = fd._year
            'AgeMonth = fd._month
            'AgeDay = fd._day
        Else
            Call clearVariable()
        End If
        db.Dispose()
    End Sub

    ''' <summary>
    ''' Check ว่าตัวเลขเป็นว่างหรือไม่
    ''' </summary>
    Public Function CheckNullInt(o As Object) As Integer
        Try
            Return Convert.ToInt32(o)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Check วันที่เป็นว่างหรือไม่
    ''' </summary>
    Public Function checkNullDate(o As Object) As Date
        Try
            Return Convert.ToDateTime(o)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' เช็คข้อมูลว่า nothing หรือไม่
    ''' </summary>
    ''' <remarks>#nothing ,#datanothing</remarks>
    Private Sub getDataPCToTable(dt2 As DataTable)
        If dt2.Rows.Count > 0 Then
            NAMEPC = If(dt2.Rows(0)("name").ToString Is Nothing, "", dt2.Rows(0)("name").ToString)
            TELEPHONEPC = If(dt2.Rows(0)("telephone").ToString Is Nothing, "", dt2.Rows(0)("telephone").ToString)
            MOBILEPC = If(dt2.Rows(0)("mobile").ToString Is Nothing, "", dt2.Rows(0)("mobile").ToString)
            RELATION = Convert.ToInt32(If(dt2.Rows(0)("relation") Is Nothing, 0, dt2.Rows(0)("relation")))
        End If

    End Sub
    ''' <summary>
    ''' จัดการที่อยู่เพื่อเก็บข้อมูลลงตัวแปรใน person class
    ''' </summary>
    ''' <remarks>
    ''' function จะวนลูป เพื่อเช็ค addresstype ว่าควรจะเก็บไว้ในตัวแปรไหน
    '''
    ''' #จัดการข้อมูลที่อยุ่,#ที่อยู่,#address
    ''' </remarks>
    Private Sub getDataAddressTotalbe(addtype As DataTable)
        For i As Integer = 0 To addtype.Rows.Count - 1
            If addtype.Rows(i)("addresstype").ToString.Trim = "1" Then
                ADDRESDEC1 = addtype.Rows(i)("addresdec1").ToString
                POSTCODE1 = addtype.Rows(i)("postcode").ToString
                TELEPHONE = addtype.Rows(i)("telephone").ToString
                If addtype.Rows(i)("country").ToString.Length = 0 Then
                    COUNTRY = "0"
                Else
                    COUNTRY = addtype.Rows(i)("country").ToString
                End If
                If addtype.Rows(i)("ampid").ToString.Length = 0 Then
                    AMPUR1 = "0"
                Else
                    AMPUR1 = addtype.Rows(i)("ampid").ToString
                End If
                If addtype.Rows(i)("codechangwat").ToString.Length = 0 Then
                    CHANGWAT1 = "0"
                Else
                    CHANGWAT1 = addtype.Rows(i)("codechangwat").ToString
                End If
                'If addtype.Rows(i)("village").ToString.Length = 0 Then
                '    VILLAGE1 = "0"
                'Else
                '    VILLAGE1 = addtype.Rows(i)("village").ToString
                'End If
                If addtype.Rows(i)("tmbid").ToString.Length = 0 Then
                    TMB1 = "0"
                Else
                    TMB1 = addtype.Rows(i)("tmbid").ToString
                End If
                MOBILE = addtype.Rows(i)("mobile").ToString
                If addtype.Rows(i)("housetype").ToString.Length = 0 Then
                    HOUSETYPE = "0"
                Else
                    HOUSETYPE = addtype.Rows(i)("housetype").ToString
                End If
            ElseIf addtype.Rows(i)("addresstype").ToString.Trim = "2" Then
                ADDRESDEC2 = If(addtype.Rows(i)("addresdec1") Is Nothing, "", addtype.Rows(i)("addresdec1").ToString)
                POSTCODE2 = If(addtype.Rows(i)("postcode") Is Nothing, "", addtype.Rows(i)("postcode").ToString)
                If addtype.Rows(i)("ampid").ToString.Length = 0 Then
                    AMPUR2 = "0"
                Else
                    AMPUR2 = addtype.Rows(i)("ampid").ToString
                End If
                If addtype.Rows(i)("codechangwat").ToString.Length = 0 Then
                    CHANGWAT2 = "0"
                Else
                    CHANGWAT2 = addtype.Rows(i)("codechangwat").ToString
                End If
                'If addtype.Rows(i)("village").ToString.Length = 0 Then
                '    VILLAGE2 = "0"
                'Else
                '    VILLAGE2 = addtype.Rows(i)("village").ToString
                'End If
                If addtype.Rows(i)("country").ToString.Length = 0 Then
                    COUNTRY2 = "0"
                Else
                    COUNTRY2 = addtype.Rows(i)("country").ToString
                End If
                If addtype.Rows(i)("tmbid").ToString.Length = 0 Then
                    TMB2 = "0"
                Else
                    TMB2 = addtype.Rows(i)("tmbid").ToString
                End If
            ElseIf addtype.Rows(i)("addresstype").ToString.Trim = "3" Then
                ADDRESDEC3 = If(addtype.Rows(i)("addresdec1") Is Nothing, "", addtype.Rows(i)("addresdec1").ToString)
                POSTCODE3 = If(addtype.Rows(i)("postcode") Is Nothing, "", addtype.Rows(i)("postcode").ToString)
                TELEPHONE2 = If(addtype.Rows(i)("telephone") Is Nothing, "", addtype.Rows(i)("telephone").ToString)
                If addtype.Rows(i)("ampid").ToString.Length = 0 Then
                    AMPUR3 = "0"
                Else
                    AMPUR3 = addtype.Rows(i)("ampid").ToString
                End If
                If addtype.Rows(i)("codechangwat").ToString.Length = 0 Then
                    CHANGWAT3 = "0"
                Else
                    CHANGWAT3 = addtype.Rows(i)("codechangwat").ToString
                End If
                If addtype.Rows(i)("country").ToString.Length = 0 Then
                    COUNTRY3 = "0"
                Else
                    COUNTRY3 = addtype.Rows(i)("country").ToString
                End If
                'If addtype.Rows(i)("village").ToString.Length = 0 Then
                '    VILLAGE3 = "0"
                'Else
                '    VILLAGE3 = addtype.Rows(i)("village").ToString
                'End If
                If addtype.Rows(i)("tmbid").ToString.Length = 0 Then
                    TMB3 = "0"
                Else
                    TMB3 = addtype.Rows(i)("tmbid").ToString
                End If
            End If
        Next

    End Sub
    ''' <summary>
    ''' นำข้อมูลจาก Datatable ลงในตัวแปร Person Class
    ''' </summary>
    Private Sub getDataPesonToVariable(dt As DataTable)
        If dt.Rows(0)("cid").ToString.Length = 0 Then
        Else
            CID = dt.Rows(0)("cid").ToString
        End If
        If dt.Rows(0)("name").ToString.Length = 0 Then
        Else
            NAME = dt.Rows(0)("name").ToString
        End If
        If dt.Rows(0)("lname").ToString.Length = 0 Then
        Else
            LNAME = dt.Rows(0)("lname").ToString
        End If
        If dt.Rows(0)("ename").ToString.Length = 0 Then
        Else
            ENAME = dt.Rows(0)("ename").ToString
        End If
        If dt.Rows(0)("emname").ToString.Length = 0 Then
        Else
            EMNAME = dt.Rows(0)("emname").ToString
        End If
        If dt.Rows(0)("elname").ToString.Length = 0 Then
        Else
            ELNAME = dt.Rows(0)("elname").ToString
        End If
        If dt.Rows(0)("hn").ToString.Length = 0 Then
        Else
            HN = Convert.ToUInt64(dt.Rows(0)("hn"))
        End If
        If dt.Rows(0)("birth").ToString.Length = 0 Then
        Else
            BIRTH = Convert.ToDateTime(dt.Rows(0)("birth"))
        End If
        If dt.Rows(0)("sex").ToString.Length = 0 Then
        Else
            SEX = Convert.ToInt32(dt.Rows(0)("sex"))
        End If
        If dt.Rows(0)("race").ToString.Length = 0 Then
        Else
            RACE = Convert.ToInt32(dt.Rows(0)("race"))
        End If
        If dt.Rows(0)("nation").ToString.Length = 0 Then
        Else
            NATION = Convert.ToInt32(dt.Rows(0)("nation"))
        End If
        If dt.Rows(0)("religion").ToString.Length = 0 Then
        Else
            RELIGION = Convert.ToInt32(dt.Rows(0)("religion"))
        End If
        If dt.Rows(0)("education").ToString.Length = 0 Then
        Else
            EDUCATION = Convert.ToInt32(dt.Rows(0)("education"))
        End If
        If dt.Rows(0)("occupation").ToString.Length = 0 Then
        Else
            OCCUPATION = Convert.ToInt32(dt.Rows(0)("occupation"))
        End If

        If dt.Rows(0)("abogroup").ToString.Length = 0 Then
        Else
            ABOGROUP = Convert.ToInt32(dt.Rows(0)("abogroup"))
        End If

        If dt.Rows(0)("rhgroup").ToString.Length = 0 Then
        Else
            RHGROUP = Convert.ToInt32(dt.Rows(0)("rhgroup"))
        End If
        If dt.Rows(0)("passport").ToString.Length = 0 Then
        Else
            PASSPORT = Convert.ToString(dt.Rows(0)("passport"))
        End If
        If dt.Rows(0)("typep").ToString.Length = 0 Then
        Else
            TYPEP = Convert.ToInt32(dt.Rows(0)("typep"))
        End If

        If dt.Rows(0)("prename").ToString.Length = 0 Then
        Else
            PRENAME = Convert.ToInt32(dt.Rows(0)("prename"))
        End If
        If dt.Rows(0)("mstatus").ToString.Length = 0 Then
        Else
            MSTATUS = Convert.ToInt32(dt.Rows(0)("mstatus"))
        End If
        If dt.Rows(0)("father").ToString.Length = 0 Then
        Else
            FATHER = dt.Rows(0)("father").ToString
        End If
        If dt.Rows(0)("fname").ToString.Length = 0 Then
        Else
            FNAME = dt.Rows(0)("fname").ToString
        End If
        If dt.Rows(0)("mother").ToString.Length = 0 Then
        Else
            MOTHER = dt.Rows(0)("mother").ToString
        End If
        If dt.Rows(0)("mname").ToString.Length = 0 Then
        Else
            MNAME = dt.Rows(0)("mname").ToString
        End If
        If dt.Rows(0)("email").ToString.Length = 0 Then
        Else
            EMAIL = dt.Rows(0)("email").ToString
        End If

        If dt.Rows(0)("penname").ToString.Length = 0 Then
        Else
            PENNAME = If(dt.Rows(0)("penname").ToString = "True", 1, 0)
        End If
        If dt.Rows(0)("vstatus").ToString.Length = 0 Then
        Else
            VSTATUS = Convert.ToInt32(dt.Rows(0)("vstatus"))
        End If

        If dt.Rows(0)("drugalert").ToString.Length = 0 Then
        Else
            DRUGALERT = If(dt.Rows(0)("drugalert").ToString = "True", 1, 0)
        End If

        If dt.Rows(0)("f_card").ToString.Length = 0 Then
        Else
            F_CARD = If(dt.Rows(0)("f_card").ToString = "True", 1, 0)
        End If

        'If dt.Rows(0)("IsNewPatient").ToString.Length = 0 Then
        'Else
        '    ISNEWPATIENT = Convert.ToBoolean(dt.Rows(0)("IsNewPatient"))
        'End If
        If dt.Rows(0)("contact_address").ToString.Length = 0 Then
        Else
            CONTACT_ADDRESS = Convert.ToInt32(dt.Rows(0)("contact_address"))
        End If

        If dt.Rows(0)("discharge").ToString.Length = 0 Then
        Else
            DISCHARGE = Convert.ToInt32(dt.Rows(0)("discharge"))
        End If
        If dt.Rows(0)("ddischarge").ToString.Length = 0 Then
        Else
            DDISCHARGE = Convert.ToDateTime(dt.Rows(0)("ddischarge"))
        End If
        If dt.Rows(0)("contact_address").ToString.Length = 0 Then
        Else
            LOCATIONPHR = Convert.ToInt32(dt.Rows(0)("contact_address"))
        End If
        If dt.Rows(0)("penid").ToString.Length = 0 Then
        Else
            PENID = Convert.ToInt32(dt.Rows(0)("penid"))
        End If
        If dt.Rows(0)("pc").ToString.Length = 0 Then
        Else
            PC = Convert.ToInt32(dt.Rows(0)("pc"))
        End If
    End Sub
    ''' <summary>
    ''' ระบบค้นหาข้อมูลผู้ป่วย
    ''' </summary>
    ''' <remarks>
    ''' #ค้นหา
    '''
    ''' หลักการทำงานจะมีการเช็คว่ามีเลขบัตรประชาชนอันนี้อยู่อีกหรือไม่ถ้ามีจะแสดงข้อมูลคนไข้คนนั้นทันที
    ''' </remarks>
    Public Sub searchPersonClass(ByVal hnx As UInt64)
        Dim ConTran As ConnecDBRYH = ConnecDBRYH.NewConnection
        ConTran.BeginTrans()
        Try
            Dim commandSql As String
            Dim dt As New DataTable
            commandSql = getCmdSelectPerson
            'commandSql += ",CASE WHEN(date_format(FDATE,'%Y-%m-%d') = curdate()) THEN 1 ELSE 0 END AS IsNewPatient " 'สถานะคนใข้ใหม่
            commandSql += " FROM person WHERE hn ='" & hnx & "';"
            dt = ConTran.GetTable(commandSql)
            If dt.Rows.Count > 0 Then
                getDataPesonToVariable(dt)
            End If
            'Select ADDRESS
            Dim commandSql1 As String
            Dim addtype As New DataTable
            commandSql1 = getCmdSelectAddress
            commandSql1 += " FROM address WHERE hn ='" & hnx & "' and `status` = 1;"
            addtype = ConTran.GetTable(commandSql1)
            getDataAddressTotalbe(addtype)
            Dim commandSql2 As String
            Dim dt2 As New DataTable
            commandSql2 = "SELECT name,telephone,mobile,relation"
            commandSql2 += " FROM personcontact WHERE hn ='" & hnx & "';"
            dt2 = ConTran.GetTable(commandSql2)
            getDataPCToTable(dt2)
            ConTran.CommitTrans()
        Catch ex As Exception
            ConTran.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            ConTran.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' ใช้สำหรับเช็คว่า ตาย แล้ว หรือไม่
    ''' </summary>
    ''' <remarks>มันจะเช็คจาก TABLE masdischage  แล้ว return กลับมาเป็น String</remarks>
    ''' <param name="id">ส่ง สถานะ/สาเหตุการจำหน่าย   person.DISCHARGE  PK-&gt;Discharge</param>
    ''' <returns>
    ''' masdischarge
    ''' '1', 'ตาย', '1'
    ''' '2', 'ย้าย', '1'
    ''' '3', 'สาบสูญ', '1'
    ''' '4', 'กลับบ้าน', '1'
    ''' '9', 'ไม่จำหน่าย', '1'
    ''' </returns>
    Public Function checkDischarge(ByVal id As Integer) As String
        Dim contran As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim commandSql As String
        Dim dt As New DataTable

        commandSql = getCmdSelectDischarge
        commandSql += " FROM masdischarge WHERE dischgid ='" & id & "'"
        dt = contran.GetTable(commandSql)
        contran.Dispose()
        If dt.Rows.Count = 0 Then
            Return 9
        Else
            Return dt.Rows(0)("dischgid").ToString
        End If

    End Function

    ''' <summary>
    ''' ระบบค้นหาผ่านเลขบัตรประชาชน ในหน้า เวชระเบียน
    ''' </summary>
    ''' <remarks>
    ''' #ค้นหา
    '''
    ''' หลักการทำงานจะมีการเช็คว่ามีเลขบัตรประชาชนอันนี้อยู่อีกหรือไม่ถ้ามีจะแสดงข้อมูลคนไข้คนนั้นทันที
    ''' </remarks>
    Public Sub searchCidPerson(ByVal cidx As String, ByVal hnx As String, ByVal ShowMsg As Boolean, typesearch As String)
        Dim ConTran As ConnecDBRYH = ConnecDBRYH.NewConnection
        ConTran.BeginTrans()
        Dim commandSql As String
        Dim dt As New DataTable
        commandSql = getCmdSelectPerson
        'commandSql += ",CASE WHEN(date_format(FDATE,'%Y-%m-%d') = curdate()) THEN 1 ELSE 0 END AS IsNewPatient " 'สถานะคนใข้ใหม่
        If typesearch = "HN" Then
            commandSql += " FROM person WHERE hn = " & hnx & " ;"
        ElseIf typesearch = "CID" Then '
            commandSql += " FROM person WHERE cid = " & cidx.Replace("-", "") & " ;"
        End If
        dt = ConTran.GetTable(commandSql)
        Dim ms As System.Windows.Forms.DialogResult
        Try
            'If IsDBNull(mySqlReader("CID")) = False And mySqlReader("CID") <> "0" Then
            '    ms = MessageBox.Show("มีข้อมูลผู้ป่วยรายนี้อยู่ในระบบ ต้องการเรียกใช้หรือไม่ !!", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
            '
            If dt.Rows.Count > 0 And ShowMsg = True Then
                ms = MessageBox.Show("มีข้อมูลผู้ป่วยรายนี้อยู่ในระบบ ต้องการเรียกใช้หรือไม่ !!", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
            ElseIf dt.Rows.Count > 0 And ShowMsg = False Then
                ms = DialogResult.OK
            End If
            If ms = DialogResult.OK Then
                getDataPesonToVariable(dt)
                Dim commandSql1 As String
                Dim addtype As New DataTable
                commandSql1 = getCmdSelectAddress
                commandSql1 += " FROM address WHERE hn =" & dt.Rows(0)("hn") & " and `status` = 1;"
                addtype = ConTran.GetTable(commandSql1)
                getDataAddressTotalbe(addtype)
                Dim commandSql2 As String
                Dim dt2 As New DataTable
                commandSql2 = "Select name,telephone,mobile,relation"
                commandSql2 += " FROM personcontact WHERE hn =" & dt.Rows(0)("hn") & ";"
                dt2 = ConTran.GetTable(commandSql2)
                getDataPCToTable(dt2)
            Else
            End If
            ConTran.CommitTrans()
        Catch ex As Exception
            ConTran.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            ConTran.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' ดึงประวัติข้อมูลการเปลี่ยนชื่อของคนไข้
    ''' </summary>
    Public Function searchOldName(hnx As String) As DataTable
        Dim ConTran As ConnecDBRYH = ConnecDBRYH.NewConnection
        ConTran.BeginTrans()
        Dim sql As String
        Try
            sql = "SELECT d_update as Date,hispnrename.`NAME` as Name ,hispnrename.`lname` as Lastname,ename as EName,emname as Middlename,elname as ELastname,remark as Remark,concat(hospemp.`name`,' ',hospemp.`lname`) as 'User Request' "
            sql += "FROM hispnrename "
            sql += "JOIN (SELECT `empid`,`name`,`lname` FROM  hospemp WHERE `status` = 1) as hospemp on hispnrename.`usmk` = hospemp.`empid` "
            sql += "WHERE hn = '" + hnx + "';"
            Return ConTran.GetTable(sql)
            ConTran.CommitTrans()
        Catch ex As Exception
            ConTran.RollbackTrans()
            MsgBox(ex.ToString)
            Return Nothing
        Finally
            ConTran.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' บันทึกประวัติการเปลี่ยนชื่อ
    ''' </summary>
    ''' <remarks>
    ''' #เปลี่ยนชื่อ
    ''' หลังจากมีการปรับปรุงชื่อจะมีการอัพเดทข้อมูล
    ''' </remarks>
    Public Function saveOldName()
        Dim ConTran As ConnecDBRYH = ConnecDBRYH.NewConnection
        ConTran.BeginTrans()
        Dim sql As String
        Try
            sql = "INSERT INTO hispnrename"
            sql += "(`hn`,`prename`,`name`,`ename`,`emname`,`lname`,`elname`,`remark`,`usmk`,`d_update`)"
            sql += "VALUES (@HN,@PRENAME,@NAME,@ENAME,@EMNAME,@LNAME,@ELNAME,@REMARK,@USMK,@D_UPDATE)"
            mySqlCommand.Parameters.Clear()
            mySqlCommand.CommandText = sql
            mySqlCommand.Parameters.AddWithValue("@HN", HN) '
            mySqlCommand.Parameters.AddWithValue("@PRENAME", REPRENAME) '
            mySqlCommand.Parameters.AddWithValue("@NAME", RENAME) '
            mySqlCommand.Parameters.AddWithValue("@ENAME", REENAME) '
            mySqlCommand.Parameters.AddWithValue("@EMNAME", REEMNAME) '
            mySqlCommand.Parameters.AddWithValue("@LNAME", RELNAME) '
            mySqlCommand.Parameters.AddWithValue("@ELNAME", REELNAME) '
            mySqlCommand.Parameters.AddWithValue("@REMARK", REMARK) '
            mySqlCommand.Parameters.AddWithValue("@USMK", USMK) '
            mySqlCommand.Parameters.AddWithValue("@D_UPDATE", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")) '
            idlast = ConTran.ExecuteScalar_Parameter(mySqlCommand)
            ConTran.CommitTrans()
        Catch ex As Exception
            ConTran.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            ConTran.Dispose()
        End Try
    End Function
    ''' <summary>
    ''' ระบบอัพเดทข้อมูลที่อยู่
    ''' </summary>
    ''' <remarks>#อัพเดทข้อมูลที่อยู่</remarks>
    Public Sub updateAddress()
        Dim ConTran As ConnecDBRYH = ConnecDBRYH.NewConnection
        ConTran.BeginTrans()
        Dim sql As String
        Dim commandTxt As String
        Try
            mySqlCommand.Parameters.Clear()
            sql = "UPDATE address SET `status` = 0 where  hn = '" & HN & "' and addresstype = 1 ; "
            mySqlCommand.CommandText = sql
            idlast = ConTran.ExecuteScalar_Parameter(mySqlCommand)
            'UPDATE ADDRESS2
            mySqlCommand.Parameters.Clear()
            sql = "UPDATE address SET `status` = 0 where  hn = '" & HN & "' and addresstype = 2 ; "
            mySqlCommand.CommandText = sql
            idlast = ConTran.ExecuteScalar_Parameter(mySqlCommand)
            'UPDATE ADDRESS3
            mySqlCommand.Parameters.Clear()
            sql = "UPDATE address SET `status` = 0 where  hn = '" & HN & "' and addresstype = 3 ; "
            mySqlCommand.CommandText = sql
            idlast = ConTran.ExecuteScalar_Parameter(mySqlCommand)

            'Insert Address ADDRESSTYPE1
            Dim sql1 As String
            Dim sql2 As String
            sql1 += "hn,"
            sql2 += "" & HN & ","
            sql1 += "addresstype,"
            sql2 += "1,"
            sql1 += "housetype,"
            sql2 += "'" & HOUSETYPE & "',"
            sql1 += "addresdec1,"
            sql2 += "'" & ADDRESDEC1 & "',"
            sql1 += "ampid,"
            sql2 += "" & AMPUR1 & ","
            sql1 += "postcode,"
            sql2 += "'" & POSTCODE1 & "',"
            sql1 += "country,"
            sql2 += "'" & COUNTRY & "',"
            sql1 += "telephone,"
            sql2 += "'" & TELEPHONE & "',"
            sql1 += "mobile,"
            sql2 += "'" & MOBILE & "',"
            sql1 += "d_update,"
            sql2 += "current_timestamp(),"
            sql1 += "status,"
            sql2 += "1,"
            sql1 += "codechangwat,"
            sql2 += "" & CHANGWAT1 & ","
            sql1 += "tmbid"
            sql2 += "" & TMB1 & ""

            Dim sql3 As String
            sql3 = "INSERT INTO  address (" & sql1 & ") values ( " & sql2 & ");"
            ConTran.ExecuteNonQuery(sql3)

            'Insert Address ADDRESSTYPE2
            sql1 = ""
            sql2 = ""
            sql3 = ""
            If ADDRESDEC2 <> "" Then
                sql1 += "hn,"
                sql2 += "" & HN & ","
                sql1 += "addresstype,"
                sql2 += "2,"
                sql1 += "housetype,"
                sql2 += "0,"
                sql1 += "addresdec1,"
                sql2 += "'" & ADDRESDEC2 & "',"
                sql1 += "ampid,"
                sql2 += "" & AMPUR2 & ","
                sql1 += "postcode,"
                sql2 += "'" & POSTCODE2 & "',"
                sql1 += "country,"
                sql2 += "" & COUNTRY2 & ","
                sql1 += "d_update,"
                sql2 += "current_timestamp(),"
                sql1 += "status,"
                sql2 += "1,"
                sql1 += "codechangwat,"
                sql2 += "" & CHANGWAT2 & ","
                sql1 += "tmbid"
                sql2 += "" & TMB2 & ""
                sql3 = "INSERT INTO  address (" & sql1 & ") VALUES ( " & sql2 & ");"
                ConTran.ExecuteNonQuery(sql3)
            End If
            sql1 = ""
            sql2 = ""
            sql3 = ""
            If ADDRESDEC3 <> "" Then
                sql1 += "hn,"
                sql2 += "" & HN & ","
                sql1 += "addresstype,"
                sql2 += "3,"
                sql1 += "housetype,"
                sql2 += "0,"
                sql1 += "addresdec1,"
                sql2 += "'" & ADDRESDEC3 & "',"
                sql1 += "ampid,"
                sql2 += "" & AMPUR3 & ","
                sql1 += "postcode,"
                sql2 += "'" & POSTCODE3 & "',"
                sql1 += "country,"
                sql2 += "" & COUNTRY3 & ","
                sql1 += "d_update,"
                sql2 += "current_timestamp(),"
                sql1 += "status,"
                sql2 += "1,"
                sql1 += "codechangwat,"
                sql2 += "" & CHANGWAT3 & ","
                sql1 += "telephone,"
                sql2 += "'" & TELEPHONE2 & "',"
                sql1 += "tmbid"
                sql2 += "" & TMB3 & ""
                sql3 = "INSERT INTO  address (" & sql1 & ") values ( " & sql2 & ");"
                ConTran.ExecuteNonQuery(sql3)
                '    'INSERT Adress ADDRESSTYPE3
            End If
            ConTran.CommitTrans()
        Catch ex As Exception
            ConTran.RollbackTrans()
            MsgBox(ex.ToString)
        Finally
            ConTran.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' หลังจากการบันทึกข้อมูลผู้ป่วยใหม่ จะเพิ่มสิทธิของผู้ป่วย
    ''' </summary>
    ''' <param name="hnx">ต้องส่งค่าHN มา</param>
    ''' <returns>#เพิ่มสิทธิ์</returns>
    Public Function getAndInsertContact(hnx As String) As DataTable
        Dim contran As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim commandtxt As String
        contran.BeginTrans()

        commandtxt = "INSERT INTO contact "
        commandtxt += " (hn,mtrgtid,strgtid,sbrgtid,ptrgtid,startdate,expiredate,ae,opd,ipd,amount,d_update,status) "
        commandtxt += " VALUES (@HN,@MTRGTID,@STRGTID,@SBRGTID,@PTRGTID,@STARTDATE,@EXPIREDATE,@AE,@OPD,@IPD,@AMOUNT,@D_UPDATE,@STATUS)"
        mySqlCommand.Parameters.Clear()
        mySqlCommand.CommandText = commandtxt
        mySqlCommand.Parameters.AddWithValue("@HN", HN) ' เลขผู้ป่วยนอก HN

        mySqlCommand.Parameters.AddWithValue("@MTRGTID", 99) '
        mySqlCommand.Parameters.AddWithValue("@STRGTID", Nothing) '
        mySqlCommand.Parameters.AddWithValue("@SBRGTID", Nothing) '
        mySqlCommand.Parameters.AddWithValue("@PTRGTID", Nothing) '
        mySqlCommand.Parameters.AddWithValue("@STARTDATE", Date.Now.ToString("yyyy-MM-dd", USCulture)) '
        mySqlCommand.Parameters.AddWithValue("@EXPIREDATE", "2099-01-01") '
        mySqlCommand.Parameters.AddWithValue("@AE", 0)
        mySqlCommand.Parameters.AddWithValue("@OPD", 0)
        mySqlCommand.Parameters.AddWithValue("@IPD", 0)
        mySqlCommand.Parameters.AddWithValue("@AMOUNT", 0)
        mySqlCommand.Parameters.AddWithValue("@D_UPDATE", Date.Now)
        mySqlCommand.Parameters.AddWithValue("@STATUS", 1)
        idlast = contran.ExecuteScalar_Parameter(mySqlCommand)

        contran.CommitTrans()
        contran.Dispose()

        contran = ConnecDBRYH.NewConnection
        Dim dt As New DataTable
        Dim sql As String
        sql = "SELECT "
        sql += "conname, pconid,mtrgtid,strgtid,sbrgtid,ptrgtid,startdate,expiredate,remark,ae,opd,ipd,amount,hn,status FROM  contact WHERE hn ='" & hnx & "' "

        dt = contran.GetTable(sql)
        dt = checkContact(dt)
        contran.Dispose()
        Return dt
    End Function
    ''' <summary>
    ''' งดใช้
    ''' </summary>
    Private Function checkContact(dtx As DataTable) As DataTable
        Dim sql As String

        'If dtx.Rows(0)("SBRGTID").ToString.Length > 0 Then
        '    sql = "SELECT "
        'sql += "CONT1.STATUS,SUBRGTNAME,CONT1.PCONID AS PCONID ,CONT1.MTRGTID AS MTRGTID ,CONT1.STRGTID  AS STRGTID ,CONT1.SBRGTID AS SBRGTID,CONT1.PTRGTID AS PTRGTID,CONT1.STARTDATE AS STARTDATE,CONT1.EXPIREDATE AS EXPIREDATE, CONT1.REMARK AS REMARK ,CONT1.AE AS AE,CONT1.OPD AS OPD, CONT1.IPD AS IPD,CONT1.AMOUNT AS AMOUNT "
        'sql += " FROM (SELECT PCONID,MTRGTID,STRGTID,SBRGTID,PTRGTID,STARTDATE,EXPIREDATE,REMARK,AE,OPD,IPD,AMOUNT,HN,STATUS FROM  contact WHERE HN ='" & dtx.Rows(0)("HN").ToString & "') AS CONT1 "
        '    sql += " JOIN  myfrriendsdb.massubrgt ON CONT1.SBRGTID = massubrgt.SBRGTID"
        'Else
        '    If dtx.Rows(0)("STRGTID").ToString.Length > 0 Then
        '        sql = "SELECT "
        '    sql += "CONT1.STATUS  ,STRGTNAME,CONT1.PCONID AS PCONID ,CONT1.MTRGTID AS MTRGTID ,CONT1.STRGTID  AS STRGTID ,CONT1.SBRGTID AS SBRGTID,CONT1.PTRGTID AS PTRGTID,CONT1.STARTDATE AS STARTDATE,CONT1.EXPIREDATE AS EXPIREDATE, CONT1.REMARK AS REMARK ,CONT1.AE AS AE,CONT1.OPD AS OPD, CONT1.IPD AS IPD,CONT1.AMOUNT AS AMOUNT "
        '    sql += " FROM (SELECT PCONID,MTRGTID,STRGTID,SBRGTID,PTRGTID,STARTDATE,EXPIREDATE,REMARK,AE,OPD,IPD,AMOUNT,HN,STATUS FROM  contact WHERE HN ='" & dtx.Rows(0)("HN").ToString & "') AS CONT1 "
        '        sql += " JOIN  myfrriendsdb.masstypergt ON CONT1.STRGTID = masstypergt.STRGTID"
        '    Else

        '        sql = "SELECT "
        '    sql += "CONT1.STATUS  ,MTRGTNAME,CONT1.PCONID AS PCONID ,CONT1.MTRGTID AS MTRGTID ,CONT1.STRGTID  AS STRGTID ,CONT1.SBRGTID AS SBRGTID,CONT1.PTRGTID AS PTRGTID,CONT1.STARTDATE AS STARTDATE,CONT1.EXPIREDATE AS EXPIREDATE, CONT1.REMARK AS REMARK ,CONT1.AE AS AE,CONT1.OPD AS OPD, CONT1.IPD AS IPD,CONT1.AMOUNT AS AMOUNT "
        '    sql += " FROM (SELECT PCONID,MTRGTID,STRGTID,SBRGTID,PTRGTID,STARTDATE,EXPIREDATE,REMARK,AE,OPD,IPD,AMOUNT,HN,STATUS FROM  contact WHERE HN ='" & dtx.Rows(0)("HN").ToString & "') AS CONT1 "
        '        sql += " JOIN  myfrriendsdb.masmtypergt ON CONT1.MTRGTID = masmtypergt.MTRGTID"

        '    End If

        'End If

        Dim dt As DataTable
        Dim contran As ConnecDBRYH = ConnecDBRYH.NewConnection

        dt = contran.GetTable(sql)
        Return dt

    End Function

    ''' <summary>
    ''' ดึงข้อมูลแพ้ยา
    ''' </summary>
    ''' <remarks>#แพ้ยา</remarks>
    Public Function getDrugAlert(hnx As String) As DataTable
        Dim ConTran As ConnecDBRYH = ConnecDBRYH.NewConnection
        ConTran.BeginTrans()
        Dim dt As New DataTable()
        Dim sql As String = "SELECT "
        sql += "dalgid,drugallergy,f_complete "
        sql += "FROM  drugallergy "
        sql += "WHERE(`status` = 1 AND hn = " & hnx & ")"
        If hnx.Trim <> "" Then
            dt = ConTran.GetTable(sql)
        End If
        ConTran.CommitTrans()
        ConTran.Dispose()
        Return dt
    End Function
    ''' <summary>
    ''' ดึงคู่สัญญาหรือสิทธิ์ของ HN นั้นๆ
    ''' </summary>
    ''' <remarks>#คู่สัญญา,#contact,#สิทธิ</remarks>
    ''' <returns>Return กลับเป็น ดาต้าเทเบิ้ล</returns>
    Public Function getContact(hnx As String) As DataTable
        Dim contran As ConnecDBRYH = ConnecDBRYH.NewConnection
        contran.BeginTrans()
        Dim dt As New DataTable
        Dim sql As String
        sql = "SELECT "
        sql += "conname, pconid,mtrgtid,strgtid,sbrgtid,ptrgtid,startdate,expiredate,remark,ae,opd,ipd,amount,hn,status FROM  contact WHERE hn ='" & hnx & "' "
        dt = contran.GetTable(sql)
        contran.CommitTrans()

        If dt.Rows.Count = 0 Then
            dt = getAndInsertContact(hnx)
        Else

        End If

        contran.Dispose()
        Return dt
    End Function

    ''' <summary>
    ''' ดึงข้อมูลแพ้ยา,แบบที่คอนเฟริมการแพ้แล้ว
    ''' </summary>
    ''' <remarks>#แพ้ยา</remarks>
    Public Function getDrugAlert(hnx As String, Iscomplete As Boolean) As DataTable
        Dim ConTran As ConnecDBRYH = ConnecDBRYH.NewConnection
        ConTran.BeginTrans()
        Dim dt As New DataTable()
        Dim sql As String = "SELECT "
        sql += "dalgid,drugallergy,f_complete "
        sql += "FROM  drugallergy "
        sql += "WHERE(`status` = 1 AND hn = " + hnx + " AND f_complete = " & Iscomplete & ")"
        If hnx.Trim <> "" Then
            dt = ConTran.GetTable(sql)
        End If
        ConTran.CommitTrans()
        ConTran.Dispose()
        Return dt
    End Function

    Public Shared Function getDrugAlertSql(hn As String) As String
        Dim sql As String
        sql = "SELECT da.dalgid, da.`daterecord`, da.prdcode, mpd.prdname, da.drugallergy, da.dname, da.informant, mif.informant AS 'informantName', da.`typedx`, mtdx.typename, da.`alevel`, mal.maslvdes, mal.maslvdes_en, da.`symptom`, mst.icd10tm, mst.symdesc, da.`alerttype`, malrt.alertname, da.`informhosp`, mhos. hospid, mhos.hospname, da.d_update, da.f_complete, da.`status`, da.f_spuse, da.`f_alert`, da.provider, he.providername, da.`getwell`, da.usmk, he2.usmkname, da.`usmktime` FROM drugallergy AS da LEFT JOIN (SELECT prdcode, prdname FROM masproduct WHERE `status` = 1) AS mpd ON mpd.prdcode = da.prdcode LEFT JOIN (SELECT infid, informant FROM masinfor WHERE `status` = 1) AS mif ON mif.infid = da.`informant` LEFT JOIN (SELECT typedx, typename FROM mastypedx WHERE `status` = 1) AS mtdx ON mtdx.typedx = da.`typedx` LEFT JOIN (SELECT alevel, maslvdes, maslvdes_en FROM masalevel WHERE `status` = 1) AS mal ON mal.alevel = da.alevel LEFT JOIN (SELECT symptom, icd10tm, symdesc FROM massymptom WHERE `status` = 1) AS mst ON mst.symptom = da.`symptom` LEFT JOIN (SELECT alerttype, alertname FROM masalert WHERE `status` = 1) AS malrt ON malrt.alerttype = da.`alerttype` LEFT JOIN (SELECT hospid, hospname FROM mashosp) AS mhos ON mhos.hospid = da.informhosp LEFT JOIN (SELECT CONCAT(`name`, ' ', `lname`) AS 'providername', empid FROM hospemp) AS he ON he.empid = da.provider LEFT JOIN (SELECT CONCAT(`name`, ' ', `lname`) AS 'usmkname', empid FROM hospemp) AS he2 ON he2.empid = da.usmk WHERE da.`hn` = '" & hn & "'"
        Return sql
    End Function

    ''' <summary>
    ''' ไว้สำหรับเช็ค โรคประจำตัว
    ''' </summary>
    ''' <remarks>#clash,#โรคประจำตัว</remarks>
    ''' <returns>เป็น Datatable</returns>
    Public Function getCondisease(hnx As String) As DataTable
        Dim ConTran As ConnecDBRYH = ConnecDBRYH.NewConnection
        ConTran.BeginTrans()
        Dim dt As New DataTable()
        Dim sql As String = "SELECT "
        sql += "diseaseid,disease,hn "
        sql += "FROM  condisease "
        sql += "WHERE(`status` = 1 AND hn = " & hnx & ")"
        If hnx.Trim <> "" Then
            dt = ConTran.GetTable(sql)
        End If
        ConTran.CommitTrans()
        ConTran.Dispose()
        Return dt
    End Function

    Public Shared Function getCondiseaseSql(hn As String) As String
        Dim sql As String
        sql = "SELECT diseaseid, disease, hn FROM condisease WHERE status = '1' AND hn = '" & hn & "'"
        Return sql
    End Function

    ''' <summary>
    ''' ดึงข้อมูลนามแฝง
    ''' </summary>
    ''' <remarks>#นามแฝง</remarks>
    ''' <param name="id">ประเภทของนามแฝงต่างๆ</param>
    ''' <returns>ชื่อและนามสกุล</returns>
    Public Shared Function getPenname(sex As String) As DataTable
        Dim command As String
        Dim dt As New DataTable
        command = "SELECT penname,penlname FROM maspenname WHERE sex ='1' AND status ='1'"
        Dim ConTran As ConnecDBRYH = ConnecDBRYH.NewConnection
        dt = ConTran.GetTable(command)
        ConTran.Dispose()
        Return dt
    End Function
    Public Overridable Function ListNewQ()

        Try
            Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

            Dim sql As String = "SELECT "
            sql += "h.`penid`, "
            sql += "CAST(h.hn AS CHAR(15)) AS HN, CASE WHEN h.`penid` = 0 THEN CONCAT(p.`name`,' ',p.lname) ELSE CONCAT(penname.`penname`,' ', penname.`penlname`) END AS 'name', "
            sql += "h.penid,cstatus AS '..',h.vn AS 'VN' "
            sql += "FROM  frnservice AS h "
            sql += "INNER JOIN (SELECT * FROM  frnclinic WHERE clinic IN (16) AND cstatus IN (1,2,3,4) AND doctor ='2534' ) AS c ON h.vn = c.vn "
            sql += "LEFT JOIN  person AS p ON h.hn = p.hn "
            sql += "LEFT JOIN (SELECT * FROM  maspenname WHERE `status` = 1) AS penname ON h.penid = penname.penid "
            sql += "ORDER BY c.sentime ASC;"

            'Dim sql As String = "SELECT CAST(h.hn AS CHAR(15)) AS HN,CASE WHEN(p.penname = 0) THEN CONCAT(p.`name`,' ',p.lname) ELSE '" & ENVIRONMENTCLASS.PENNAME.PenNameThai & "' END AS `ชื่อ - นามสกุล`,cstatus AS '..',h.vn AS 'VN' "
            'sql += "FROM  frnservice AS h "
            'sql += "INNER JOIN (SELECT * FROM  frnclinic WHERE clinic IN (" & CLINIC & ") AND cstatus IN (1,2,3,4) AND doctor ='" & DOCTORID & "' ) as c ON h.vn = c.vn "
            'sql += "LEFT JOIN  person AS p on h.hn = p.hn "
            'sql += "ORDER BY c.sentime ASC;"

            Dim dt As New DataTable()
            dt = db.GetTable(sql)
            Return dt
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Return Nothing

    End Function

End Class