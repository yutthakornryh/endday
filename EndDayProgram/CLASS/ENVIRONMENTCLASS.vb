﻿Public Class ENVIRONMENTCLASS '(ใช้เก็บค่าทั่วไปของระบบ)
    Public Class PATIENT
        ''' <summary>
        ''' คนไข้ใหม่
        ''' </summary>
        ''' PATIENT
        Public Shared ReadOnly NewPatientWord As String = " (คนไข้ใหม่)"
    End Class
    Public Class GETORDERSEQ
        ''' <summary>
        ''' +1 SEQ ใน table masorder sequence
        ''' </summary>
        ''' <remarks>UPDATE  masordseq SET `SEQ` = `SEQ` +1   WHERE(ID &gt; 1 AND YR = 58 AND `MM` = DATE_FORMAT(NOW(),'%m'));</remarks>
        Public Shared ReadOnly SQLCOMMAND As String = "UPDATE  masordseq SET `seq` = `seq` +1   WHERE( `mm` = DATE_FORMAT(NOW(),'%m') AND year = " & Date.Now.Year.ToString & "); "
    End Class
    ''' <summary>
    ''' ใช้เก็บค่าสำหรับผู้ป่วยที่ใช้นามแฝงสำหรับเข้ารับบริการในโรงพยาบาล
    ''' </summary>
    ''' Public Class
    Public Class PENNAME
        ''' <summary>
        ''' นามแฝงไทย
        ''' </summary>
        ''' PENNAME
        Public Shared ReadOnly PenNameThai As String = "นามแฝง ชื่อสกุลแฝง"
        ''' <summary>
        ''' นามแฝงอังกฤษ
        ''' </summary>
        Public Shared ReadOnly PenNameEnglish As String = "PENNAME PENNAME"
        ''' <summary>
        ''' เก็บค่าสี (แดง)
        ''' </summary>
        Public Shared ReadOnly PenNameColor As System.Drawing.Color = Color.Red
        ''' <summary>
        ''' เก็บค่าสี (ดำ)
        ''' </summary>
        Public Shared ReadOnly PenNameColor2 As System.Drawing.Color = Color.Black

    End Class
    Public Class PLD
        Public Shared ReadOnly po As Integer = "3" 'ออก PO แล้ว
        Public Shared ReadOnly compld As Integer = "6" 'รับสินค้าเข้าคลัง
        Public Shared ReadOnly sompld As Integer = "5" 'รับบางส่วน

    End Class

    Public Class BRG
        Public Shared ReadOnly ดำเนินการ As Integer = "1"
        Public Shared ReadOnly อนุมัติแล้ว As Integer = "8"
        Public Shared ReadOnly เบิกแล้ว As Integer = "9"
    End Class
    Public Class FCARD
        Public Shared ReadOnly F_CARD_TRUE As String = "อยู่"
        Public Shared ReadOnly F_CARD_FALE As String = "ไม่อยู่"
        Public Shared ReadOnly F_CARD_COLOR_TRUE As Color = Color.ForestGreen
        Public Shared ReadOnly F_CARD_COLOR_FALE As Color = Color.Red
    End Class
    Public Class MACHINE
        ''' <summary>
        ''' รหัสคลินิก
        ''' </summary>
        Public Shared Clinic As String = ""
        ''' <summary>
        ''' โค้ดคลินิก
        ''' </summary>
        Public Shared ClinicCode As String = "N/A"
        ''' <summary>
        ''' ชื่อคลินิก
        ''' </summary>
        Public Shared ClinicName As String = "N/A"

        Public Shared NextClinic As String = ""
        Public Shared Stkid As String = ""
        Public Shared IDCOM As String = ""
    End Class
    ''' <summary>
    ''' เก็บคำสถานะการเพิ่ม, อัพเดตข้อมูล
    ''' </summary>
    Public Class DB
        ''' <summary>
        ''' สถานะเมื่อเพิ่มสำเร็จ
        ''' </summary>
        Public Shared MsgInsert As String = "บันทึกสำเร็จ"
        ''' <summary>
        ''' สถานะเมื่อแก้ไขสำเร็จ
        ''' </summary>
        Public Shared MsgUpdate As String = "แก้ใขสำเร็จ"
        ''' <summary>
        ''' สถานะเมื่อเพิ่มไม่สำเร็จ
        ''' </summary>
        Public Shared MsgInsertError As String = "บันทึกไม่สำเร็จ"
        ''' <summary>
        ''' สถานะเมื่อแก้ไขไม่สำเร็จ
        ''' </summary>
        Public Shared MsgUpdateError As String = "แก้ใขไม่สำเร็จ"
    End Class

    Public Class OPERID
        Public Shared OPER As Integer = 0
    End Class

    ''' <summary>
    '''
    ''' </summary>
    ''' <remarks></remarks>

    Public Class CLINICID
        ''' <summary>
        ''' รหัสแผนกอายุรกรรม
        ''' </summary>
        Public Shared MED As Integer = 7
        ''' <summary>
        ''' ชื่อแผนกอายุรกรรม
        ''' </summary>
        Public Shared MED_NAME As String = "อายุรกรรม"
        ''' <summary>
        ''' รหัสแผนกคัดกรอง
        ''' </summary>
        Public Shared SCREENING As Integer = 13
        ''' <summary>
        ''' ชื่อแผนกคัดกรอง
        ''' </summary>
        Public Shared SCREENING_NAME As String = "จุดคัดกรอง"
    End Class
    Public Class TYPEIN
        ''' <summary>
        ''' รหัสแผนกอายุรกรรม
        ''' </summary>
        Public Shared APPOINT As Integer = 2
        ''' <summary>
        ''' ชื่อแผนกอายุรกรรม
        ''' </summary>
        Public Shared APPOINT_NAME As String = "มาตามนัดหมาย"
    End Class

    'Public Class FINANCEID
    '    Public Shared FINANCEFRONT_ID As String = "6"
    '    Public Shared FINANCEFRONT_NAME As String = "การเงินนอก"
    'End Class

    ''' <summary>
    ''' เก็บข้อมูลแผนกห้องยา
    ''' </summary>
    Public Class PHXID
        Public Shared F_DISCHARGE As Integer = 0
    End Class

    ''' <summary>
    ''' เก็บข้อมูลแผนกห้องยา
    ''' </summary>

    Public Class CLINICCODE
        Public Shared XRAY As String = "XRAY"
        Public Shared WRD As String = "WRD"
        Public Shared SCN As String = "SCN"
        Public Shared REG As String = "REG"
        Public Shared PT As String = "PT"
        Public Shared PRX As String = "PRX"
        Public Shared ORX As String = "ORX"
        Public Shared OPD As String = "OPD"
        Public Shared LAB As String = "LAB"
        Public Shared IPD As String = "IPD"
        Public Shared HD As String = "HD"
        Public Shared EXM As String = "EXM"
        Public Shared ER As String = "ER"
        Public Shared CASH As String = "CASH"
    End Class

    ''' <summary>
    ''' เก็บข้อมูลการออเดอร์
    ''' </summary>
    Public Class ORDEREVENT
        Public Shared POLICIES_LAB As Integer = 1
        Public Shared POLICIES_NORMAL As Integer = 0

        Public Shared DELMSGCHKIN_T_TH As String = "ออเดอร์นี้ได้ถูกยกเลิกแล้ว "
        Public Shared DELMSGCHKIN_ENG As String = "Order Cancel Complete "

        Public Shared DELMSGCHKIN_F_ENG As String = "Order Checkin Complete Cannot Cancel"
        Public Shared DELMSGCHKIN_F_TH As String = "ออเดอร์ไม่สามารถยกเลิกได้"

        Public Shared DELMSGCOMP_T_TH As String = "ออเดอร์นี้ได้ถูกยกเลิกแล้ว"
        Public Shared DELMSGCOMP_T_ENG As String = "Order Cancel Complete "

        Public Shared DELMSGCOMP_F_TH As String = "ออเดอร์นี้ไม่สามารถยกเลิกได้เนื่องจาก เสร็จสิ้นกระบวนการทำงานแล้ว"
        Public Shared DELMSGCOMP_F_ENG As String = "Order Complete Cannot Cancel"

    End Class

    ''' <summary>
    ''' เก็บข้อมูลประเภทการพิมพ์เอกสาร
    ''' </summary>
    Public Class PRINTERTYPE
        'A4
        Public Shared PRINTTYPEA4 As String = "99"

    End Class
    ''' <summary>
    ''' เก็บข้อมูลเกี่ยวกับ permission และการ login เพื่อใช้งาน
    ''' </summary>
    Public Class USERUSEEVENT
        Public Shared ADD As String = "PADD"
        Public Shared EDIT As String = "PUPD"
        Public Shared DELETE As String = "PDEL"
        Public Shared VIEW As String = "PSEE"
        Public Shared PRINT As String = "PPRT"
        Public Shared USERFAIL As String = "USRFAIL"
        '
        Public Shared ADDMSG_F As String = "ไม่มีสิทธิ์ในการบันทึกข้อมูล"
        Public Shared EDITMSG_F As String = "ไม่มีสิทธิ์ในการแก้ไขข้อมูล"
        Public Shared DELMSG_F As String = "ไม่มีสิทธิ์ในการลบข้อมูล"
        Public Shared VIEWMSG_F As String = "ไม่มีสิทธิ๋ในการดึงข้อมูลมาแสดงผล"
        Public Shared PRINTMSG_F As String = "ไม่มีสิทธิ๋ในการปริ้นเอกสาร"

        Public Shared USERFAILMSG_TH_F As String = "รหัสผ่านไม่ถูกต้อง กรุณาล๊อคอินใหม่อีกครั้ง"
        Public Shared USERFAILMSG_EN_F As String = "Wrong Password. Please try again."

        Public Shared USERFAILMSG_POLICIES_TH As String = "รหัสผ่านไม่มีสิทธิ์ในการใช้งานโปรแกรม"

        Public Shared USERFAILMSG_NOTREGIST_TH As String = "รหัสผ่านนี้ไม่มีอยู่ในระบบ"

        Public Shared USERCANNOT_LOGIN_TH As String = "กรุณา login เข้าใช้งาน"

        Public Shared ADDMSG_T As String = ""
        Public Shared EDITMSG_T As String = ""
        Public Shared DELMSG_T As String = ""
        Public Shared VIEWMSG_T As String = ""
        Public Shared PRINTMSG_T As String = ""

    End Class

    ''' <summary>
    ''' ตรวจว่าเป็นราคาประเภทใด OPD หรือ IPD
    ''' </summary>
    Public Class PRCSTAT
        Public Shared PRCOPD As Integer = 1
    End Class
    Public Class DEFAULTPRG
        Public Shared IDPRG As String = "1"

    End Class
    Public Class MASCSTATUS
        Public Shared wait As Integer = 1
        Public Shared waitdiag As Integer = 2
        Public Shared diag As Integer = 3
        Public Shared accept As Integer = 4
        Public Shared send As Integer = 5
        Public Shared complete As Integer = 6
        Public Shared returnOrder As Integer = 7
        Public Shared waitsend As Integer = 9
        Public Shared cancel As Integer = 10
        Public Shared senttodoctor As Integer = 11

        Public Shared msg As String = "สถานะคลินิกผิดพลาด"
        Public Shared msgnoclinic As String = "ผู้ป่วยไม่ได้รับบริการคลินิกนี้"
        Public Shared msgnotvn As String = "ผู้ป่วยยังไม่ได้เปิด VN"
        Public Shared msgfinish As String = "ผู้ป่วยจบการรักษาที่คลินิกนี้แล้ว"
    End Class

    Public Class MASRMSTATUS
        Public Shared ReadOnly Request As Integer = 1
        Public Shared ReadOnly Req_Dis As Integer = 2
        Public Shared ReadOnly Admit As Integer = 3
        Public Shared ReadOnly Discharge As Integer = 4
        Public Shared ReadOnly Cleaning As Integer = 5
        Public Shared ReadOnly Ready As Integer = 6
        Public Shared ReadOnly Recondition As Integer = 7
        Public Shared ReadOnly Req_Move As Integer = 8
        Public Shared ReadOnly Cash_Dis As Integer = 9
    End Class
    ''' <summary>
    ''' เก็บค่า tag ของโปรแกรม
    ''' </summary>
    Public Class GETTAGPROGRAM
        Public Shared LAB As String = "LAB000"
        Public Shared XRAY As String = "XRY000"
        Public Shared OPER As String = "OPR000"

    End Class
    ''' <summary>
    ''' เก็บสถานะประเภทต่างๆของ order
    ''' </summary>
    Public Class MASORDSTATUS
        Public Shared REQUEST As Integer = 1
        Public Shared CHECKIN As Integer = 2
        Public Shared COMPLETE As Integer = 3
        Public Shared PASS As Integer = 4
        Public Shared CANCEL As Integer = 5
        Public Shared SOMECOMPLETE As Integer = 6
        Public Shared PAID As Integer = 7
    End Class

    Public Class MASDISCHARGE
        Public Shared ตาย As Integer = 1
        Public Shared ย้าย As Integer = 2
        Public Shared สาบสูญ As Integer = 3
        Public Shared ไม่จำหน่าย As Integer = 9
    End Class
    ''' <summary>
    ''' เก็บชื่อประเภทรูป และ path ของรูป default
    ''' </summary>
    Public Class PICNAME
        Public Shared PICIDCARD As String = "\PicIDCard.jpg"
        Public Shared PICHN As String = "\HN_Profile.jpg"

        Public Shared TYPEPROFILE As String = "profile"
        Public Shared TYPEIDCARD As String = "idcard"
        Public Shared NOPIC As String = "NoPic"
        Public Shared CARD As String = "Card"
    End Class
    ''' <summary>
    ''' เก็บประเภทเอกสาร
    ''' </summary>
    Public Class DOCTYPE
        Public Shared OPDNOTE As String = "ON"
        Public Shared DRORDER As String = "DO"
        Public Shared ใบรับรองแพทย์ As String = "C1"
        Public Shared ใบรับรองการรักษา As String = "C2"
        Public Shared ADMITNOTE As String = "AN"

        Public Shared OPDNOTE_ID As String = 1
        Public Shared DRORDER_ID As String = 2
        Public Shared ใบรับรองแพทย์_ID As String = 3
        Public Shared ใบรับรองการรักษา_ID As String = 4
        Public Shared ADMITNOTE_ID As String = 5

    End Class

    Public Class MASDOCUMENT
        Public Shared ใบรับรองแพทย์ As Integer = 1
        Public Shared ใบรับรองแพทย์_5โรค As Integer = 2
        Public Shared ใบรับรองการรักษา As Integer = 3
        Public Shared ใบเคลมประกัน As Integer = 4
    End Class

    ''' <summary>
    ''' รูปแบบของใบเสร็จคู่สัญญา
    ''' </summary>
    Public Class CONTACT
        Public Shared TMPID As String = "1"
    End Class

    ''' <summary>
    ''' ip ของ database server
    ''' </summary>
    Public Class CONNECTION
        Public Shared IP As String = "172.30.10.9"
    End Class
    Public Class GETTYPERCCODE

        Public Shared OPD As String = "OPD"
        Public Shared IPD As String = "IPD"
        Public Shared HPD As String = "HPD"

    End Class
    Public Class GETMASTPRDCAT
        Public Shared PRX As Integer = 1
        Public Shared MEDICAL As Integer = 2
        Public Shared OPERATION As Integer = 3
        Public Shared XRAY As Integer = 4
        Public Shared LAB As Integer = 5
        Public Shared SUPLLIES As Integer = 6
        Public Shared OTHER As Integer = 7
    End Class
    Public Class GETMASPSTATUS
        Public Shared พบแพทย์ As Integer = 23
        Public Shared ชำระเงินแล้ว As Integer = 3
        Public Shared รอชำระเงิน As Integer = 29
        Public Shared GOHOME As Integer = 7
        Public Shared ADMIT As Integer = 11
        Public Shared รอADMIT As Integer = 12

    End Class
    Public Class GETDRUGALLERGYALERT
        Public Shared ALERTTYPE1 As String = "ไม่สามารถจ่ายยานี้ได้ เนื่องจากผู้ป่วยมีการแพ้ยานี้"
        Public Shared ALERTTYPE1_1 As String = "สามารถจ่ายยานี้ได้ แต่ผู้ป่วยมีการแพ้ยาชนิดนี้ ต้องดูแลเป็นพิเศษ"
        Public Shared ALERTTYPE2 As String = "สามารถจ่ายยานี้ได้ แต่ต้องมีการเฝ้าระวังการใช้ยา"
        Public Shared ALERTTYPE3 As String = "สามารถจ่ายยานี้ได้ แต่ต้องใช้ภายใต้การดูแลของเจ้าหน้าที่"
    End Class

    Public Class GETMASDIAG
        Public Shared PRINCIPLEDIAG As Integer = 1

    End Class

    'ประเภทเครื่องพิมพ์
    Public Class PRINTTYPE
        Public Shared ใบนำทาง As Integer = 1
        Public Shared ใบสื่อสาร As Integer = 2
        Public Shared บัตรนัด As Integer = 3
        Public Shared หน้าแฟ้ม As Integer = 4
        Public Shared ใบrequest As Integer = 5
        Public Shared ฉลากยา As Integer = 6
        Public Shared เอกสารA4 As Integer = 7
    End Class
End Class