﻿Module GENDOC
    Dim condb As ConnecDBRYH

    Public Function getIDform(ByVal menuname As String) As String
        Dim sql As String
        Dim dt As New DataTable
        Dim sql1 As String
        condb = ConnecDBRYH.NewConnection

        Try
            sql1 = "UPDATE mascmrunning SET isrunning=isrunning+1  WHERE menuname ='" & menuname & "';"
            condb.ExecuteNonQuery(sql1)
            sql = "SELECT * FROM mascmrunning WHERE menuname ='" & menuname & "';"
            Dim USCulture As New System.Globalization.CultureInfo("en-US", True)
            Dim THculture As New System.Globalization.CultureInfo("th-TH", True)

            dt = condb.GetTable(sql)
            Dim docno As String
            docno = If(IsDBNull(dt.Rows(0)("prefixchar")), "", dt.Rows(0)("prefixchar"))
            docno += If(IsDBNull(dt.Rows(0)("showyear")), Date.Now.ToString(dt.Rows(0)("formatdate"), THculture), Date.Now.ToString(dt.Rows(0)("formatdate"), USCulture))
            docno += If(IsDBNull(dt.Rows(0)("separatechar")), "", dt.Rows(0)("separatechar"))

            Dim countchar As Integer
            Dim isruning As String
            countchar = dt.Rows(0)("countchar")
            isruning = dt.Rows(0)("isrunning")
            For i = 0 To countchar - isruning.Length - 1
                docno += "0"
            Next
            docno += dt.Rows(0)("isrunning").ToString
            Return docno
            '  MsgBox(docno)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Function



    Public Function getGenerateId(tableName As String, pk As String) As String
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable
        Dim curdate As String = ""
        Dim dbHead As String = ""
        Dim dbRuning As String = ""
        Dim headLeight As Integer = 4
        Dim runLeight As Integer = 4

        db.BeginTrans()
        Dim id As String = ""
        Dim sql As String = "SELECT " & pk & ",SUBSTRING(" & pk & "," & headLeight + 1 & "," & runLeight & ") AS run,SUBSTRING(" & pk & ",1," & headLeight & ") AS head FROM  " & tableName & " ORDER BY " & pk & " DESC LIMIT 1;"
        dt = db.GetTable(sql)
        If dt.Rows.Count > 0 Then
            dbHead = dt.Rows(0)("head").ToString
            dbRuning = dt.Rows(0)("run").ToString
        End If
        sql = "SELECT DATE_FORMAT(CURRENT_TIMESTAMP(),'%y%m') cur"
        curdate = db.GetTable(sql).Rows(0)("cur").ToString
        If curdate = dbHead Then
            Dim numFormat As String = ""
            For i As Integer = 0 To runLeight - 1
                numFormat &= "0"
            Next i
            id = dbHead & (Convert.ToInt32(dbRuning) + 1).ToString(numFormat)
        Else
            Dim numFormat As String = ""
            For i As Integer = 0 To runLeight - 1
                If i = runLeight - 1 Then
                    numFormat &= "1"
                Else
                    numFormat &= "0"
                End If
            Next i
            id = curdate & numFormat
        End If
        db.CommitTrans()
        db.Dispose()
        Return id
    End Function

    'ใช้ดูข้อมูลต่างๆของ docno ใน table Freeze, ส่ง docno เข้ามา แล้วจะได้ข้อมูลของ docno เป็น datatable กลับไป
    Public Function GetDocnoFreezeInfo(docno As String) As DataTable
        Dim db = ConnecDBRYH.NewConnection
        Dim sql As String = "SELECT * FROM freezeprdstkorder WHERE docno = '" & docno & "'"
        Dim dtFreezeInfo As DataTable = db.GetTable(sql)
        db.Dispose()
        Return dtFreezeInfo
    End Function
End Module
