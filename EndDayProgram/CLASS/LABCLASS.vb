﻿Imports DevComponents.DotNetBar
Imports System.Windows.Forms
Imports DevComponents.DotNetBar.SuperGrid

Public Class LABCLASS
    Structure LABORDER
        Sub New(j As Integer)
            lab_item_group_code = "0"
            correct_date = "0000-00-00"
            lab_receive_number = "0"
        End Sub

        Dim lab_order_number As String
        Dim hn As String
        Dim title As String
        Dim fname As String
        Dim surname As String
        Dim sex As String
        Dim age As String
        Dim birthday As String
        Dim doctor_name As String
        Dim lab_item_group_code As String
        Dim order_date As String
        Dim order_time As String
        Dim correct_date As String
        Dim lab_receive_number As String
        Dim lab_status As String


    End Structure
    Structure LABORDERDT
        Dim lab_item_code As String
        Dim lab_item_name As String
        Dim lab_items_normal As String
        Dim lab_item_unit As String
        Dim specimen_code As String
        Dim result_flag As String
        Dim result_flag2 As String
        Dim critical_flag As String
        Dim lab_item_group_code As String
        Dim lab_item_group_name As String

        Dim out_lab As String

    End Structure
    Private PERSON As PERSONCLASS
    Private VN As VNCLASS.VNStruct
    Dim LabOrderStr As LABORDER
    Dim LabOrderDtStr As LABORDERDT
    Dim MSG_CANCLE As String
    Public Property MSG_CANCLE_ As String
    Public WriteOnly Property SetPerson As PERSONCLASS
        Set(value As PERSONCLASS)
            PERSON = value
        End Set
    End Property
    Public WriteOnly Property SetVN As VNCLASS.VNStruct
        Set(value As VNCLASS.VNStruct)
            VN = value
        End Set
    End Property

    Public Sub UpdateOrderClinicid(clinicid As String, orderid As String)
        Dim sql As String
        sql = "UPDATE prdorder SET clinicid = " & clinicid & " WHERE orderid =" & orderid & " AND f_cancel = 0 and f_lock <> 1 and status  = 1"
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        db.ExecuteNonQuery(sql)
        db.Dispose()
    End Sub
    Public Shared Sub getOrderPrd(ByVal Prdcat As String, ByVal VN As String, ByVal Hn As String, ByRef dtset As DataSet, Optional ByVal an As String = "null")
        Dim sql As String
        sql = " SELECT l.ORDERID , remark,d_order ,CONCAT_WS('',hos1.name , ' ' , hos1.lname ) AS drname ,dr_provider ,CONCAT_WS('',hos.name , ' ' , hos.lname) AS mkname ,mk_provider, masordstatus.odsname AS STAT   ,ordprc  FROM ( SELECT ord_status,clinicid,prdcat,vn,mk_provider,orderid,remark,dr_provider,d_order FROM  prdorder WHERE(prdcat = " & Prdcat & ")   AND vn =" & VN & " ) AS l "
        sql += " LEFT JOIN  hospemp AS hos ON  l.mk_provider = hos.empid   "
        sql += "LEFT JOIN ( SELECT empid,name,lname  FROM  hospemp  ) as hos1 ON l.dr_provider = hos1.empid"
        sql += "  JOIN  masordstatus ON masordstatus.ord_status = l.ord_status "
        sql += " LEFT JOIN ( SELECT SUM(prdprc) AS ordprc ,orderid FROM  prdorderdt WHERE   hn = " & Hn & " AND f_chkin = 1 AND f_cancel = 0   GROUP BY orderid ) AS priceorder ON l.orderid = priceorder.orderid "
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        db.GetTable(sql, dtset.Tables("PRDORDER"))
        If an <> "" Then
            sql = " SELECT l.ORDERID , remark,d_order ,CONCAT_WS('',hos1.name , ' ' , hos1.lname ) AS drname ,dr_provider ,CONCAT_WS('',hos.name , ' ' , hos.lname) AS mkname ,mk_provider, masordstatus.odsname AS STAT   ,ordprc  FROM ( SELECT ord_status,clinicid,prdcat,vn,mk_provider,orderid,remark,dr_provider,d_order FROM  prdorder WHERE(prdcat = " & Prdcat & ")   AND an =" & an & " ) AS l "
            sql += " LEFT JOIN  hospemp AS hos ON  l.mk_provider = hos.empid   "
            sql += "LEFT JOIN ( SELECT empid,name,lname  FROM  hospemp  ) as hos1 ON l.dr_provider = hos1.empid"
            sql += "  JOIN  masordstatus ON masordstatus.ord_status = l.ord_status "
            sql += " LEFT JOIN ( SELECT SUM(prdprc) AS ordprc ,orderid FROM  prdorderdt WHERE   hn = " & Hn & " AND f_chkin = 1 AND f_cancel = 0   GROUP BY orderid ) AS priceorder ON l.orderid = priceorder.orderid "
            db.GetTable(sql, dtset.Tables("PRDORDER"))
        End If
        db.Dispose()

    End Sub

    ''' <summary>
    ''' สำหรับ Insert ข้อมูลที่เป็น Order Lab
    ''' </summary>
    ''' <remarks>
    ''' ในการ Insert ข้อมูล LAB
    ''' 1.ต้องนำข้อมูลลง Prdorder
    ''' 2. เอาเลข PRDorder มาเก็บไว้เพื่อลงใน Prdorder dt
    ''' 3. หลังจากนั้น กวาด Order ย่อยในระบบ Lab item เพื่อ ดึง Insert ลงใน Table prdorderlabresult โดยการเอา ID Orderid dt แต่ละตัวมาใช้ ผ่าน loop
    ''' frnclinic clinic ที่ต้องส่งไป
    ''' TYPERCCODE เป็นโค๊ดที่บอกว่า OPD หริอ IPD
    ''' TYPEDATASET ชื่อตาราง
    ''' TYPECLINIC CLinicไหน
    ''' </remarks>
    Public Sub InsertOrder(grid As DataSet, frnclinic As Integer, ByVal USRPROLICIES As PROVIDERCLASS, ByVal TYPERCCODE As String, TYPEDATASET As String, TYPECLINIC As Integer, Optional doctor As Boolean = False, Optional Endday As Boolean = True)

        Dim db = ConnecDBRYH.NewConnection
        Dim db1 As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = ""
        Dim ISROLLBACK As Boolean = True
        sql = ENVIRONMENTCLASS.GETORDERSEQ.SQLCOMMAND
        Dim dtPrdorderid As DataTable
        dtPrdorderid = db.GetTable(sql + "SELECT CONCAT(CAST(yr AS NCHAR(2)),CAST(mm AS NCHAR(2)),CAST(`seq` AS NCHAR(20)))AS str FROM  masordseq  WHERE (`mm` = DATE_FORMAT(NOW(),'%m')) AND year = " & Date.Now.Year.ToString & "  LIMIT 1")
        Dim prdorderid As String = dtPrdorderid.Rows(0)(0)
        sql = ""

        db.BeginTrans()

        If Endday = True Then
            sql = "INSERT INTO  prdorder(orderid,f_comp,vn,an,prdcat,hn,clinic,d_order,mk_provider,dr_provider,clinicid,usmk,usmktime,ord_status,stkid) VALUES "
            sql += "(" & prdorderid & ", 1 , "

        Else
            sql = "INSERT INTO  prdorder(orderid,f_comp,vn,an,prdcat,hn,clinic,d_order,mk_provider,dr_provider,clinicid,usmk,usmktime,ord_status,stkid) VALUES "
            sql += "(" & prdorderid & ", 1 , "

        End If
   
        'VN
        If TYPERCCODE = ENVIRONMENTCLASS.GETTYPERCCODE.OPD Then
            sql += "" & VN._VN & ", "
            sql += "null, "
        ElseIf TYPERCCODE = ENVIRONMENTCLASS.GETTYPERCCODE.IPD Then
            sql += "" & VN._VN & ", "
            sql += "" & VN._AN & ", "
        End If

        'PRDCAT
        If grid.Tables(TYPEDATASET).Rows.Count > 0 Then
            sql += "" & grid.Tables(TYPEDATASET).Rows(0)("prdcat").ToString & ", " 'PRDCAT
        Else
            sql += "0, "
        End If
        sql += "" & VN._HN & ", "

        sql += " " & VN._BCLINIC & ","
        'D_ORDER
        sql += "current_timestamp(), "
        'MK_PROVIDER
        sql += "2," 'MK_Provider
        'DR_PROVIDER
        If VN._DR = Nothing Then
            sql += "Null , "
        Else
            sql += "" & VN._DR & " ," 'DR_PROVIDER
        End If
        'CLINICID
        sql += "" & frnclinic & ",  "
        sql += "2, "
        sql += "current_timestamp(), "
        If ENVIRONMENTCLASS.MACHINE.ClinicCode = ENVIRONMENTCLASS.CLINICCODE.REG Then
            sql += " " & ENVIRONMENTCLASS.MASORDSTATUS.CHECKIN & ","
        Else
            sql += "" & ENVIRONMENTCLASS.MASORDSTATUS.REQUEST & ","
        End If

        sql += " " & STK_ID(VN._BCLINIC) & " "

        sql += ");"

        If grid.Tables(TYPEDATASET).Rows.Count > 0 Then
            db.ExecuteNonQuery(sql)
        End If

        sql = ""





        For i As Integer = 0 To grid.Tables(TYPEDATASET).Rows.Count - 1

            Dim STK_COMP As Boolean = True
            Dim ISSTKCTRL As Boolean = False
            Dim _STKID As String = 0

            Dim _LOTID As String = 0
            'ตัดสต็อค'

            Dim _PRDCODE As String = grid.Tables(TYPEDATASET).Rows(i)("prdcode").ToString
            Dim _QTY As String = grid.Tables(TYPEDATASET).Rows(i)("QTY").ToString
            Dim _UNIT As String = grid.Tables(TYPEDATASET).Rows(i)("S_UNIT").ToString
            ISSTKCTRL = grid.Tables(TYPEDATASET).Rows(i)("F_STK").ToString

            If ISSTKCTRL Then
                _STKID = STK_ID(VN._CLINIC)
                STK_COMP = False
                STK_COMP = STK_ONHAND(_STKID, grid.Tables(TYPEDATASET).Rows(i)("F_LOT").ToString, _PRDCODE, _QTY, _UNIT, _LOTID)    'ตัดสตอค
            End If

            If STK_COMP Then    'กรณีตัดสตอคได้ เวชภัณฑ์คงคลังเพียงพอ หรือกรณี ORDER TYPE -> บันทึกรายการ
                If Endday = True Then
                    sql += "INSERT  prdorderdt(orderid,f_comp,prdcode,chkin_provider,chkin_date,prdprc,ccode,rcode,hn,prdcat,qty,usmk,usmktime,ord_status,f_chkin,unitid, lotid,remark) VALUES "
                    sql += "(" & prdorderid & ", 1,"
                Else
                    sql += "INSERT  prdorderdt(orderid,prdcode,chkin_provider,chkin_date,prdprc,ccode,rcode,hn,prdcat,qty,usmk,usmktime,ord_status,f_chkin,unitid, lotid,remark) VALUES "
                    sql += "(" & prdorderid & ","
                End If
                
                sql += "" & grid.Tables(TYPEDATASET).Rows(i)("prdcode").ToString & ", " 'PRDCODE

                If ENVIRONMENTCLASS.MACHINE.ClinicCode = ENVIRONMENTCLASS.CLINICCODE.LAB Then
                    sql += " 0 ,"
                    sql += "null,"

                ElseIf ENVIRONMENTCLASS.MACHINE.ClinicCode = ENVIRONMENTCLASS.CLINICCODE.XRAY Then
                    sql += " 0 ,"
                    sql += "null,"
                Else
                    sql += "2," 'CHKIN_PROVIDER ผู้ทำรายการ
                    sql += "current_timestamp(),"

                End If

                If TYPERCCODE = ENVIRONMENTCLASS.GETTYPERCCODE.OPD Then

                    If doctor = True Then

                    Else
                        sql += "" & grid.Tables(TYPEDATASET).Rows(i)("opdprc").ToString & " , " 'PRDPRC ราคาสินค้า

                    End If

                    sql += "" & grid.Tables(TYPEDATASET).Rows(i)("ccode").ToString & " , "
                    sql += "" & grid.Tables(TYPEDATASET).Rows(i)("rcode").ToString & "  ,"
                ElseIf TYPERCCODE = ENVIRONMENTCLASS.GETTYPERCCODE.IPD Then

                    sql += "" & grid.Tables(TYPEDATASET).Rows(i)("ipdprc").ToString & " , " 'PRDPRC ราคาสินค้า

                    sql += "" & grid.Tables(TYPEDATASET).Rows(i)("iccode").ToString & " , "
                    sql += "" & grid.Tables(TYPEDATASET).Rows(i)("ircode").ToString & "  ,"

                End If
                sql += "" & VN._HN & ","

                sql += "" & grid.Tables(TYPEDATASET).Rows(i)("prdcat").ToString & ", "
                sql += "" & grid.Tables(TYPEDATASET).Rows(i)("qty").ToString & " ,"
                'MK_PROVIDER

                sql += "2," 'CHKIN_PROVIDER ผู้ทำรายการ
                sql += "current_timestamp(),"

                If grid.Tables(TYPEDATASET).Rows(i)("prdcat").ToString = ENVIRONMENTCLASS.GETMASTPRDCAT.OPERATION Or grid.Tables(TYPEDATASET).Rows(i)("prdcat").ToString = ENVIRONMENTCLASS.GETMASTPRDCAT.PRX Then

                    Dim sql2 As String
                    sql2 = "SELECT chkin from operatitem WHERE PRDCODE = " & grid.Tables(TYPEDATASET).Rows(i)("prdcode").ToString & " and STATUS = 1  "
                    Dim dbcon As ConnecDBRYH = ConnecDBRYH.NewConnection
                    Dim dt As DataTable
                    dt = dbcon.GetTable(sql2)
                    dbcon.Dispose()
                    If dt.Rows.Count >= 1 Then
                        If dt.Rows(0)("chkin").ToString = "True" Then
                            sql += "" & ENVIRONMENTCLASS.MASORDSTATUS.REQUEST & ","
                            sql += "0 ,"
                        Else
                            sql += "" & ENVIRONMENTCLASS.MASORDSTATUS.CHECKIN & ","
                            sql += "1 ,"

                        End If

                    Else
                        sql += "" & ENVIRONMENTCLASS.MASORDSTATUS.CHECKIN & ","
                        sql += "1 ,"

                    End If

                Else
                    sql += "" & ENVIRONMENTCLASS.MASORDSTATUS.REQUEST & ","
                    sql += "0 , "
                End If
                sql += "" & grid.Tables(TYPEDATASET).Rows(i)("s_unit").ToString & ", "

                If _LOTID = 0 Then
                    sql += " NULL ,"
                Else
                    sql += " " & _LOTID & ", "
                End If
                sql += "'" & grid.Tables(TYPEDATASET).Rows(i)("Comment").ToString & "' "
                sql += ");SELECT LAST_INSERT_ID();"

                Dim idprorderdt As Integer
                idprorderdt = db.ExecuteScalar(sql)

                If ENVIRONMENTCLASS.MACHINE.ClinicCode = ENVIRONMENTCLASS.CLINICCODE.LAB Then
                    LabOrderStr.hn = VN._HN
                    LabOrderStr.age = VN._AGEYEAR
                    LabOrderStr.birthday = VN._BIRTH
                    LabOrderStr.fname = VN._NAME
                    LabOrderStr.surname = VN._LNAME
                    LabOrderStr.title = VN._FTPRENAME
                    LabOrderDtStr.lab_item_code = ""
                    LabOrderStr.lab_status = "0"
                    LabOrderStr.lab_order_number = prdorderid
                    LabOrderStr.order_date = Date.Now.Year.ToString("yyyy") + "-" + Date.Now.ToString("MM-dd")
                    LabOrderStr.order_time = Date.Now.ToString("HH:mm")
                    sql = InsertMaster()
                    db.ExecuteNonQuery(sql)





                    sql = ""

                    Dim sql2 As String = "SELECT DISTINCT  * FROM  lab_item WHERE lab_order_code ='" & grid.Tables(TYPEDATASET).Rows(i)("code").ToString & "' AND f_cancel = 0 ;"
                    db1 = ConnecDBRYH.NewConnection
                    db1.BeginTrans()
                    Dim dt As DataTable = New DataTable
                    dt = db1.GetTable(sql2)
                    For j As Integer = 0 To dt.Rows.Count - 1
                        Dim sql3 As String
                        Dim sql4 As String

                        'sql = "INSERT interfacedb.lab_detail "
                        sql3 = "lab_order_number ,"
                        sql4 = " " & prdorderid & " ,"
                        sql3 += "lab_items_code ,"
                        sql4 += " '" & dt.Rows(j)("code").ToString.Trim & "' ,"
                        sql3 += "lab_items_name ,"
                        sql4 += " '" & dt.Rows(j)("name").ToString.Trim & "',"

                        sql3 += "lab_items_unit ,"
                        sql4 += " '" & dt.Rows(j)("unit").ToString.Trim & "',"


                        sql3 += "lab_items_normal ,"
                        sql4 += " '" & dt.Rows(j)("normal").ToString.Trim & "',"


                        sql3 += "result_flag ,"
                        sql4 += " 1,"

                        sql3 += "result_flag2 ,"
                        sql4 += " 0,"
                        sql3 += "order_date ,"
                        sql4 += " DATE_FORMAT(NOW(),'%Y-%d-%m'),"

                        sql3 += "order_time ,"
                        sql4 += " DATE_FORMAT(NOW(),'%h:%i %p'),"
                        sql3 += "critical_flag "
                        sql4 += " 0"

                        sql = "INSERT INTO interfacedb.lab_detail ( " & sql3 & " )  VALUES  ( " & sql4 & " );SELECT LAST_INSERT_ID(); "
                        Dim labdetail As Integer
                        labdetail = db.ExecuteScalar(sql)

                        sql = ""
                        sql += "INSERT  prdordelabresult  ( labcode,name,unit,normal,orderdtid,comment, hn,an,vn ,serial) VALUES  "
                        sql += " ( " & grid.Tables(TYPEDATASET).Rows(i)("prdcode").ToString & ",  "
                        sql += "'" & MySql.Data.MySqlClient.MySqlHelper.EscapeString(dt.Rows(j)("name").ToString.Trim) & "' , "
                        sql += "'" & dt.Rows(j)("unit").ToString.Trim & "' , "
                        sql += "'" & dt.Rows(j)("normal").ToString.Trim & "'  , "
                        sql += "" & idprorderdt & " , "
                        sql += "'" & dt.Rows(j)("comment").ToString.Trim & "'  ,"
                        sql += " '" & VN._HN & "', "
                        sql += " " & If(VN._AN = "", "null", VN._AN) & " , "
                        sql += " '" & VN._VN & "' , '" & labdetail & "' ) "

                        sql += " ; "
                        db.ExecuteNonQuery(sql)
                    Next

                End If

                sql = ""
                ISROLLBACK = False
            Else
                ISROLLBACK = True
            End If

        Next i

        Try
            If ISROLLBACK Then
                db.RollbackTrans()
                db1.RollbackTrans()
                'MsgBox("บันทึกไม่สำเร็จ : ไม่มีเวชภัณฑ์ในคลัง")
            Else
                db.CommitTrans()
                db1.CommitTrans()
                'MsgBox("บันทึกสำเร็จ")
            End If
        Catch ex As Exception
            db.RollbackTrans()
            db1.RollbackTrans()
            'MsgBox("บันทึกไม่สำเร็จ : " & ex.Message)
        Finally
            db.Dispose()
            db1.Dispose()
        End Try
    End Sub


    Public Function InsertMaster() As String
        Dim sql As String
        Dim sql1 As String
        sql = "lab_order_number , "
        sql1 = " '" & LabOrderStr.lab_order_number & "',"

        sql += "hn ,"
        sql1 += " '" & LabOrderStr.hn & "' , "
        sql += "title, "
        sql1 += "'" & LabOrderStr.title & "' , "
        sql += "fname,"
        sql1 += " '" & LabOrderStr.fname & "',"


        sql += "surname,"
        sql1 += " '" & LabOrderStr.surname & "',"


        sql += "age,"
        sql1 += " '" & LabOrderStr.age & "',"

        sql += "birthday,"
        sql1 += " '" & LabOrderStr.birthday & "',"

        sql += "order_date ,"
        sql1 += " DATE_FORMAT(NOW(),'%Y-%d-%m'),"

        sql += "order_time ,"
        sql1 += " DATE_FORMAT(NOW(),'%h:%i %p'),"




        sql += "lab_status"
        sql1 += " '" & LabOrderStr.lab_status & "'"
        Dim sql3 As String
        sql3 = "INSERT INTO interfacedb.lab_master (" & sql & ") VALUES ( " & sql1 & ") "
        Return sql3

    End Function

    Public Function InsertOrderDt() As String
        Dim sql As String
        Dim sql1 As String
        sql += "lab_item_code ,"
        sql1 += "'" & LabOrderDtStr.lab_item_code & "',"

        sql += "lab_item_name ,"
        sql1 += "'" & LabOrderDtStr.lab_item_name & "',"
        sql += "lab_items_normal ,"
        sql1 += "'" & LabOrderDtStr.lab_items_normal & "',"
        sql += "lab_item_unit ,"
        sql1 += "'" & LabOrderDtStr.lab_item_unit & "',"
        sql += "specimen_code ,"
        sql1 += "'" & LabOrderDtStr.specimen_code & "',"
        sql += "result_flag ,"
        sql1 += "'" & LabOrderDtStr.result_flag & "',"
        sql += "result_flag2 ,"
        sql1 += "'" & LabOrderDtStr.result_flag2 & "',"
        sql += "critical_flag ,"
        sql1 += "'" & LabOrderDtStr.critical_flag & "',"

        sql += "lab_item_group_code ,"
        sql1 += "'" & LabOrderDtStr.lab_item_group_code & "',"

        sql += "lab_item_group_name ,"
        sql1 += "'" & LabOrderDtStr.lab_item_group_name & "',"

        sql += "out_lab "
        sql1 += "'" & LabOrderDtStr.out_lab & "'"
        Dim sql3 As String
        sql3 = "INSERT INTO interfacedb.lab_detail (" & sql & ") VALUES ( " & sql1 & ") "


    End Function

    Public Sub InsertLab(grid As DevComponents.DotNetBar.SuperGrid.SuperGridControl, orderID As String)
        Dim db = ConnecDBRYH.NewConnection
        db.BeginTrans()
        Dim sql As String = ""
        For i As Integer = 0 To grid.PrimaryGrid.Rows.Count - 1
            If CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("ID").Value.ToString.Trim = "" Then
                sql += "INSERT  prdorderdt(orderid,prdcode,chkin_provider,chkin_date,prdprc) VALUES("
                sql += "'" & orderID & "',"
                sql += "" & CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("PRDCODE").Value & "," 'PRDCODE
                sql += "99," 'CHKIN_PROVIDER ผู้ทำรายการ
                sql += "current_timestamp(),"
                sql += "-1" 'PRDPRC ราคาสินค้า
                sql += ");"

            End If
        Next i

        Try
            db.ExecuteNonQuery(sql)
            db.CommitTrans()
            MsgBox("บันทึกสำเร็จ")
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox("บันทึกไม่สำเร็จ : " & ex.Message)
        Finally
            db.Dispose()
        End Try
    End Sub
    ''' <summary>
    ''' การยกเลิก Order
    ''' </summary>
    ''' <remarks>
    ''' หลักการทำงาน
    '''      result = OPRLABXRAYCLASS.cancelOrderDetail(IDORDERDT, 0)
    ''' ต้องส่งเลข IDOrder Detail และ สิทธิในการส่งหน้าๆนั้น
    ''' 0  = ทั่วไป
    ''' 1 =  ห้องแลป
    ''' เพราะการเข้าถึงการยกเลิกต่างกันอย่างเช่นห้องแลป สามารถยกเลิก ได้ถึงแม้จะ Check IN Order ไปแล้ว
    ''' ทั่วไปจะยกเลิก Order ได้ เมื่อ ไม่มีการ Check IN  ถ้า Check In แล้วไม่สามารถยกเลิกได้เป็นต้น
    '''
    ''' จะถูกเรียก function checkOrderDetailPolicies  เพื่อไปเอาวถานะของ Order ขณะนั้นเพื่อทำการตรวจสอบ
    ''' และ return ออกมาเป็น true หรือ false เพื่อบอกว่า สามารถยกเลิกได้หรือไม่
    ''' </remarks>
    ''' <returns>True สามารถยกเลิกได้ , False ไม่สามารถยกเลิกได้</returns>
    Public Function cancelOrderDetail(id As String, Policies As Integer) As Boolean
        Dim Result_policies As Boolean = True
        If Policies = ENVIRONMENTCLASS.ORDEREVENT.POLICIES_LAB Then
            Result_policies = checkOrderDetailPolicies(id, ENVIRONMENTCLASS.ORDEREVENT.POLICIES_LAB)
        Else
            Result_policies = checkOrderDetailPolicies(id, ENVIRONMENTCLASS.ORDEREVENT.POLICIES_NORMAL)
        End If

        If Result_policies = True Then

            Dim db = ConnecDBRYH.NewConnection
            Dim sql As String = "UPDATE  prdorderdt SET f_cancel = 1 WHERE(`id` = '" & id & "');"
            db.BeginTrans()
            Try
                db.ExecuteNonQuery(sql)
                db.CommitTrans()
                'MsgBox("บันทึกสำเร็จ")
            Catch ex As Exception
                db.RollbackTrans()
                MsgBox("เกิดข้อผิดพลาด " & ex.Message)
            Finally
                db.Dispose()
            End Try
            Return True

        ElseIf Result_policies = False Then

            Return False
        End If
    End Function
    ''' <summary>
    ''' Check Order Policies ว่าสามารถแก้ไขข้อมูลได้หรือไม่
    ''' </summary>
    Public Function checkOrderDetailPolicies(IDORDERDT As Integer, Policies As Integer) As Boolean
        Dim db = ConnecDBRYH.NewConnection

        Dim sql As String = "SELECT f_chkin,f_comp,f_cancel,f_cont FROM  prdorderdt WHERE id = '" & IDORDERDT & "';"
        db.BeginTrans()
        Dim dt As New DataTable
        Try
            dt = db.GetTable(sql)
            db.CommitTrans()
            If Policies = ENVIRONMENTCLASS.ORDEREVENT.POLICIES_LAB Then
                If dt.Rows(0)("f_comp").ToString = "1" Then
                    MSG_CANCLE_ = ENVIRONMENTCLASS.ORDEREVENT.DELMSGCOMP_F_TH
                    Return False
                ElseIf dt.Rows(0)("f_comp").ToString = "0" Then
                    MSG_CANCLE_ = ENVIRONMENTCLASS.ORDEREVENT.DELMSGCOMP_T_TH
                    Return True
                End If
            ElseIf Policies = ENVIRONMENTCLASS.ORDEREVENT.POLICIES_NORMAL Then
                If dt.Rows(0)("f_chkin").ToString = "1" Then
                    MSG_CANCLE_ = ENVIRONMENTCLASS.ORDEREVENT.DELMSGCHKIN_F_TH
                    Return False
                ElseIf dt.Rows(0)("f_chkin").ToString = "0" Then
                    MSG_CANCLE_ = ENVIRONMENTCLASS.ORDEREVENT.DELMSGCHKIN_T_TH
                    Return True
                End If
            End If
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)
            Return False
        Finally
            db.Dispose()
        End Try
    End Function
    Public Sub InsertXRay(grid As DevComponents.DotNetBar.SuperGrid.SuperGridControl)
        Dim db = ConnecDBRYH.NewConnection
        'Dim ORDERID As String = ""
        db.BeginTrans()
        Try
            Dim sql As String = ""
            'Update รหัส Sequen
            sql = "SELECT id FROM  masordseq WHERE(id = 1 AND `mm` = DATE_FORMAT(NOW(),'%m'));"
            If db.GetTable(sql).Rows.Count = 0 Then
                sql = "UPDATE  masordseq SET `seq` = 0000001,`mm` = DATE_FORMAT(NOW(),'%m') "
                sql += "WHERE(id = 1);"
            Else
                sql = "UPDATE  masordseq SET `seq` = `seq` +1 "
                sql += "WHERE(id = 1 AND `mm` = DATE_FORMAT(NOW(),'%m'));"
            End If
            'db.ExecuteNonQuery(sql)
            'sql = ""
            'Insert prdorder
            sql += "INSERT INTO  prdorder(orderid,vn,prdcat,hn,clinic,d_order,mk_provider,dr_provider) VALUES("
            sql += "(SELECT CONCAT(CAST(yr AS NCHAR(2)),CAST(mm AS NCHAR(2)),CAST(`seq` AS NCHAR(20)))AS str FROM  masordseq  WHERE(`mm` = DATE_FORMAT(NOW(),'%m')) LIMIT 1),"
            sql += "" & VN._VN & ","
            sql += "" & CType(grid.PrimaryGrid.GridPanel.ActiveRow, DevComponents.DotNetBar.SuperGrid.GridRow).Cells("prdcat").Value & ","

            sql += "" & PERSON.HN_ & ","
            sql += "" & VN._CLINIC & ","
            sql += "current_timestamp(),"
            sql += "99," 'MK_Provider
            sql += "" & VN._DR & "" 'DR_PROVIDER
            sql += ");"
            'db.ExecuteNonQuery(sql)
            'sql = ""
            For i As Integer = 0 To grid.PrimaryGrid.Rows.Count - 1
                sql += "INSERT  prdorderdt(orderid,prdcode,chkin_provider,chkin_date,prdprc) VALUES("
                sql += "(SELECT CONCAT(CAST(yr AS NCHAR(2)),CAST(mm AS NCHAR(2)),CAST(`seq` AS NCHAR(20)))AS str FROM  masordseq  WHERE(`mm` = DATE_FORMAT(NOW(),'%m')) LIMIT 1),"
                sql += "" & CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("prdcode").Value & "," 'PRDCODE
                sql += "99," 'CHKIN_PROVIDER ผู้ทำรายการ
                sql += "current_timestamp(),"
                sql += "-1" 'PRDPRC ราคาสินค้า
                sql += ");"
            Next i
            db.ExecuteNonQuery(sql)
            db.CommitTrans()
            MsgBox("บันทึกสำเร็จ")
        Catch ex As Exception
            db.RollbackTrans()
            db.Dispose()
            MsgBox(ex.Message)
        Finally
            db.Dispose()
        End Try
    End Sub




    Public Sub InsertOperate(grid As DevComponents.DotNetBar.SuperGrid.SuperGridControl, ByRef providerid As DataTable)
        Dim db = ConnecDBRYH.NewConnection
        Dim id As String = ""
        db.BeginTrans()
        Try
            Dim sql As String = ""
            'Update รหัส Sequen
            sql += "UPDATE  masordseq SET `seq` = `seq` +1 "
            sql += "WHERE(id = 1 AND `mm` = DATE_FORMAT(NOW(),'%m'));"
            db.ExecuteNonQuery(sql)
            sql = ""
            'Insert prdorder
            sql += "INSERT INTO  prdorder(orderid,vn,prdcat,hn,clinic,d_order,mk_provider,dr_provider) VALUES("
            sql += "(SELECT CONCAT(CAST(yr AS NCHAR(2)),CAST(mm AS NCHAR(2)),CAST(`seq` AS NCHAR(20)))AS str FROM  masordseq  WHERE(`mm` = DATE_FORMAT(NOW(),'%m')) LIMIT 1),"
            sql += "" & VN._VN & ","
            If grid.PrimaryGrid.Rows.Count > 0 Then
                sql += "" & CType(grid.PrimaryGrid.GridPanel.Rows(0), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("prdcat").Value & "," 'PRDCAT
            Else
                sql += "0,"
            End If

            sql += "" & PERSON.HN_ & ","
            sql += "" & VN._CLINIC & ","
            sql += "current_timestamp(),"
            sql += "99," 'MK_Provider
            If VN._DR = Nothing Then
                sql += "Null " 'DR_PROVIDER
            Else
                sql += "" & VN._DR & "" 'DR_PROVIDER
            End If
            'sql += "" & VN._DR & "" 'DR_PROVIDER
            sql += ");"
            db.ExecuteNonQuery(sql)

            For i As Integer = 0 To grid.PrimaryGrid.Rows.Count - 1
                sql = ""
                sql += "INSERT  prdorderdt(orderid,prdcode,chkin_provider,chkin_date,f_chkin,prdprc,qty) VALUES("
                sql += "(SELECT CONCAT(CAST(yr AS NCHAR(2)),CAST(mm AS NCHAR(2)),CAST(`seq` AS NCHAR(20)))AS str FROM  masordseq  WHERE(`mm` = DATE_FORMAT(NOW(),'%m')) LIMIT 1),"
                sql += "" & CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("PRDCODE").Value & "," 'PRDCODE
                sql += "99," 'CHKIN_PROVIDER ผู้ทำรายการ
                sql += "current_timestamp(),"
                If Convert.ToBoolean(CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("CHKIN").Value) = True Then
                    sql += "1,"
                Else
                    sql += "0,"
                End If
                sql += "" & CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("COST").Value & ", " 'PRDPRC ราคาสินค้า
                sql += "" & CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("QTY").Value & "  "
                sql += ");"
                sql += "SELECT LAST_INSERT_ID();"
                id = db.ExecuteScalar(sql)
                ' เช็ครายการ เชคอิน auto
                If Convert.ToBoolean(CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("CHKIN").Value) = True Then
                    sql = ""
                    sql += "INSERT INTO  orderchkin(`orderid`,`id`) VALUES("
                    sql += "(SELECT CONCAT(CAST(yr AS NCHAR(2)),CAST(mm AS NCHAR(2)),CAST(`seq` AS NCHAR(20)))AS str FROM  masordseq  WHERE(`mm` = DATE_FORMAT(NOW(),'%m')) LIMIT 1),"
                    sql += "'" & id & "');"
                    db.ExecuteNonQuery(sql)
                End If
            Next i
            'db.ExecuteNonQuery(sql)
            db.CommitTrans()
            MsgBox("บันทึกสำเร็จ")
        Catch ex As Exception
            db.RollbackTrans()
            db.Dispose()
            MsgBox(ex.Message)
        Finally
            db.Dispose()
        End Try
    End Sub

    Public Sub InsertOperateFinanceAdd(hnX As String, vnX As String, clinicX As String, grid As DevComponents.DotNetBar.SuperGrid.SuperGridControl, keyBy As String)
        Dim db = ConnecDBRYH.NewConnection
        Dim id As String = ""
        db.BeginTrans()
        Try
            Dim sql As String = ""
            'Update รหัส Sequen
            sql += "UPDATE  masordseq SET `seq` = `seq` +1 "
            sql += "WHERE(`mm` = DATE_FORMAT(NOW(),'%m'));"
            db.ExecuteNonQuery(sql)
            sql = ""
            'Insert prdorder
            sql += "INSERT INTO  prdorder(orderid,vn,prdcat,hn,clinic,d_order,ord_status,usmk,usmktime,mk_provider,dr_provider) VALUES("
            sql += "(SELECT CONCAT(CAST(yr AS NCHAR(2)),CAST(mm AS NCHAR(2)),CAST(`seq` AS NCHAR(20)))AS str FROM  masordseq  WHERE(`mm` = DATE_FORMAT(NOW(),'%m')) LIMIT 1),"
            sql += "" & vnX & ","
            If grid.PrimaryGrid.Rows.Count > 0 Then
                sql += "" & CType(grid.PrimaryGrid.GridPanel.Rows(0), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("prdcat").Value & "," 'PRDCAT
            Else
                sql += "0,"
            End If

            sql += "" & hnX & ","
            sql += "" & clinicX & ","
            sql += "current_timestamp(),"
            sql += "" & ENVIRONMENTCLASS.MASORDSTATUS.CHECKIN & ","
            sql += "'" & keyBy & "',"
            sql += "current_timestamp(),"
            sql += "'" & keyBy & "'," 'MK_Provider
            sql += "Null " 'DR_PROVIDER
            'sql += "" & VN._DR & "" 'DR_PROVIDER
            sql += ");"
            db.ExecuteNonQuery(sql)

            For i As Integer = 0 To grid.PrimaryGrid.Rows.Count - 1
                If Convert.ToBoolean(CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("เลือก").Value) = True Then
                    sql = ""
                    sql += "INSERT  prdorderdt(orderid,prdcode,chkin_provider,chkin_date,f_chkin,ord_status,hn,prdcat,usmk,usmktime,prdprc,rcode,qty) VALUES("
                    sql += "(SELECT CONCAT(CAST(yr AS NCHAR(2)),CAST(mm AS NCHAR(2)),CAST(`seq` AS NCHAR(20)))AS str FROM  masordseq  WHERE(`mm` = DATE_FORMAT(NOW(),'%m')) LIMIT 1),"
                    sql += "" & CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("prdcode").Value & "," 'PRDCODE
                    sql += "'" & keyBy & "'," 'CHKIN_PROVIDER ผู้ทำรายการ
                    sql += "current_timestamp(),"
                    'If Convert.ToBoolean(CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("CHKIN").Value) = True Then
                    sql += "1,"
                    sql += "" & ENVIRONMENTCLASS.MASORDSTATUS.CHECKIN & ","
                    sql += "" & hnX & ","
                    If grid.PrimaryGrid.Rows.Count > 0 Then
                        sql += "" & CType(grid.PrimaryGrid.GridPanel.Rows(0), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("prdcat").Value & "," 'PRDCAT
                    Else
                        sql += "0,"
                    End If
                    sql += "'" & keyBy & "',"
                    sql += "current_timestamp(),"
                    'Else
                    '    sql += "0,"
                    'End If
                    sql += "" & CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("opdprc").Value & ", " 'PRDPRC ราคาสินค้า
                    sql += "" & CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("rcode").Value & ", "
                    sql += "" & CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("qty").Value & "  "
                    sql += ");"
                    sql += "SELECT LAST_INSERT_ID();"
                    id = db.ExecuteScalar(sql)
                    ' เช็ครายการ เชคอิน auto
                    If Convert.ToBoolean(CType(grid.PrimaryGrid.GridPanel.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("chkin").Value) = True Then
                        sql = ""
                        sql += "INSERT INTO  orderchkin(`orderid`,`id`) VALUES("
                        sql += "(SELECT CONCAT(CAST(yr AS NCHAR(2)),CAST(mm AS NCHAR(2)),CAST(`seq` AS NCHAR(20)))AS str FROM  masordseq  WHERE(`mm` = DATE_FORMAT(NOW(),'%m')) LIMIT 1),"
                        sql += "'" & id & "');"
                        db.ExecuteNonQuery(sql)
                    End If
                End If
            Next i
            'db.ExecuteNonQuery(sql)
            db.CommitTrans()
            MsgBox("บันทึกสำเร็จ")
        Catch ex As Exception
            db.RollbackTrans()
            db.Dispose()
            MsgBox(ex.Message)
        Finally
            db.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' ดึงข้อมูล Lab ทั้งหมด ตาม VN
    ''' </summary>
    Public Sub getOrderAllVn(ByVal VN As String, ByRef dtset As DataSet, prdcat As String, ByVal hnstring As String, Optional IsIPD As Boolean = False)
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable
        Dim sql As String = ""
        sql = "SELECT  orderdt.id, orderdt.qty , orderdt.orderid , prdname AS NAME  , orderdt.remark AS Shortname , orderdt.f_chkin ,orderdt.f_comp , orderdt.prdcode , orderdt.prdprc as PRC  "
        sql += " FROM  (SELECT * FROM   prdorderdt WHERE  f_cancel = 0 and hn = " & hnstring & "  ) as orderdt  JOIN   "
        sql += " ( SELECT orderid FROM   prdorder "
        If IsIPD = False Then
            sql += "WHERE vn = " & VN & " "
        Else
            sql += "WHERE an = " & VN & " "
        End If
        sql += "AND f_cancel = 0 AND prdcat = " & prdcat & ")  as orderm ON   orderdt.orderid  =  orderm.orderid "
        sql += " JOIN masproduct  ON masproduct.prdcode  =  orderdt.prdcode LEFT JOIN drugitem ON masproduct.prdcode = drugitem.prdcode "
        If CInt(prdcat) = ENVIRONMENTCLASS.GETMASTPRDCAT.PRX Then
            sql += " WHERE drugitem.f_ms=1 "
        End If
        sql += "ORDER BY  orderdt.f_chkin  "

        db.GetTable(sql, dtset.Tables("LABORDERALL"))
        ' dt = db.GetTable(sql)
        db.Dispose()

    End Sub

    Public Sub getOrderAllLabResultHn(ByVal hn As String, ByRef dtset As DataSet)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = ""
        sql = " SELECT prdorder.orderid,prdorder.vn,prdorder.d_order,dr_provider,drname.doctorname , usmk , usermk.mkname , prdname ,id FROM  (SELECT  orderid,vn,d_order,dr_provider,usmk  FROM  prdorder WHERE HN =" & hn & "  AND  status = 1 ) as prdorder  LEFT JOIN  ( SELECT CONCAT_WS ('', stprename ,' ',name,' ',lname) AS 'doctorname',empid  FROM  hospemp LEFT JOIN masprename ON hospemp.prename = masprename.prename  ) as drname ON  drname.empid = prdorder.dr_provider LEFT JOIN (  SELECT CONCAT_WS ('', stprename ,' ',name,' ',lname) AS 'mkname',empid  FROM  hospemp LEFT JOIN masprename ON hospemp.prename = masprename.prename   )  as usermk  ON  usermk.empid = prdorder.usmk JOIN  (  SELECT id,orderid,prdcode,prdprc ,remark FROM prdorderdt  WHERE hn =" & hn & "  AND prdcat = " & ENVIRONMENTCLASS.GETMASTPRDCAT.LAB & " AND f_cancel = 0  ) AS prdorderdt  ON prdorderdt.orderid = prdorder.orderid JOIN masproduct  ON  prdorderdt.prdcode = masproduct.prdcode    WHERE prdorder.d_order > NOW() - INTERVAL 3 MONTH  "
        db.GetTable(sql, dtset.Tables("LABORDERHIS"))

        sql = "SELECT orderlabid,orderdtid,name,unit,normal,comment,value FROM "

    End Sub
    Public Sub getOrderAllLabResultHnStat(ByVal hn As String, ByRef dtset As DataSet)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = ""
        sql = " SELECT prdorder.orderid,prdorder.vn,prdorder.d_order,dr_provider,drname.doctorname , usmk , usermk.mkname , prdname ,id,orderlabid,name,normal,value,prdorderdt.prdprc FROM  (SELECT  orderid,vn,d_order,dr_provider,usmk  FROM  prdorder WHERE HN =" & hn & "  AND  status = 1  AND prdcat =" & ENVIRONMENTCLASS.GETMASTPRDCAT.LAB & ") as prdorder  JOIN  (  SELECT id,orderid,prdcode,prdprc ,remark FROM prdorderdt  WHERE hn =" & hn & "  AND prdcat = " & ENVIRONMENTCLASS.GETMASTPRDCAT.LAB & " AND f_cancel = 0   ) AS prdorderdt  ON prdorderdt.orderid = prdorder.orderid JOIN masproduct  ON  prdorderdt.prdcode = masproduct.prdcode JOIN (SELECT  * FROM prdordelabresult WHERE hn =" & hn & ") AS prdordelabresult ON prdordelabresult.orderdtid  = prdorderdt.id   LEFT JOIN  ( SELECT CONCAT_WS ('', stprename ,' ',name,' ',lname) AS 'doctorname',empid  FROM  hospemp LEFT JOIN masprename ON hospemp.prename = masprename.prename  ) as drname ON  drname.empid = prdorder.dr_provider LEFT JOIN (  SELECT CONCAT_WS ('', stprename ,' ',name,' ',lname) AS 'mkname',empid  FROM  hospemp LEFT JOIN masprename ON hospemp.prename = masprename.prename   )  as usermk  ON  usermk.empid = prdorder.usmk WHERE prdorder.d_order > NOW() - INTERVAL 3 MONTH  "
        db.GetTable(sql, dtset.Tables("LADORDERSTAT"))
    End Sub
    Public Sub getOrderAllXrayResultHNstat(ByVal vn As String, ByRef dtset As DataSet, ByVal datestart As Date, dateto As Date, Optional hn As String = "0")
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql = ""
        If hn = "0" Then
            sql = " SELECT  FROM  ( SELECT  orderid,vn,d_order,dr_provider,usmk  FROM  prdorder WHERE HN =" & hn & "  AND  status = 1  AND prdcat =" & ENVIRONMENTCLASS.GETMASTPRDCAT.XRAY & ")  as prdorder  JOIN  (  SELECT id,orderid,prdcode,prdprc ,remark FROM prdorderdt  WHERE hn =" & hn & "  AND prdcat = " & ENVIRONMENTCLASS.GETMASTPRDCAT.LAB & " AND f_cancel = 0   ) AS prdorderdt  ON prdorderdt.orderid = prdorder.orderid JOIN masproduct  ON  prdorderdt.prdcode = masproduct.prdcode "
        End If

        db.Dispose()
    End Sub
    Public Sub getVitalSignHistory(ByVal vn As String, ByRef dtset As DataSet, ByVal datestart As Date, ByVal dateto As Date, Optional hn As String = "0")
        Dim sql As String
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim USCulture As New System.Globalization.CultureInfo("en-US", True)

        sql = "SELECT frnvsopd.vsid,resultvs,vsdesc,ll,hl,unit,min,max,d_update FROM  (SELECT * FROM   frnvsopd  WHERE hn = " & hn & " AND d_update > '" & datestart.Year & datestart.ToString("-MM-dd HH:mm:ss") & "' AND d_update < '" & dateto.Year & dateto.ToString("-MM-dd HH:mm:ss") & "' ) AS frnvsopd JOIN masvstype ON masvstype.vsid = frnvsopd.vsid  ;"

        db.GetTable(sql, dtset.Tables("VITALSIGN"))
        db.Dispose()
    End Sub

    Public Shared Function getLastestVitalSignDatatable(ByVal hn As String, number As String, typeNumber As String) As DataTable
        Dim sql As String
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

        If typeNumber = "vn" Then
            sql = "SELECT frnvsopd.vsid, resultvs, vsdesc, ll, hl, unit, MIN, MAX, d_update FROM (SELECT * FROM frnvsopd  WHERE hn = '" & hn & "' AND vn = '" & number & "') AS frnvsopd JOIN masvstype ON masvstype.vsid = frnvsopd.vsid  "
        ElseIf typeNumber = "an" Then
            sql = "SELECT frnvsopd.vsid, resultvs, vsdesc, ll, hl, unit, MIN, MAX, d_update FROM (SELECT * FROM frnvsopd  WHERE hn = '" & hn & "' AND an = '" & number & "') AS frnvsopd JOIN masvstype ON masvstype.vsid = frnvsopd.vsid  "
        End If

        Dim dt As DataTable
        dt = db.GetTable(sql)

        db.Dispose()

        Return dt
    End Function

    Public Sub getOrderAllLabResultHnStat(ByVal hn As String, ByRef dtset As DataSet, ByVal datestart As Date, ByVal dateto As Date)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = ""

        Dim USCulture As New System.Globalization.CultureInfo("en-US", True)

        sql = " SELECT prdorder.orderid,prdorder.vn,prdorder.d_order,dr_provider,drname.doctorname , usmk , usermk.mkname , prdname ,id,orderlabid,name,normal,value,prdorderdt.prdprc FROM  (SELECT  orderid,vn,d_order,dr_provider,usmk  FROM  prdorder WHERE HN =" & hn & "  AND  status = 1  AND prdcat =" & ENVIRONMENTCLASS.GETMASTPRDCAT.LAB & ") as prdorder  JOIN  (  SELECT id,orderid,prdcode,prdprc ,remark FROM prdorderdt  WHERE hn =" & hn & "  AND prdcat = " & ENVIRONMENTCLASS.GETMASTPRDCAT.LAB & " AND f_cancel = 0   ) AS prdorderdt  ON prdorderdt.orderid = prdorder.orderid JOIN masproduct  ON  prdorderdt.prdcode = masproduct.prdcode JOIN (SELECT  * FROM prdordelabresult WHERE hn =" & hn & ") AS prdordelabresult ON prdordelabresult.orderdtid  = prdorderdt.id   LEFT JOIN  ( SELECT CONCAT_WS ('', stprename ,' ',name,' ',lname) AS 'doctorname',empid  FROM  hospemp LEFT JOIN masprename ON hospemp.prename = masprename.prename  ) as drname ON  drname.empid = prdorder.dr_provider LEFT JOIN (  SELECT CONCAT_WS ('', stprename ,' ',name,' ',lname) AS 'mkname',empid  FROM  hospemp LEFT JOIN masprename ON hospemp.prename = masprename.prename   )  as usermk  ON  usermk.empid = prdorder.usmk WHERE prdorder.d_order > '" & datestart.Year & datestart.ToString("-MM-dd HH:mm:ss") & "' AND prdorder.d_order < '" & dateto.Year & dateto.ToString("-MM-dd HH:mm:ss") & "' "
        db.GetTable(sql, dtset.Tables("LADORDERSTAT"))
        db.Dispose()
    End Sub

    Public Sub getOrderLabdtALLVn(ByVal VN As String, ByRef dtset As DataSet, prdcat As String, ByVal hnstring As String)
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable
        Dim sql As String = ""
        sql = "SELECT orderlabid,name,unit,normal,comment,value ,orderdtid "
        sql += " FROM  (SELECT * FROM   prdorderdt WHERE  f_cancel = 0 and hn = " & hnstring & "  ) as orderdt  JOIN   "
        sql += " ( SELECT orderid FROM   prdorder WHERE vn = " & VN & " AND f_cancel = 0 AND prdcat = " & prdcat & ")  as orderm ON   orderdt.orderid  =  orderm.orderid "
        sql += " JOIN  prdordelabresult  ON  prdordelabresult.ORDERDTID = orderdt.ID ORDER BY  orderdt.f_chkin  ; "

        db.GetTable(sql, dtset.Tables("ORDERDTLAB"))
        ' dt = db.GetTable(sql)
        db.Dispose()
    End Sub

    Public Sub setOrderLabDt(ByVal idorderdt As String, ByVal result As String)

        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = ""
        sql = "UPDATE prdordelabresult SET value = '" & result & "' WHERE orderlabid =" & idorderdt & " ;"
        db.ExecuteNonQuery(sql)
        db.Dispose()

    End Sub
    ''' <summary>
    ''' ดึงข้อมูล Operation ต่างๆ ตาม VN ต้องส่ง Prdcat ประเภทชนิดต่างๆเช่น Lab , หัตถการ,x ray
    ''' </summary>
    Public Shared Sub getOperationVn(VN As String, HN As String, dt As DataTable, Optional IsAN As Boolean = False)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = ""
        sql += "  SELECT prdorderdt.orderid as 'ORDERID', masproduct.prdcode AS 'ITEM',prdorderdt.prdcode as 'PRDCODE', masproduct.prdname AS 'DESCRIPTION' ,prdorderdt.ID AS 'ID',prdprc AS 'PRICE', QTY, prdprc * qty AS 'TOTAL', prdorderdt.f_chkin as 'CHECKIN', prdorderdt.f_comp as 'COMPLETE' FROM prdorderdt LEFT JOIN masproduct ON masproduct.prdcode = prdorderdt.prdcode LEFT JOIN drugitem ON masproduct.prdcode = drugitem.prdcode JOIN prdorder ON prdorderdt.orderid=prdorder.orderid WHERE "
        If IsAN = True Then
            sql += "prdorder.an ='" & VN & "' "
        Else
            sql += "prdorder.vn ='" & VN & "' "
        End If

        sql += "AND prdorderdt.hn='" & HN & "' AND prdorder.prdcat IN (1, 3, 4, 5) AND prdorderdt.f_cancel=0 AND (drugitem.f_ms IS NULL or drugitem.f_ms=1); "
        db.GetTable(sql, dt)
        db.Dispose()
    End Sub

    Public Shared Sub getPRDORDER(VN As String, dt As DataTable, hn As String, Optional IsAN As Boolean = False)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = ""
        sql = "SELECT DISTINCT(prdorder.orderid), prdorder.vn, prdorder.hn, masprdcats.catname, dr.DR_NAME as 'DR', CONCAT(provider.name, ' ', provider.lname) as 'PROVIDER', prdorder.d_order "
        sql += "FROM (SELECT * FROM prdorder WHERE status = 1 AND f_cancel = 0 AND "
        If IsAN = True Then
            sql += "an = '" & VN & "' "
        Else
            sql += "vn = '" & VN & "' "
        End If
        sql += "AND prdcat IN (1, 3, 4, 5)) AS prdorder "
        sql += "JOIN masprdcats ON prdorder.prdcat = masprdcats.prdcat "
        sql += "LEFT JOIN ( SELECT concat(masprename.STPRENAME ,hospemp.NAME, ' ', hospemp.LNAME) as 'DR_NAME', hospemp.empid FROM hospemp JOIN masprename ON hospemp.PRENAME = masprename.prename )as dr ON prdorder.dr_provider = dr.empid "
        sql += "JOIN hospemp as provider ON prdorder.mk_provider = provider.empid "
        sql += "JOIN (SELECT * FROM  prdorderdt WHERE hn = " & hn & " ) prdorderdt ON prdorderdt.orderid=prdorder.orderid LEFT JOIN drugitem ON prdorderdt.prdcode = drugitem.prdcode "
        sql += "WHERE prdorder.status=1 AND prdorder.f_cancel=0 AND "
        If IsAN = True Then
            sql += "prdorder.an = '" & VN & "' "
        Else
            sql += "prdorder.vn = '" & VN & "' "
        End If
        sql += "AND prdorder.prdcat IN (1, 3, 4, 5) AND prdorderdt.f_cancel=0 AND (drugitem.f_ms IS NULL or drugitem.f_ms=1);"
        db.GetTable(sql, dt)
        db.Dispose()
    End Sub

    Public Shared Sub getLABRESULT(VN As String, dt As DataTable, Optional IsAN As Boolean = False)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = ""
        sql = "SELECT orderdtid, normal, value, unit, comment FROM  prdordelabresult WHERE "
        If IsAN = True Then
            sql += "an = " & VN & ";"
        Else
            sql += "VN = " & VN & ";"
        End If

        db.GetTable(sql, dt)
        db.Dispose()
    End Sub
    ''' <summary>
    ''' ดึงข้อมูล X ray ที่ไม่สมบูรณ์ ตาม OrderID นั้นๆ Status ต้องเป็น 0 หรือ  1
    ''' </summary>
    ''' Public Shared Function GetReqXRayNonComp(vnID As String, prdcatID As String) As DataTable
    Public Shared Function getReqXRayNonComp(orderid As String, ByRef dtset As DataSet)
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable()
        Dim sql As String = ""
        sql += "SELECT prdorderdt.`id`,prdorderdt.orderid,prdorderdt.prdcode,masproduct.prdname,xrayitem.`code`,xrayitem.xryename  ,  prdorderdt.prdprc AS PRC "
        sql += ",prdorderdt.f_chkin AS 'CHECKIN' ,prdorderdt.f_comp AS 'COMPLETE' , prdorderdt.remark AS 'REMARK' "
        sql += "FROM (SELECT * FROM  prdorderdt WHERE orderid = '" & orderid & "' AND f_cancel = 0) AS prdorderdt  "
        sql += "LEFT JOIN  xrayitem AS xrayitem ON prdorderdt.prdcode = xrayitem.prdcode "
        sql += "LEFT JOIN (SELECT * FROM  masproduct WHERE(`status` = 1) ) AS masproduct ON prdorderdt.prdcode = masproduct.prdcode "

        db.GetTable(sql, dtset.Tables("ORDERXRAY"))
        db.Dispose()
    End Function

    ''' <summary>
    ''' ดึงข้อมูล Lab ที่ไม่สมบูรณ์ ตาม OrderID นั้นๆ Status ต้องเป็น 0 หรือ  1
    ''' </summary>
    ''' Public Shared Function GetReqLabNonComp(vnID As String, prdcatID As String) As DataTable
    Public Shared Function getReqLabNonComp(orderid As String, ByRef dtset As DataSet)
        Dim db = ConnecDBRYH.NewConnection

        Dim sql As String = ""
        sql += "SELECT t.`id`,t.orderid AS 'ORDERID',t.prdcode AS 'PRDCODE',p.prdname AS 'PRDNAME',lo.`code` , t.prdprc AS PRC "
        sql += ",t.f_chkin AS 'CHECKIN' ,t.f_comp AS 'COMPLETE',remark AS 'REMARK'"
        sql += "FROM  (SELECT * FROM  prdorderdt WHERE orderid = '" & orderid & "' AND f_cancel = 0) AS t "
        sql += "LEFT JOIN  lab_order AS lo ON t.prdcode = lo.prdcode  "
        sql += "LEFT JOIN (SELECT * FROM  masproduct WHERE(`status` IN(0,1)) ) AS p ON t.prdcode = p.prdcode "
        db.GetTable(sql, dtset.Tables("ORDERLAB"))
        db.Dispose()
        'dt = db.GetTable(sql)
        'db.Dispose()
        'Return dt
    End Function
    ''' <summary>
    ''' ดึงข้อมูล Lab ย่อย
    ''' </summary>
    ''' <remarks>จะทำหน้าเมื่อจะเห็นข้อมูล getLab ย่อย ใน Table prdorderlabresult</remarks>
    Public Shared Function getLabItem(LBLORDER As String, ByRef dtset As DataSet, ByVal HN As String)
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable
        Dim sql As String = ""
        sql = "SELECT orderlabid,name,unit,normal,comment,value ,orderdtid"
        sql += " FROM  ( SELECT  * FROM prdordelabresult WHERE hn =" & HN & " ) AS prdordelabresult  JOIN ( SELECT id FROM  prdorderdt WHERE hn = " & HN & "  and  orderid = " & LBLORDER & ") AS prdorderdt ON  prdordelabresult.orderdtid = prdorderdt.id  "
        db.GetTable(sql, dtset.Tables("ORDERDTLAB"))
        db.Dispose()
        ' Return dt
    End Function
    ''' <summary>
    ''' function อัพเดทสถานะ OrderLab ทั้ง Order ต้อง joid Order กับ OrderDt
    ''' </summary>
    ''' <remarks>ส่งค่า OrderID มา และสถานะที่ต้องการ update</remarks>
    Public Function updateLabStatus(ordID As String, LabStatus As Integer, PROVIDER As String) As Boolean
        Dim db = ConnecDBRYH.NewConnection
        Try
            db.BeginTrans()

            Dim sql As String = "UPDATE  prdorder JOIN  prdorderdt ON prdorder.orderid = prdorderdt.orderid  SET "
            If LabStatus = ENVIRONMENTCLASS.MASORDSTATUS.CHECKIN Then
                sql += "  prdorderdt.`chkin_provider` =  " & PROVIDER & " ,  prdorderdt.`chkin_date`  = now() , "
                sql += "prdorderdt.`f_chkin` = 1  , prdorderdt.ord_status =  " & ENVIRONMENTCLASS.MASORDSTATUS.CHECKIN & " "
                sql += " ,  prdorderdt.usmk = " & PROVIDER & " ,  prdorderdt.usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME)  "
                sql += "  WHERE  (prdorder.orderid = " & ordID & ") AND prdorderdt.f_comp = 0 AND prdorderdt.f_cancel = 0 ;"
                Dim sql1 As String = ""
                Dim sql2 As String = ""
                Dim sql3 As String = ""
                sql1 = "`PLACER_ORDER_NUMBER`,"
                sql2 = "" & ordID & ","
                sql1 += "`SEND_APP`,"
                sql2 += "'',"
                sql1 += "`SEND_FAC`,"
                sql2 += "'',"
                sql1 += "`PATIENT_ID`,"
                sql2 += ","
                sql1 += "`PATIENT_NAME`,"
                sql2 += ","
                sql1 += "`BIRTHDAY`,"
                sql2 += ","
                sql1 += "`SEX`,"
                sql2 += ","
                sql1 += "`PATIENT_CLASS`,"
                sql2 += ","
                sql1 += "`DOCTOR_ID`,"
                sql2 += ","
                sql1 += "`DOCTOR_NAME`,"
                sql2 += ","
                sql1 += "`ORDER_STAT`,"
                sql2 += ","
                sql1 += "`ORDER_TIME`,"
                sql2 += ","
                sql1 += "`ORDER_CODE`,"
                sql2 += ","
                sql1 += "`ORDER_KOR_DESC`,"
                sql2 += ","
                sql1 += "`ORDER_ENG_DESC`,"
                sql2 += ","
                sql1 += "`MODALITY`,"
                sql2 += ","
                sql1 += "`REC_DATE`,"
                sql2 += ","
                sql1 += "`REC_STAT`"
                sql2 += ""
                sql3 = "INSERT INTO T_HL_EXAM (" & sql1 & ") VALUES (" & sql2 & ");"
            ElseIf LabStatus = ENVIRONMENTCLASS.MASORDSTATUS.COMPLETE Then

                sql += "prdorderdt.`f_comp` = 1  , prdorderdt.ord_status =  " & ENVIRONMENTCLASS.MASORDSTATUS.COMPLETE & "  "
                sql += " ,  prdorderdt.usmk = " & PROVIDER & " ,  prdorderdt.USMKTIME = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME) "
                sql += "  WHERE (prdorder.orderid = " & ordID & ") AND prdorderdt.f_comp = 0 AND prdorderdt.f_cancel = 0 AND prdorderdt.f_chkin = 1  ;"

            ElseIf LabStatus = ENVIRONMENTCLASS.MASORDSTATUS.PASS Then

                sql += "prdorderdt.`f_chkin` = 1 , prdorderdt.ord_status =  " & ENVIRONMENTCLASS.MASORDSTATUS.PASS & "  "
                sql += " ,  prdorderdt.usmk = " & PROVIDER & " ,  prdorderdt.usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME) "
                sql += "  WHERE (prdorder.orderid = " & ordID & ") AND prdorderdt.f_comp = 0 AND prdorderdt.f_cancel = 0;"

            ElseIf LabStatus = ENVIRONMENTCLASS.MASORDSTATUS.CANCEL Then

                sql += "prdorderdt.`f_cancel` = 1  , prdorderdt.ord_status =  " & ENVIRONMENTCLASS.MASORDSTATUS.CANCEL & "  "
                sql += " ,  prdorderdt.usmk = " & PROVIDER & " ,  prdorderdt.usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME) "
                sql += "  WHERE (prdorder.orderid = " & ordID & ") AND prdorderdt.f_comp = 0 AND prdorderdt.f_cancel = 0;"

            End If



            db.ExecuteNonQuery(sql)
            db.CommitTrans()
            updateStatusOrder(ordID, PROVIDER, False)
            Return True
        Catch ex As Exception
            db.RollbackTrans()
            MsgBox(ex.ToString)
            Return False
        Finally
            db.Dispose()
        End Try
    End Function
    Public Sub insertpacs()






    End Sub

    ''' <summary>
    ''' function อัพเดทสถานะ OrderLab  OrderDt
    ''' </summary>
    ''' <remarks>
    ''' OrderDt เลข idOrderDt
    ''' Labstatus สถานะที่ต้องการ Update
    ''' Provider เลข User Req
    ''' Order เลขOrder คุม
    '''
    ''' เมื่อทำการ Update สถานนะเสร็จ
    ''' จะเรียก function
    ''' updateStatusOrder เพื่อเช็คว่า Order ทั้งหมด ใน Orderหลักควรจะอยู่ในสถานะไหน
    ''' </remarks>
    Public Sub updateLabOrderDtStatus(ORDETDT As Integer, LABSTATUS As Integer, PROVIDER As String, ORDER As String, REMARK As String)
        Dim db = ConnecDBRYH.NewConnection
        Dim sql As String = "UPDATE  prdorderdt SET  prdorderdt.remark  = '" & REMARK & "' ,"
        If LABSTATUS = ENVIRONMENTCLASS.MASORDSTATUS.CHECKIN Then
            sql += "  prdorderdt.`chkin_provider` =  " & PROVIDER & " ,  prdorderdt.`chkin_date`  = now() , "

            sql += "prdorderdt.`f_chkin` = 1  , prdorderdt.ord_status =  " & ENVIRONMENTCLASS.MASORDSTATUS.CHECKIN & " "
            sql += " ,  prdorderdt.usmk = " & PROVIDER & " ,  prdorderdt.usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME)  "
            sql += "  WHERE  (prdorderdt.id = " & ORDETDT & ") AND prdorderdt.f_cancel = 0 ;"

        ElseIf LABSTATUS = ENVIRONMENTCLASS.MASORDSTATUS.COMPLETE Then

            sql += "prdorderdt.`f_comp` = 1  , prdorderdt.ord_status =  " & ENVIRONMENTCLASS.MASORDSTATUS.COMPLETE & "  "
            sql += " ,  prdorderdt.usmk = " & PROVIDER & " ,  prdorderdt.usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME) "
            sql += "  WHERE  (prdorderdt.id = " & ORDETDT & ") AND prdorderdt.f_comp = 0 AND prdorderdt.f_cancel = 0 AND prdorderdt.f_chkin = 1 ;"

        ElseIf LABSTATUS = ENVIRONMENTCLASS.MASORDSTATUS.PASS Then

            sql += "prdorderdt.`f_chkin` = 1 , prdorderdt.ord_status =  " & ENVIRONMENTCLASS.MASORDSTATUS.PASS & "  "
            sql += " ,  prdorderdt.usmk = " & PROVIDER & " ,  prdorderdt.usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME) "
            sql += "  WHERE  (prdorderdt.id = " & ORDETDT & ") AND prdorderdt.F_COMP = 0 AND prdorderdt.f_cancel = 0 AND prdorderdt.f_chkin = 0 ;"

        ElseIf LABSTATUS = ENVIRONMENTCLASS.MASORDSTATUS.CANCEL Then

            sql += "prdorderdt.`f_cancel` = 1  , prdorderdt.`f_chkin` = 0  , prdorderdt.ord_status =  " & ENVIRONMENTCLASS.MASORDSTATUS.CANCEL & "  "
            sql += " ,  prdorderdt.usmk = " & PROVIDER & " ,  prdorderdt.usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME) "
            sql += "  WHERE  (prdorderdt.id = " & ORDETDT & ") AND prdorderdt.f_comp = 0 ;"

        End If
        db.ExecuteNonQuery(sql)
        db.Dispose()
        updateStatusOrder(ORDER.Trim, PROVIDER.Trim, False)

    End Sub

    Public Sub updateRemarkOrderDt(ORDERIDDT As String, REMARK As String, PRCPRC As Integer)
        Dim sql As String
        sql = "UPDATE  prdorderdt SET remark = '" & REMARK & "', prdprc = " & PRCPRC & " WHERE id = " & ORDERIDDT & ";"
        Dim condb As ConnecDBRYH = ConnecDBRYH.NewConnection
        condb.ExecuteNonQuery(sql)
        condb.Dispose()
    End Sub

    Public Sub updateRemarkOrder(ORDERID As String, REMARK As String)
        Dim sql As String
        sql = "UPDATE prdorder SET remark = '" & REMARK & "' WHERE ORDERID = " & ORDERID & ";"

        Dim condb As ConnecDBRYH = ConnecDBRYH.NewConnection
        condb.ExecuteNonQuery(sql)
        condb.Dispose()

    End Sub
    ''' <summary>
    ''' function นี้จะเช็ค ว่า Order dt ทั้งหมดควรอยู่ในสถานะอะไร
    ''' </summary>
    ''' <remarks>Order เลข ID จะวนลูป เช็ค ว่า Order dt ทั้งหมดเป็นสถานะอะไร แล้วควรจะอัพเดทเป็นอย่างไร</remarks>
    Public Function updateStatusOrder(ORDERID As String, PROVIDER As String, IsReturnSQL As Boolean) As String
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String
        Try

            Dim sql1 As String = "SELECT f_chkin , f_comp  ,f_cancel FROM  prdorderdt WHERE prdorderdt.orderid = " & ORDERID & " ;"
            Dim dt1 As DataTable
            dt1 = db.GetTable(sql1)
            Dim F_CHKIN As Integer = 0
            Dim F_COMP As Integer = 0
            Dim F_CANCEL As Integer = 0

            For i As Integer = 0 To dt1.Rows.Count - 1
                F_CHKIN += Convert.ToInt32(dt1.Rows(i)("f_chkin"))
                F_COMP += Convert.ToInt32(dt1.Rows(i)("f_comp"))
                F_CANCEL += Convert.ToInt32(dt1.Rows(i)("f_cancel"))
            Next

            db.Dispose()
            db = ConnecDBRYH.NewConnection

            sql = "UPDATE  prdorder JOIN  frnclinic ON prdorder.clinicid = frnclinic.clinicid  SET "

            If F_CANCEL = dt1.Rows.Count And F_CHKIN = 0 Then
                sql += " f_cancel = 1 , frnclinic.usmk = " & PROVIDER & " , prdorder.usmk = " & PROVIDER & "  , usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME) , ord_status = " & ENVIRONMENTCLASS.MASORDSTATUS.CANCEL & ", frnclinic.fintime = now() , frnclinic.rectime = now() , frnclinic.f_finish = 1   ,frnclinic.cstatus = " & ENVIRONMENTCLASS.MASCSTATUS.complete & "   "
            ElseIf F_CANCEL = dt1.Rows.Count And F_CHKIN <> 0 Then
                sql += " f_cancel = 1 , frnclinic.usmk = " & PROVIDER & " , prdorder.usmk = " & PROVIDER & "   , usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME) , ord_status = " & ENVIRONMENTCLASS.MASORDSTATUS.CANCEL & ", frnclinic.fintime = now() , frnclinic.f_finish = 1 ,frnclinic.cstatus = " & ENVIRONMENTCLASS.MASORDSTATUS.COMPLETE & "  "
            ElseIf F_CHKIN = 1 And F_CHKIN < dt1.Rows.Count Then
                sql += " frnclinic.rectime = now()  , frnclinic.cstatus = " & ENVIRONMENTCLASS.MASCSTATUS.accept & ""
            ElseIf F_CHKIN = dt1.Rows.Count And F_COMP = 0 And F_CANCEL = 0 Then
                sql += " frnclinic.usmk = " & PROVIDER & " , prdorder.usmk = " & PROVIDER & "  , usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME) , ord_status = " & ENVIRONMENTCLASS.MASORDSTATUS.CHECKIN & "  "
            ElseIf F_COMP > 0 And F_COMP < dt1.Rows.Count And (F_COMP + F_CANCEL < F_CHKIN) Then
                sql += " frnclinic.usmk = " & PROVIDER & " , prdorder.usmk = " & PROVIDER & " , usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME) , ord_status = " & ENVIRONMENTCLASS.MASORDSTATUS.SOMECOMPLETE & " "
            ElseIf (F_COMP + F_CANCEL) = dt1.Rows.Count Then
                sql += " f_comp = 1, frnclinic.usmk = " & PROVIDER & " , prdorder.usmk = " & PROVIDER & " ,usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME) , ord_status =" & ENVIRONMENTCLASS.MASORDSTATUS.COMPLETE & " ,  frnclinic.f_finish = 1   ,frnclinic.cstatus = " & ENVIRONMENTCLASS.MASCSTATUS.complete & "    "
            ElseIf (F_CHKIN + F_CANCEL) = dt1.Rows.Count Then
                sql += " f_comp = 1, frnclinic.usmk = " & PROVIDER & " , prdorder.usmk = " & PROVIDER & " ,usmktime = CAST(CONCAT(CAST(curdate() AS CHAR(10)),' ',CAST(curtime() AS CHAR(10))) AS DATETIME) , ord_status =" & ENVIRONMENTCLASS.MASORDSTATUS.COMPLETE & " ,  frnclinic.f_finish = 1   ,frnclinic.cstatus = " & ENVIRONMENTCLASS.MASCSTATUS.complete & "    "
            Else
                'Exit Function
                Return ""
            End If
            sql += " WHERE ORDERID = " & ORDERID & " ; "
            If IsReturnSQL = False Then
                db.ExecuteNonQuery(sql)
                db.Dispose()
            Else
                db.Dispose()
                Return sql
            End If
        Catch ex As Exception
            If IsReturnSQL = False Then
                MsgBox(ex.ToString)
            End If
            Return ""
        Finally
            db.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' ดึงข้อมูลราคาต่างๆลง ใน Tab Dgv
    ''' </summary>
    Public Shared Function getTabLabOrder(ByVal VN As String) As String
        Dim dt As New DataTable
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String
        Dim textintab As String
        sql = "SELECT  vn ,COUNT(CASE WHEN(prdorder.status = 1) THEN 1  END ) AS Request  , "
        sql += "  COUNT(CASE WHEN(prdorder.status = 2) THEN 1  END ) AS CHECKIN  , COUNT(CASE WHEN(prdorder.status = 3) THEN 1  END ) AS Complete ,   "
        sql += "  COUNT(CASE WHEN(prdorder.status = 4) THEN 1  END ) AS PASS    , COUNT(CASE WHEN(prdorder.status = 5) THEN 1  END ) AS CANCEL    "
        sql += "  FROM  prdorder WHERE vn ='" & VN & "'  GROUP BY vn ;  "

        dt = db.GetTable(sql)
        db.Dispose()
        textintab = "LAB"
        textintab += ""
        If dt.Rows.Count = 0 Then
            Return textintab
        Else

        End If
        If dt.Rows(0)("Request").ToString = "0" Then

        Else
            textintab += "<b><font color=""#57769D"">(Re-" & dt.Rows(0)("Request").ToString & ")</font></b>"
        End If

        If dt.Rows(0)("CHECKIN").ToString = "0" Then

        Else
            textintab += "<b><font color=""#B5CC88"">(CH-" & dt.Rows(0)("CHECKIN").ToString & ")</font></b>"
        End If

        If dt.Rows(0)("Complete").ToString = "0" Then

        Else
            textintab += "<b><font color=""#0F243E"">(COM-" & dt.Rows(0)("Complete").ToString & ")</font></b>"
        End If
        '
        If dt.Rows(0)("PASS").ToString = "0" Then

        Else
            textintab += "<b><font color=""#4E5D30"">(P-" & dt.Rows(0)("PASS").ToString & ")</font></b>"
        End If

        If dt.Rows(0)("CANCEL").ToString = "0" Then

        Else
            textintab += "<b><font color=""#BA1419"">(CAN-" & dt.Rows(0)("PASS").ToString & ")</font></b>"
        End If

        Return textintab
        '        LAB
        '<font color="#B5CC88"><b>(CH-0)</b></font>
        '<font color="#0F243E"><b>(COM-0)</b></font>
        '<font color="#BA1419"><b>(CAN-0)</b></font><font color="#4E5D30"><b>(P-0)</b></font>

    End Function

    ''' <summary>
    ''' ดึงข้อมูล ราคาต่างๆแยกตาม สถานะที่ Checkin หรือราคาที่ไม่ Checkin
    ''' </summary>
    Public Shared Function getSumOrderLab(ByVal dgv As DevComponents.DotNetBar.SuperGrid.SuperGridControl) As String
        Dim sumtotal_checkin As Integer = 0
        Dim sumtotal_notcheckin As Integer = 0

        For i As Integer = 0 To dgv.PrimaryGrid.Rows.Count - 1
            If CType(dgv.PrimaryGrid.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("STATUS").Value = True Then
                sumtotal_checkin += CType(dgv.PrimaryGrid.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("PRC").Value
            Else
                sumtotal_notcheckin += CType(dgv.PrimaryGrid.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("PRC").Value
            End If
        Next
        Return String.Format("<div align =""right""><font color=""Green"">ราคา(Check IN) {0:C} BATH</font> <font color=""Red""> ราคา(ยังไม่ได้ Check IN) {1:C} BATH </font><font color=""Blue"">{2:C} BATH</font> </div>", sumtotal_checkin, sumtotal_notcheckin, sumtotal_checkin + sumtotal_notcheckin)
    End Function
    ''' <summary>
    ''' ดึงราคารวม Price Total ไม่สนใจสถานะ
    ''' </summary>
    Public Shared Function getTotalPriceOrder(ByVal dgv As DevComponents.DotNetBar.SuperGrid.SuperGridControl) As String
        Dim sumtotal As Integer = 0
        For i As Integer = 0 To dgv.PrimaryGrid.Rows.Count - 1
            sumtotal += CType(dgv.PrimaryGrid.Rows(i), DevComponents.DotNetBar.SuperGrid.GridRow).Cells("PRC").Value
        Next
        Return sumtotal
    End Function

    ''' <summary>
    ''' ดึงข้อมูลหมอที่ Request Order
    ''' </summary>
    Function getDoctorReq(ByVal VN As String) As DataTable
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        'Dim sql As String = "SELECT p.orderid AS 'id',p.prdcat AS 'type',pd.prdname AS 'ITEM',os.odsname AS 'Result' "
        'sql += "FROM  prdorder p JOIN  prdorderdt pdt ON p.orderid = pdt.orderid "
        'sql += "JOIN  masproduct pd ON pdt.prdcode = pd.prdcode "
        'sql += "JOIN  masordstatus os ON pdt.ord_status = os.ord_status "
        'sql += "WHERE vn='" & VN & "' AND p.prdcat IN (4,5) AND pdt.ord_status != '5';"

        Dim sql As String = "SELECT p.orderid AS 'id',p.prdcat AS 'type',pd.prdname AS 'ITEM',os.odsname AS 'Result' "
        sql += "FROM  prdorder p JOIN  prdorderdt pdt ON p.orderid = pdt.orderid "
        sql += "JOIN  masproduct pd ON pdt.prdcode = pd.prdcode "
        sql += "JOIN  masordstatus os ON pdt.ord_status = os.ord_status "
        sql += "WHERE vn='" & VN & "' AND p.prdcat IN (4,5) AND pdt.ord_status != '5' "
        sql += ";"

        Try
            Dim dt As DataTable = db.GetTable(sql)
            db.Dispose()
            Return dt
        Catch ex As Exception
            db.Dispose()
            MsgBox(ex.ToString)
        Finally
            db.Dispose()
        End Try
        Return Nothing
    End Function

    Private Function STK_CTRL(ByVal _PRDCODE As String, ByRef dTable As DataTable) As Boolean
        Dim _STKCTRL As Boolean = False
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = ""
        sql = "SELECT f_stk, f_lot FROM masproduct WHERE prdcode=" & _PRDCODE & ";"
        dTable = db.GetTable(sql)
        db.Dispose()
        If dTable.Rows.Count > 0 Then
            _STKCTRL = dTable.Rows(0)("f_stk")
        End If
        Return _STKCTRL
    End Function

    Public Function STK_ID(ByVal _CLINICID As String) As String
        Dim _STKID As String = "null"
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = ""
        Dim dTable As New DataTable
        sql = "SELECT stkid FROM masclinic WHERE clinic=" & _CLINICID & ";"
        dTable = db.GetTable(sql)
        db.Dispose()
        If dTable.Rows.Count > 0 Then
            _STKID = If(IsDBNull(dTable.Rows(0)(0)), "null", dTable.Rows(0)(0))
        End If

        Return _STKID
    End Function

    Private Function STK_ONHAND(ByVal _STKID As String, ByVal F_LOT As String, ByVal _PRDCODE As String, ByVal _QTY As String, ByVal _UNIT As String, ByRef _LOTID As String) As Boolean
        Dim _COMP As Boolean = False 'FLAG เก็บค่าว่าสามารถตัดสตอคได้หรือไม่
        Dim _STKREMAIN As Integer = 0
        Dim _ONHANDID As Integer = 0
        Dim _STKUSAGE As Integer = 0
        Dim sql As String = ""
        Dim dTable As New DataTable
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        If F_LOT Then

            'หา LOT
            sql = "SELECT lotid FROM prdlotcon WHERE prdcode=" & _PRDCODE & " ORDER BY expdate ASC LIMIT 1;"
            db.GetTable(sql, dTable)
            If dTable.Rows.Count > 0 Then
                _LOTID = dTable.Rows(0)(0)
            End If

            'ตัดสต็อคเมื่อ f_stk ใน prdorderdt เป็น 1
            sql = "SELECT SUM(QTY) as 'STKUSAGE' FROM prdorderdt WHERE prdcode=" & _PRDCODE & " AND unitid=" & _UNIT & " AND lotid=" & _LOTID & " AND f_stk=1;"
            dTable = db.GetTable(sql)
            If dTable.Rows.Count > 0 Then
                _STKUSAGE = If(IsDBNull(dTable.Rows(0)(0)), 0, CInt(dTable.Rows(0)(0)))
            End If

            'หา id -stock ปริมาณคงเหลือ
            sql = "SELECT onhandid, qtytotal FROM stockonhand WHERE prdcode=" & _PRDCODE & " AND lotid=" & _LOTID & " AND UNITID=" & _UNIT & ";"
            dTable = db.GetTable(sql)

            If dTable.Rows.Count > 0 Then
                _STKREMAIN = If(IsDBNull(dTable.Rows(0)("qtytotal")), 0, CInt(dTable.Rows(0)("qtytotal")))
                _ONHANDID = If(IsDBNull(dTable.Rows(0)("onhandid")), 0, CInt(dTable.Rows(0)("onhandid")))
            End If

        Else
            sql = "SELECT SUM(QTY) as 'STKUSAGE' FROM prdorderdt WHERE prdcode=" & _PRDCODE & " AND unitid=" & _UNIT & " AND f_stk=1;"
            dTable = db.GetTable(sql)
            If dTable.Rows.Count > 0 Then
                _STKUSAGE = If(IsDBNull(dTable.Rows(0)(0)), 0, CInt(dTable.Rows(0)(0)))
            End If

            sql = "SELECT onhandid, qtytotal FROM stockonhand WHERE prdcode=" & _PRDCODE & " AND UNITID=" & _UNIT & ";"
            dTable = db.GetTable(sql)
            If dTable.Rows.Count > 0 Then
                _STKREMAIN = If(IsDBNull(dTable.Rows(0)("qtytotal")), 0, CInt(dTable.Rows(0)("qtytotal")))
                _ONHANDID = If(IsDBNull(dTable.Rows(0)("onhandid")), 0, CInt(dTable.Rows(0)("onhandid")))
            End If
        End If

        _STKREMAIN = _STKREMAIN - _STKUSAGE

        If _STKREMAIN > _QTY Then
            'sql = "UPDATE stockonhand SET qtytotal=" & (_STKREMAIN - _QTY) & " WHERE onhandid=" & _ONHANDID & ";"
            'db.ExecuteNonQuery(sql)
            _COMP = True
        Else
            _COMP = False
        End If
        db.Dispose()
        Return _COMP
    End Function




End Class